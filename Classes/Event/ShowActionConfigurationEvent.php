<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2021 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Event;

use KDN\KdnEvents\Domain\Model\Event;

final class ShowActionConfigurationEvent
{
    /**
     * @var Event
     */
    private $event;

    /**
     * @var array
     */
    private $viewParameters;

    /**
     * @var bool
     */
    private $enableRegistration;

    /**
     * @var bool
     */
    private $enableInvitation;

    /**
     * ShowActionConfigurationEvent constructor.
     * @param array $viewParameters
     * @param Event $event
     */
    public function __construct(array $viewParameters, Event $event)
    {
        $this->event = $event;
        $this->viewParameters = $viewParameters;
        $this->enableRegistration = $viewParameters['enableRegistration'] ?? false;
        $this->enableInvitation = $viewParameters['enableInvitation'] ?? false;
    }

    /**
     * @return Event
     */
    public function getEvent(): Event
    {
        return $this->event;
    }

    /**
     * @return array
     */
    public function getViewParameters(): array
    {
        $parameters = $this->viewParameters;
        $parameters['event'] = $this->event;
        $parameters['enableRegistration'] = $this->enableRegistration;
        $parameters['enableInvitation'] = $this->enableInvitation;
        if (!$this->enableInvitation) {
            $parameters['invitation'] = null;
        }
        return $parameters;
    }

    /**
     * @return bool
     */
    public function isEnableRegistration(): bool
    {
        return $this->enableRegistration;
    }

    /**
     * @param bool $enableRegistration
     */
    public function setEnableRegistration(bool $enableRegistration): void
    {
        $this->enableRegistration = $enableRegistration;
    }

    /**
     * @return bool
     */
    public function isEnableInvitation(): bool
    {
        return $this->enableInvitation;
    }

    /**
     * @param bool $enableInvitation
     */
    public function setEnableInvitation(bool $enableInvitation): void
    {
        $this->enableInvitation = $enableInvitation;
    }

}