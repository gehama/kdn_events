<?php
declare(strict_types=1);

namespace KDN\KdnEvents\Command;

use KDN\KdnEvents\Service\ReminderService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class ReminderCommand
 */
class ReminderCommand extends Command
{
    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this
            ->setDescription('KDN Events: Send reminders for all event registrations one week before the event');
    }

    /**
     * Executes the command for adding the lock file
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());
        // Bootstrap::initializeBackendAuthentication();
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var ReminderService $reminderService */
        $reminderService = $objectManager->get(ReminderService::class);
        $reminderService->sendReminders(7);
        return 0;
    }
}
