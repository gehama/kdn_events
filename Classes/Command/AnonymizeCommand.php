<?php
declare(strict_types=1);

namespace KDN\KdnEvents\Command;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Domain\Repository\EventRepository;
use KDN\KdnEvents\Domain\Repository\RegistrationRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class AnonymizeCommand
 */
class AnonymizeCommand extends Command
{
    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this
            ->setDescription('KDN Events: Anonymize registrations')
            ->addArgument(
                'anonymizePeriod',
                InputArgument::OPTIONAL,
                'Number of days after event when registrations will be anonymized',
                30
            );
    }

    /**
     * Executes the command for adding the lock file
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());
        $anonymizePeriod = (int) $input->getOption('anonymizePeriod');
        // Bootstrap::initializeBackendAuthentication();
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var EventRepository $eventRepository */
        $eventRepository = $objectManager->get(EventRepository::class);
        /** @var RegistrationRepository $registrationRepository */
        $registrationRepository = $objectManager->get(RegistrationRepository::class);
        /** @var RegistrationRepository $registrationRepository */
        $events = $eventRepository->findForAnonymization($anonymizePeriod);
        foreach ($events as $event) {
            /** @var Event $event */
            $registrations = $registrationRepository->findEventRowsForAnonymization($event);
            foreach ($registrations as $registration) {
                /** @var Registration $registration */
                if (strpos($registration->getEmail(), 'anonym') === false
                    || strpos($registration->getLastName(), 'anonym') === false) {
                    $registration->setEmail('anonym' . $registration->getUid() . '@example.com');
                    $registration->setFirstName('Anonym');
                    $registration->setLastName('Anonym');
                    $registration->setOrganisation('Anonym');
                    $registration->setStreet('');
                    //$registration->setZipcode('12345');
                    //$registration->setTown('Anonym');
                    $registration->setPhone('12345');
                    $registration->setOrganisationEmail($registration->getEmail());
                    $registration->setOrganisationStreet('');
                    $registration->setOrganisationPhone('12345');
                    $registration->setCustomerMessage('');
                    $registration->setWorkExperience('');
                    $registration->setOccupation('');
                    $registrationRepository->update($registration);
                }
            }
        }
        return 0;
    }
}
