<?php
declare(strict_types=1);

namespace KDN\KdnEvents\Command;

use KDN\KdnEvents\Domain\Model\Dto\EventDemand;
use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Repository\EventRepository;
use KDN\KdnEvents\Service\EventService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class UpdateCommand
 */
class UpdateCommand extends Command
{
    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure()
    {
        $this
            ->setDescription('KDN Events: Update event data for published events');
    }

    /**
     * Executes the command for adding the lock file
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());
        // Bootstrap::initializeBackendAuthentication();
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var EventRepository $eventRepository */
        $eventRepository = $objectManager->get(EventRepository::class);
        $demand = new EventDemand();
        $demand->setRespectStoragePid(false);
        $demand->setIgnoreHideInListFlag(true);
        $demand->setMinStartTimestamp(time());
        $events = $eventRepository->findDemanded($demand);
        /** @var EventService $eventService */
        $eventService = $objectManager->get(EventService::class);
        foreach ($events as $event) {
            /** @var Event $event */
            if ((null !== $status = $event->getStatus()) && $status->isEventVisible()) {
                $eventService->updateEventRegistrationCount($event->getUid());
                $eventService->updateEventTimespan($event->getUid());
            }
        }
        return 0;
    }
}
