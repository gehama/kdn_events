<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Registration;

use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Service\MailService;

/**
 * Event dispatched before registration emails are sent
 */
final class CancellationBeforeEmailEvent extends AbstractRegistrationEvent
{
    /**
     * Files will be attached to confirmation email
     * @var array
     */
    protected $attachedFiles = [];

    /**
     * @var string
     */
    protected $templateEmailCustomer = MailService::TEMPLATE_KEY_CANCEL_INFO_CUSTOMER;

    /**
     * Email settings
     * @var array
     */
    protected $mailSettings;

    public function __construct(Registration $registration, array $mailSettings)
    {
        parent::__construct($registration);
        $this->mailSettings = $mailSettings;
    }

    /**
     * @return array
     */
    public function getAttachedFiles(): array
    {
        return $this->attachedFiles;
    }

    /**
     * Add attachment (used in emails)
     *
     * @param string $absolutePath Absolute path to file
     */
    public function addAttachmentPath(string $absolutePath)
    {
        $this->attachedFiles[] = $absolutePath;
    }

    /**
     * @return array
     */
    public function getMailSettings(): array
    {
        return $this->mailSettings;
    }

    /**
     * @param array $mailSettings
     */
    public function setMailSettings(array $mailSettings): void
    {
        $this->mailSettings = $mailSettings;
    }

    /**
     * @return string
     */
    public function getTemplateEmailCustomer(): string
    {
        return $this->templateEmailCustomer;
    }

    /**
     * @param string $templateEmailCustomer
     */
    public function setTemplateEmailCustomer(string $templateEmailCustomer): void
    {
        $this->templateEmailCustomer = $templateEmailCustomer;
    }

}
