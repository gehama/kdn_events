<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Registration;

use KDN\KdnEvents\Domain\Model\AbstractEventPersonModel;
use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Exception\RegistrationCancellationException;
use KDN\KdnEvents\Exception\RegistrationValidationException;
use KDN\KdnEvents\Service\ConfigurationService;
use KDN\KdnEvents\Service\MailService;
use KDN\KdnEvents\Service\OrderItemPriceCalculator;
use KDN\KdnEvents\Utility\MiscUtility;
use KDN\KdnEvents\Utility\TcaUtility;
use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Class registration service
 */
class RegistrationManager extends ConfigurationService
{

    /**
     * Event repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\EventRepository
     */
    protected $eventRepository;

    /**
     * @var \KDN\KdnEvents\Service\FrontendSessionHandlerService
     */
    protected $frontendSessionHandlerService;

    /**
     * @var \KDN\KdnEvents\Service\WaitlistService
     */
    protected $waitlistService;

    /**
     * @var \KDN\KdnEvents\Service\MailService
     */
    protected $mailService;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * Registration repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\RegistrationRepository
     */
    protected $registrationRepository;

    /**
     * Invitation repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\InvitationRepository
     */
    protected $invitationRepository;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function injectEventRepository(\KDN\KdnEvents\Domain\Repository\EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * DI for invitation repository
     *
     * @param \KDN\KdnEvents\Domain\Repository\InvitationRepository $invitationRepository
     */
    public function injectInvitationRepository(\KDN\KdnEvents\Domain\Repository\InvitationRepository $invitationRepository)
    {
        $this->invitationRepository = $invitationRepository;
    }

    /**
     * DI for frontend session handler service
     *
     * @param \KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService
     */
    public function injectFrontendSessionHandlerService(\KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService)
    {
        $this->frontendSessionHandlerService = $frontendSessionHandlerService;
    }

    /**
     * DI for waitlist service
     *
     * @param \KDN\KdnEvents\Service\WaitlistService $waitlistService
     */
    public function injectWaitlistService(\KDN\KdnEvents\Service\WaitlistService $waitlistService)
    {
        $this->waitlistService = $waitlistService;
    }

    /**
     * DI for registration repository
     *
     * @param \KDN\KdnEvents\Domain\Repository\RegistrationRepository $registrationRepository
     */
    public function injectRegistrationRepository(\KDN\KdnEvents\Domain\Repository\RegistrationRepository $registrationRepository)
    {
        $this->registrationRepository = $registrationRepository;
    }

    /**
     * DI for mail service
     *
     * @param \KDN\KdnEvents\Service\MailService $mailService
     */
    public function injectMailService(\KDN\KdnEvents\Service\MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * Initialize parameters for submit action view
     *
     * @param Event $event
     * @param Registration $registration
     * @param string|null $notes Used as Honeypot field for spammers
     * @param string|null $confirmation
     * @param array $settings
     * @return array The show action parameters
     * @throws RegistrationValidationException
     */
    public function initializeSubmitViewParameters(
        Event $event,
        Registration $registration,
        ?string $notes,
        ?string $confirmation,
        array $settings
    ): array
    {
        $registration->setEvent($event);
        $feSessionHandler = $this->frontendSessionHandlerService;
        if ($feSessionHandler->isFixedUserData()) {
            $feSessionHandler->prefillPersonDataForUserIfSet($registration);
        }
        $this->assertRegistrationValid($registration, $settings);
        if (!TcaUtility::getExtConf('disablePrivacyTermsForAuthenticatedUsers') || 0 >= MiscUtility::getFrontendUserId()) {
            $registration->setPrivacyTerms(true);
        }
        //$encryptionService = new EncryptionService();
        return array(
            'event' => $event,
            'registration' => $registration,
            'notes' => (string)$notes,
            'enabledFields' => $this->getEnabledFields($event, $registration),
            'requiredFields' => $this->getRequiredFields($event, $registration),
            'cryptKey' => $confirmation,//$encryptionService->encrypt(date('Y-m-d H:i:s')),
        );
    }

    /**
     * Initialize parameters for submit action view
     *
     * @param Event $event
     * @param Registration $registration
     * @param array $settings
     * @return void
     * @throws RegistrationValidationException
     */
    public function processRegistration(
        Event $event,
        Registration $registration,
        array $settings
    )
    {
        if (!TcaUtility::getExtConf('disablePrivacyTermsForAuthenticatedUsers') || 0 >= MiscUtility::getFrontendUserId()) {
            $registration->setPrivacyTerms(true);
        }
        $feSessionHandler = $this->frontendSessionHandlerService;
        if ($feSessionHandler->isFixedUserData()) {
            $feSessionHandler->prefillPersonDataForUserIfSet($registration);
        }
        $registration->setEvent($event);
        $customerMessage = trim(strip_tags((string)$registration->getCustomerMessage()));
        $registration->setCustomerMessage($customerMessage);
        if (null === $registration->getEventTime() && null !== $firstActiveEventTime = $event->getFirstActiveEventTime()) {
            $registration->setEventTime($firstActiveEventTime);
        }
        $this->assertRegistrationValid($registration, $settings);

        $frontendUser = $this->frontendSessionHandlerService->getCurrentFrontendUser();
        if (null !== $frontendUser) {
            $registration->setUser($frontendUser);
        }
        $status = Registration::STATUS_REGISTERED;
        if ($event->getStatus()->isWaitlistOpen()) {
            $status = Registration::STATUS_WAITLIST;
        }
        $registration->setStatus($status);

        // Increment registration count
        $newRegistrationCount = (int)$event->getRegistrations() + 1;
        $event->setRegistrations($newRegistrationCount);

        $this->waitlistService->updateEventRegistrationStatus($event);
        $this->eventRepository->update($event);

        /** @var OrderItemPriceCalculator $priceCalculator */
        $priceCalculator = GeneralUtility::makeInstance(OrderItemPriceCalculator::class);
        if (null !== $price = $priceCalculator->getItemPrice($event)) {
            $registration->setPrice($price);
        }
        $this->registrationRepository->generateUniqueHash($registration);

        $dispatchEvent = new RegistrationBeforePersistEvent($registration);
        $this->eventDispatcher->dispatch($dispatchEvent);

        $this->registrationRepository->add($registration);

        /** @var PersistenceManager $persistenceManager */
        $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
        if ((null !== $invitation = $registration->getInvitation())
            && (int)$invitation->getStatus() <= AbstractEventPersonModel::STATUS_CREATED) {
            $invitation->setStatus(Registration::STATUS_PARTICIPATED);
            $invitation->setRegistration($registration);
            if (null !== $this->invitationRepository) {
                $this->invitationRepository->update($invitation);
            } else {
                $persistenceManager->update($invitation);
            }
        }

        $persistenceManager->persistAll();
        $dispatchEvent = new RegistrationAfterPersistEvent($registration, $settings);
        $this->eventDispatcher->dispatch($dispatchEvent);
    }

    /**
     * Initialize parameters for submit action view
     *
     * @param Event $event
     * @param Registration $registration
     * @param array $mailSettings
     * @return void
     */
    public function sendRegistrationNotifications(
        Event $event,
        Registration $registration,
        array $mailSettings
    )
    {
        //$contentObject = $this->configurationManager->getContentObject();
        $orgEmail = $registration->getOrganisationEmail();
        if (GeneralUtility::validEmail($orgEmail) && $orgEmail !== $registration->getEmail()) {
            if (!empty($mailSettings['ccEmail']) && !is_array($mailSettings['ccEmail'])) {
                $mailSettings['ccEmail'] = [$mailSettings['ccEmail']];
            }
            $mailSettings['ccEmail'][] = $orgEmail;
        }
        $dispatchEvent = new RegistrationBeforeEmailEvent($registration, $mailSettings);
        if ($event->getStatus()->isWaitlistOpen()) {
            $dispatchEvent->setTemplateEmailCustomer(MailService::TEMPLATE_KEY_REGISTRATION_EMAIL_CUSTOMER_WAITLIST);
        }
        $this->eventDispatcher->dispatch($dispatchEvent);
        $mailSettings = $dispatchEvent->getMailSettings();
        $attachFiles = $dispatchEvent->getAttachedFiles();
        $mailService = $this->mailService;
        $mailService->sendRegistrationMail(
            $registration,
            $registration->getEmail(),
            $mailSettings,
            $dispatchEvent->getTemplateEmailCustomer(),
            null,
            null,
            false,
            $attachFiles
        );

        // Send email to admin
        $adminMailSettings = $mailSettings;
        $adminMailSettings['is_admin_email'] = true;
        $this->addOrganizerEmailIfEnabled($registration, $adminMailSettings);
        $dispatchEvent = new RegistrationBeforeEmailEvent($registration, $adminMailSettings);
        $this->eventDispatcher->dispatch($dispatchEvent);
        $adminMailSettings = $dispatchEvent->getMailSettings();
        $attachFiles = $dispatchEvent->getAttachedFiles();
        $mailService->sendRegistrationMail(
            $registration,
            $adminMailSettings['adminEmail'],
            $adminMailSettings,
            MailService::TEMPLATE_KEY_REGISTRATION_EMAIL_SERVICE,
            null,
            null,
            false,
            $attachFiles
        );
    }

    /**
     * Add organizer ass cc if feature is enabled and organizer email is valid
     *
     * @param Registration $registration
     * @param array $mailSettings
     */
    private function addOrganizerEmailIfEnabled(Registration $registration, array &$mailSettings)
    {
        if (TcaUtility::getExtConf('enableEmailToOrganizer')
            && (null !== $event = $registration->getEvent())
            && null !== $organizer = $event->getOrganizer()) {
            $organizerEmail = $organizer->getEmail();
            if (GeneralUtility::validEmail($organizerEmail) && $organizerEmail !== $registration->getEmail()) {
                if (!empty($mailSettings['ccEmail']) && !is_array($mailSettings['ccEmail'])) {
                    $mailSettings['ccEmail'] = [$mailSettings['ccEmail']];
                }
                $mailSettings['ccEmail'][] = $organizerEmail;
            }
        }
    }

    /**
     * process action registration cancel
     *
     * @param string|null $hash
     * @param string|null $email
     * @param array $settings
     *
     * @return Registration The registration to be cancelled
     *
     * @throws RegistrationCancellationException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException
     */
    public function processCancelAction(?string $hash, ?string $email, array $settings): Registration
    {
        $registration = empty($hash) ? null : $this->registrationRepository->findOneByHash($hash);
        /** @var Registration|null $registration */
        if (null !== $registration && $registration->getCanBeCancelled() && null !== $event = $registration->getEvent()) {
            if (!empty($email)) {
                if ($registration->getEmail() !== $email) {
                    $exception = new RegistrationCancellationException(
                        'Registration not found or cannot be cancelled',
                        RegistrationCancellationException::CODE_INCORRECT_EMAIL
                    );
                    $exception->setMessageLabelKey('message_email_does_not_match');
                    throw $exception;
                }
                if ($registration->getStatus() === Registration::STATUS_CANCELLED) {
                    $exception = new RegistrationCancellationException(
                        'Registration not found or cannot be cancelled',
                        RegistrationCancellationException::CODE_ALREADY_CANCELLED
                    );
                    $exception->setMessageLabelKey('message_registration_already_cancelled');
                    throw $exception;
                }
                $registration->setStatus(Registration::STATUS_CANCELLED);
                $this->registrationRepository->update($registration);
                /** @var PersistenceManager $persistenceManager */
                $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
                $persistenceManager->persistAll();
                $waitlistService = $this->waitlistService;
                $waitlistService->updateRegistrationCountAndWaitlist($event->getUid());
                $persistenceManager->persistAll();
                $adminMailSettings = $settings['mail'];
                $adminMailSettings['is_admin_email'] = true;
                $this->addOrganizerEmailIfEnabled($registration, $adminMailSettings);
                $dispatchEvent = new CancellationBeforeEmailEvent($registration, $adminMailSettings);
                $this->eventDispatcher->dispatch($dispatchEvent);
                $adminMailSettings = $dispatchEvent->getMailSettings();
                $attachFiles = $dispatchEvent->getAttachedFiles();
                $mailService = $this->mailService;
                $mailService->sendRegistrationMail(
                    $registration,
                    $adminMailSettings['adminEmail'],
                    $adminMailSettings,
                    MailService::TEMPLATE_KEY_REGISTRATION_CANCEL_EMAIL_SERVICE,
                    null,
                    null,
                    false,
                    $attachFiles
                );
            }

            return $registration;
        }
        throw new RegistrationCancellationException(
            'Registration not found or cannot be cancelled',
            RegistrationCancellationException::CODE_CANNOT_BE_CANCELLED
        );
    }

    /**
     * Additional validation for registration values
     * If any value is invalid, an exception is thrown
     *
     * @param Registration $registration
     * @param array $settings
     * @throws RegistrationValidationException
     */
    protected function assertRegistrationValid(Registration $registration, array $settings): void
    {
        $email = $registration->getEmail();
        $firstName = $registration->getFirstName();
        $lastName = $registration->getLastName();
        $acceptTerms = $registration->isAgreeToTerms();
        if (empty($email) || empty($firstName)
            || empty($lastName)
            || empty($acceptTerms)) {
            throw new RegistrationValidationException(
                'Form is not valid',
                RegistrationValidationException::CODE_MISSING_REQUIRED_FIELDS
            );
        }

        // Redirect if the username exists
        if ($settings['checkUniqueRegistrationEmailForEvent']
            && null !== $this->registrationRepository->findDuplicate($registration)) {
            $exception = new RegistrationValidationException(
                'Form data are not unique',
                RegistrationValidationException::CODE_NOT_UNIQUE
            );
            $exception->setMessageLabelKey('message_already_registered');
            throw $exception;
        }

        if (!GeneralUtility::validEmail($email)) {
            $exception = new RegistrationValidationException(
                'Email is not valid',
                RegistrationValidationException::CODE_NOT_UNIQUE
            );
            $exception->setMessageLabelKey('message_invalid_email');
            throw $exception;
        }
    }
}
