<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Hook;

use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Service\EventService;
use KDN\KdnEvents\Utility\DbUtility;
use KDN\KdnEvents\Utility\TcaUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Data handler hooks
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class CoreDataHandler
{

    /**
     * @var integer
     */
    private $eventId;

    /**
     * @var EventService
     */
    private $eventService;

    /**
     * This method is called by a hook in the TYPO3 Core Engine (TCEmain) when a record is saved. We use it to disable
     * saving of the current record if it has categories assigned that are not allowed for the BE user.
     *
     * @param array $fieldArray : The field names and their values to be processed (passed by reference)
     * @param string $table : The table TCEmain is currently processing
     * @param string $id : The records id (if any)
     * @param object $pObj : Reference to the parent object (TCEmain)
     * @return    void
     * @access public
     */
    public function processDatamap_preProcessFieldArray(&$fieldArray, $table, $id, &$pObj)
    {
        if ($table === TcaUtility::TABLE_EVENT_TIMES) {
            /**
             * Make sure event end dates are greater or equal to start dates
             */
            if (!empty($fieldArray['time_start']) && !empty($fieldArray['time_end'])
                && $fieldArray['time_end'] < $fieldArray['time_start']) {
                unset($fieldArray['time_end']);
            }
        }
        if ($table === TcaUtility::TABLE_EVENTS) {
            if (!empty($fieldArray['time_start']) && !empty($fieldArray['time_end'])
                && $fieldArray['time_end'] < $fieldArray['time_start']) {
                unset($fieldArray['time_end']);
            }
            /**
             * Make sure registration end dates are greater or equal to start dates
             */
            if (!empty($fieldArray['registration_start']) && !empty($fieldArray['registration_end'])
                && $fieldArray['registration_end'] < $fieldArray['registration_start']) {
                unset($fieldArray['registration_end']);
            }
            $this->setWorkAreasEnabledField($fieldArray);
        }
        if ($table === TcaUtility::TABLE_REGISTRATIONS) {
            // Must be set here, because $fieldArray in processDatamap_afterDatabaseOperations is empty
            // if event was not changed!
            $this->eventId = $fieldArray['event'];
            $origRegistration = $this->getEventService()->fetchRegistrationRow($id);

            if (!empty($origRegistration)
                && (
                    ((int)$fieldArray['status'] === Registration::STATUS_CANCELLED
                        && (int)$origRegistration['status'] !== Registration::STATUS_CANCELLED)
                    || ($fieldArray['hidden'] && !$origRegistration['hidden']))) {
                $cancelledRecord = array_merge($origRegistration, $fieldArray);
                $this->getEventService()->updateRegistrationWaitlist($cancelledRecord);
            }
        }
    }

    /**
     * Enable the work area optional field if work area choices have been selected
     * @param array $fieldArray
     */
    private function setWorkAreasEnabledField(&$fieldArray)
    {
        $optFields = $fieldArray['enable_optional_fields'];
        $optIsArray = is_array($optFields);
        if (!$optIsArray) {
            $optFields = $optFields ? GeneralUtility::trimExplode(',', $optFields) : [];
        }
        $key = array_search('work_areas', $optFields, false);
        if (!empty($fieldArray['enabled_work_areas'])) {
            if (false === $key) {
                $optFields[] = 'work_areas';
            }
        } elseif (false !== $key) {
            unset($optFields[$key]);
        }
        $fieldArray['enable_optional_fields'] = $optIsArray ? $optFields : implode(',', $optFields);
    }

    /**
     * Hook: processDatamap_afterDatabaseOperations
     * (calls $hookObj->processDatamap_afterDatabaseOperations($status, $table, $id, $fieldArray, $this);)
     *
     * Note: When using the hook after INSERT operations, you will only get the temporary NEW... id passed to your hook as $id,
     *         but you can easily translate it to the real uid of the inserted record using the $this->substNEWwithIDs array.
     *
     * @param string $status : (reference) Status of the current operation, 'new' or 'update
     * @param string $table : (reference) The table currently processing data for
     * @param string $id : (reference) The record uid currently processing data for, [integer] or [string] (like 'NEW...')
     * @param array $fieldArray : (reference) The field array of a record
     * @param DataHandler $tceMain TCEmain parent object
     * @return    void
     */
    public function processDatamap_afterDatabaseOperations($status, $table, $id, &$fieldArray, DataHandler $tceMain)
    {
        //prevent file uploads from being deleted if they change
        $recordId = (int)$id;
        if ($recordId > 0) {
            if ($table === TcaUtility::TABLE_REGISTRATIONS) {
                $this->eventId = $this->getEventIdByRegistrationId($recordId);
            } elseif ($table === TcaUtility::TABLE_EVENTS) {
                $this->eventId = $recordId;
            } elseif ($table === TcaUtility::TABLE_EVENT_TIMES) {
                $row = DbUtility::fetchRowByFields(TcaUtility::TABLE_EVENT_TIMES, ['uid' => $recordId]);
                if (!empty($row['event'])) {
                    $this->eventId = (int)$row['event'];
                }
            }
        }
    }

    public function processDatamap_afterAllOperations(DataHandler $tceMain)
    {
        if (!empty($this->eventId)) {
            $this->getEventService()->updateEventRegistrationCount($this->eventId);
            $this->getEventService()->updateEventTimespan($this->eventId);
        }
    }


    public function processCmdmap_deleteAction($table, $id, $recordToDelete, $recordWasDeleted, $dataHandler)
    {

        if ($table === TcaUtility::TABLE_REGISTRATIONS && (int)$recordToDelete['status'] === Registration::STATUS_REGISTERED) {
            $this->getEventService()->updateRegistrationWaitlist($recordToDelete);
        }
    }

    /**
     * @param string $command Command name
     * @param string $table Table name
     * @param string $id
     * @param string $value
     * @param DataHandler $tceMain
     */
    public function processCmdmap_postProcess($command, $table, $id, $value, DataHandler $tceMain)
    {
        if ($table === TcaUtility::TABLE_REGISTRATIONS) {
            $eventId = $this->getEventIdByRegistrationId((int)$id);
            if ($eventId > 0) {
                $this->getEventService()->updateEventRegistrationCount($eventId);
            }
        }
    }

    private function getEventIdByRegistrationId(int $registrationId): int
    {
        $registration = $this->getEventService()->fetchRegistrationRow($registrationId);
        if (!empty($registration)) {
            return (int)$registration['event'];
        }
        return 0;
    }

    /**
     * @return EventService
     */
    public function getEventService(): EventService
    {
        if (null === $this->eventService) {
            /** @var ObjectManager $objectManager */
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
            /** @var EventService $eventService */
            $eventService = $objectManager->get(EventService::class);
            $this->eventService = $eventService;
        }
        return $this->eventService;
    }

}
