<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Hook;

use KDN\KdnEvents\Utility\ExtendedFieldChoicesUtility;
use KDN\KdnEvents\Utility\ObjectUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility as BackendUtilityCore;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Userfunc to render alternative label for media elements
 */
class ItemsProcFunc
{

    /**
     * @var ExtendedFieldChoicesUtility
     */
    protected $extendedFieldChoicesUtility;

    public function __construct()
    {
        $this->extendedFieldChoicesUtility = GeneralUtility::makeInstance(ExtendedFieldChoicesUtility::class);
    }

    /**
     * Itemsproc function to extend the selection of event types in the plugin
     *
     * @param array &$config configuration array
     */
    public function user_eventTypes(array &$config): void
    {
        $pageId = $this->getPageId($config['flexParentDatabaseRow']['pid']);

        if ($pageId > 0) {
            $templateLayouts = $this->extendedFieldChoicesUtility->getAvailableTemplateLayouts($pageId);

            foreach ($templateLayouts as $layout) {
                $additionalLayout = [
                    htmlspecialchars(ObjectUtility::getLanguageService()->sL($layout[0])),
                    $layout[1]
                ];
                $config['items'][] = $additionalLayout;
            }
        }
    }

    /**
     * Get page id, if negative, then it is a "after record"
     *
     * @param int $pid
     * @return int
     */
    private function getPageId($pid): int
    {
        $pid = (int)$pid;

        if ($pid > 0) {
            return $pid;
        }

        $row = BackendUtilityCore::getRecord('tt_content', abs($pid), 'uid,pid');
        return $row['pid'];
    }
}
