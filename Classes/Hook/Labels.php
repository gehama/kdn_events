<?php


/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Hook;

/**
 * TCA label hooks
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Labels
{
    /**
     * Generate event label
     *
     * @param array $params
     */
    public function getEventLabel(array &$params)
    {
        $title = $params['row']['title'];
        if (!empty($params['row']['event_start'])) {
            $title .= ' (' . $this->formatTimespan($params['row']['event_start'], $params['row']['event_end']) . ')';
        }
        $params['title'] = $title;
    }

    /**
     * Generate event registration label
     *
     * @param array $params
     */
    public function getEventRegistrationLabel(array &$params)
    {
        $title = trim($params['row']['first_name'] . ' ' . $params['row']['last_name']);
        $title .= ' ('.$params['row']['email'].')';
        $params['title'] = $title;
    }

    /**
     * Generate event invitation label
     *
     * @param array $params
     */
    public function getEventInvitationLabel(array &$params)
    {
        $title = trim($params['row']['first_name'] . ' ' . $params['row']['last_name']);
        $title .= ' ('.$params['row']['email'].')';
        $params['title'] = $title;
    }

    /**
     * Generate event time label
     *
     * @param array $params
     */
    public function getEventTimeLabel(array &$params)
    {
        $params['title'] = $this->formatTimespan($params['row']['time_start'], $params['row']['time_end']);
    }

    private function formatTimespan($start, $end)
    {
        $intStart = (int) $start;
        $intEnd = (int) $end;
        $text = $start;
        if ($intStart > 0) {
            $text = date('d.m.Y H:i', $intStart);
            if ($intEnd > 0 && $intEnd > $intStart) {
                $dateStart = date('Y-m-d', $intStart);
                $dateEnd = date('Y-m-d', $intEnd);
                $endFormat = $dateEnd !== $dateStart ? 'd.m.Y H:i' : 'H:i';
                $text .= ' - ' . date($endFormat, $intEnd);
            }
        }
        return $text;
    }
}