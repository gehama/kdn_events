<?php
namespace KDN\KdnEvents\Utility;

/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 ***/

if (!class_exists('TCPDF')) {
// tell TCPDF not to use default config
    define("K_TCPDF_EXTERNAL_CONFIG", true);

    require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('kdn_events').'Configuration/TcpdfConfig.php');

    $libBaseDir = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('kdn_events') . 'Resources/Private/PHP/tcpdf/';
    require_once($libBaseDir.'tcpdf.php');
//require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('kdn_events').'Resources/Private/PHP/fpdi/fpdi.php');

    class_exists('TCPDF', true); // trigger Composers autoloader to load the TCPDF class
}

class Pdf extends \TCPDF
{

    /**
     * Add page header
     */
    public function Header() {
    }

    /**
     * Add page header
     */
    public function Footer() {
    }
}