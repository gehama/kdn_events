<?php
/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2021 Gert Hammes <info@gerthammes.de>
 *
 ***/

namespace KDN\KdnEvents\Utility;

use IntlDateFormatter;


/**
 * Class TimeFormatUtility
 */
class TimeFormatUtility
{

    /**
     * Render the supplied DateTime object as a formatted date.
     *
     * @param string|\DateTime $dateStart
     * @param string|\DateTime $dateEnd
     * @param string|null $format
     * @param bool $showTime
     * @return string Formatted date timespan
     * @throws \Exception
     */
    public function render($dateStart, $dateEnd, ?string $format = null, bool $showTime = true)
    {
        $start = $this->getDateTimeObj($dateStart);
        $end = $this->getDateTimeObj($dateEnd);
        if (empty($format)) {
            $format = '%A, %d. %B %Y';
        }
        $startDay = $this->format($start, $format, '');
        $endDay = $this->format($end, $format, '');
        $endsOnDifferentDay = $endDay !== $startDay;
        $dateStr = $startDay;
        if ($showTime) {
            $startTime = $this->format($start, '', '%H:%M');
            $endTime = $this->format($end, '', '%H:%M');
            if ($startTime !== '00:00' || $endTime !== $startTime) {
                if ($endsOnDifferentDay) {
                    $endTime = $endDay . ', ' . $endTime;
                }
                $timeAppend = $endTime !== $startTime ? ' &ndash; ' . $endTime : '';
                $dateStr .= ', ' . $startTime . $timeAppend;
            } elseif ($endsOnDifferentDay) {
                $dateStr .= ' &ndash; ' . $endDay;
            }
        } elseif ($endsOnDifferentDay) {
            $dateStr .= ' &ndash; ' . $endDay;
        }
        return $dateStr;
    }

    /**
     * Formats the date in the given format
     *
     * @param \DateTime $date
     * @param string $dateFormat
     * @param string $timeFormat
     * @return string
     */
    private function format(\DateTime $date, $dateFormat, $timeFormat): string
    {
        $format = trim($dateFormat . ' ' . $timeFormat);
        if (strpos($format, '%') !== FALSE) {
            if (class_exists('IntlDateFormatter')) {
                $pattern = str_replace(
                    ['%A', '%d', '%m', '%B', '%Y', '%H', '%M', '%S'],
                    ['EEEE', 'dd', 'MM', 'MMMM', 'YYYY', 'HH', 'mm', 'ss',],
                    $format
                );
                $dateType = IntlDateFormatter::FULL;
                if (empty($timeFormat)) {
                    $timeType = IntlDateFormatter::NONE;
                } else {
                    $timeType = IntlDateFormatter::FULL;
                }
                $timezone = date_default_timezone_get();
                if (empty($timezone)) {
                    $timezone = 'Europe/Berlin';
                }
                $fmt = new IntlDateFormatter(
                    'de-DE',
                    $dateType,
                    $timeType,
                    $timezone,
                    IntlDateFormatter::GREGORIAN,
                    $pattern
                );
                return $fmt->format($date);
            }
            return strftime($format, $date->format('U'));
        }
        return $date->format($format);
    }

    /**
     * Create a datetime object from the given parameter (that may be a string or already a DateTime object)
     *
     * @param string|\DateTime $date
     * @return \DateTime|bool
     */
    private function getDateTimeObj($date)
    {
        if (!$date instanceof \DateTime) {
            if (is_numeric($date) && (int) $date > strtotime('2000-01-01')) {
                $date = date_create('@' . (int) $date);
            } else {
                $date = date_create((string)$date);
            }
            $date->setTimezone(new \DateTimeZone(date_default_timezone_get()));
        }
        return $date;
    }
}
