<?php
/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 ***/

namespace KDN\KdnEvents\Utility;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * ExtendedFieldChoicesUtility utility class
 */
class ExtendedFieldChoicesUtility implements SingletonInterface
{
    private const EVENT_TYPE_TSCONFIG_KEY = 'eventTypes';

    /**
     * Get available field choices for a certain page
     *
     * @param int $pageUid
     * @param string $field
     * @return array
     */
    public function getAvailableTemplateLayouts(int $pageUid, string $field = self::EVENT_TYPE_TSCONFIG_KEY): array
    {
        $tcaChoices = [];

        $extName = TcaUtility::getExtName();
        // Check if the choices are extended by ext_tables
        if (isset($GLOBALS['TYPO3_CONF_VARS']['EXT'][$extName][$field])
            && is_array($GLOBALS['TYPO3_CONF_VARS']['EXT'][$extName][$field])
        ) {
            $tcaChoices = $GLOBALS['TYPO3_CONF_VARS']['EXT'][$extName][$field];
        }

        // Add TsConfig values
        foreach ($this->getFieldChoicesFromTsConfig($pageUid, $field) as $choiceKey => $choiceLabel) {
            if (GeneralUtility::isFirstPartOfStr($choiceLabel, '--div--')) {
                $optGroupParts = GeneralUtility::trimExplode(',', $choiceLabel, true, 2);
                $choiceLabel = $optGroupParts[1];
                $choiceKey = $optGroupParts[0];
            }
            $tcaChoices[] = [$choiceLabel, $choiceKey];
        }

        return $tcaChoices;
    }

    /**
     * Get the field value choices from the page TSConfig
     *
     * @param int $pageUid
     * @param string $field
     * @return array
     */
    private function getFieldChoicesFromTsConfig(int $pageUid, string $field)
    {
        $templateLayouts = [];
        $pagesTsConfig = BackendUtility::getPagesTSconfig($pageUid);
        $key = $field . '.';
        if (isset($pagesTsConfig[TcaUtility::EXT_CONFIG_KEY][$key])
            && is_array($pagesTsConfig[TcaUtility::EXT_CONFIG_KEY][$key])) {
            $templateLayouts = $pagesTsConfig[TcaUtility::EXT_CONFIG_KEY][$key];
        }
        return $templateLayouts;
    }
}
