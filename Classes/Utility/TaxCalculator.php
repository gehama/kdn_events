<?php
/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 ***/

namespace KDN\KdnEvents\Utility;

/**
 * Class TaxCalculator
 */
class TaxCalculator
{
    public const TAX_RATE_DEFAULT = 19;
    public const TAX_RATE_CUSTOM = 16;
    public const TAX_RATE_LOW = 7;
    public const TAX_RATE_LOW_CUSTOM = 5;

    public static function calculateTaxes($amount, int $taxType = null, $performanceTimestamp = null)
    {
        return max(0, $amount - self::calculateAmountNet($amount, $taxType, $performanceTimestamp));
    }

    public static function calculateAmountNet($amount, int $taxType = null, $performanceTimestamp = null)
    {
        $taxValue = self::getTaxValue($taxType, $performanceTimestamp);
        if ($taxValue > 0) {
            $taxMultiplier = (100 + $taxValue) / 100;
            return round($amount / $taxMultiplier, 2);
        }
        return $amount;
    }

    public static function getTaxValue(int $taxType = null, $performanceTimestamp = null): int
    {
        $taxValue = $taxType ?? self::TAX_RATE_DEFAULT;
        if ($taxValue > 0 && $taxValue < 1) {
            $taxValue *= 100;
        }
        $minCheckTstamp = mktime(0, 0, 0, 7, 1, 2020);
        $checkTime = !empty($performanceTimestamp) && $performanceTimestamp > strtotime('2010-01-01') ? $performanceTimestamp : time();
        if ($checkTime >= $minCheckTstamp
            && $checkTime < mktime(0, 0, 0, 1, 1, 2021)) {
            if ($taxValue === self::TAX_RATE_DEFAULT) {
                $taxValue = self::TAX_RATE_DEFAULT;
            } elseif ($taxValue === self::TAX_RATE_LOW) {
                $taxValue = self::TAX_RATE_LOW_CUSTOM;
            }
        }
        return $taxValue;
    }
}
