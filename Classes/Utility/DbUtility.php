<?php
/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 ***/

namespace KDN\KdnEvents\Utility;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;


/**
 * Class DbUtility
 */
class DbUtility
{

    public static function getConnectionForTable(string $table): Connection
    {
        return GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
    }

    public static function getQueryBuilderForTable(string $table): QueryBuilder
    {
        return self::getConnectionForTable($table)->createQueryBuilder();
    }

    /**
     * Load record from database
     *
     * @param string $table
     * @param array $identifiers
     * @param array $orderBy
     * @param string[] $extraConditions
     * @return array Table row array; null if empty
     */
    public static function fetchRowByFields($table, $identifiers, $orderBy = [], $extraConditions = []): ?array
    {
        $queryBuilder = self::getQueryBuilderForTable($table);
        $queryBuilder
            ->select('*')
            ->from($table)
            ->setMaxResults(1);
        $queryBuilder->getRestrictions()->removeByType(HiddenRestriction::class);
        foreach ($identifiers as $field => $value) {
            $type = $field === 'uid' && is_int($value) ? \PDO::PARAM_INT : \PDO::PARAM_STR;
            $expr = $queryBuilder->expr()->eq($field, $queryBuilder->createNamedParameter($value, $type));
            $queryBuilder->andWhere($expr);
        }
        foreach ($extraConditions as $condition) {
            $queryBuilder->andWhere($condition);
        }
        foreach ($orderBy as $field => $order) {
            $queryBuilder->addOrderBy($field, $order);
        }
        $row = $queryBuilder->execute()->fetch();
        if (empty($row)) {
            return null;
        }
        return $row;
    }
}
