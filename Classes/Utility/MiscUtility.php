<?php
/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 ***/

namespace KDN\KdnEvents\Utility;

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Context\TypoScriptAspect;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\CMS\Core\Routing\PageArguments;
use TYPO3\CMS\Core\Routing\SiteMatcher;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;


/**
 * Class MiscUtility
 */
class MiscUtility
{

    /**
     * @param int|null $pageUid
     * @return TypoScriptFrontendController
     * @throws \TYPO3\CMS\Core\Error\Http\ServiceUnavailableException
     * @see \TYPO3\CMS\Frontend\Typolink\AbstractTypolinkBuilder::getTypoScriptFrontendController
     */
    public static function initTypoScriptFrontendController(int $pageUid = null): TypoScriptFrontendController
    {
        if (!isset($GLOBALS['TSFE'])) {

            /** @var SiteFinder $siteFinder */
            $site = null;
            if ($pageUid) {
                $siteMatcher = GeneralUtility::makeInstance(SiteMatcher::class);
                $site = $siteMatcher->matchByPageId($pageUid);
            }
            if (null === $site) {
                $siteFinder = GeneralUtility::makeInstance(SiteFinder::class);
                $site = current($siteFinder->getAllSites());
            }
            if (!isset($GLOBALS['TYPO3_REQUEST'])) {

                $request = new ServerRequest(
                    $site->getBase(),
                    'GET'
                );
                $GLOBALS['TYPO3_REQUEST'] = $request;
            } else {
                $request = $GLOBALS['TYPO3_REQUEST'];
            }
            /** @var Context $context */
            $context = GeneralUtility::makeInstance(Context::class);
            $context->setAspect('typoscript', new TypoScriptAspect(true));
            $pageArguments = new PageArguments($site->getRootPageId(), 0, [], []);
            /** @var TypoScriptFrontendController $tsFe */
            $tsFe = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
                TypoScriptFrontendController::class,
                $context,
                $site,
                $site->getDefaultLanguage(),
                $pageArguments
            );

            $tsFe->sys_page = GeneralUtility::makeInstance(PageRepository::class, $context);
            $tsFe->getPageAndRootlineWithDomain($site->getRootPageId(), $request);
            $tsFe->id = $site->getRootPageId();
            $tsFe->getConfigArray();
            $GLOBALS['TSFE'] = $tsFe;
        } else {
            /** @var TypoScriptFrontendController $tsFe */
            $tsFe = $GLOBALS['TSFE'];
        }
        if ($pageUid) {
            $tsFe->id = $pageUid;
        }
        return $tsFe;
    }

    /**
     * Returns the id of the frontend user or 0 if no user is set
     *
     * @return int
     */
    public static function getFrontendUserId(): int
    {
        return (int)self::getFrontendUserAspect('id');
    }

    /**
     * Returns the value for the given user property
     *
     * @param string $propertyName
     * @return mixed
     * @see https://docs.typo3.org/m/typo3/reference-coreapi/master/en-us/ApiOverview/Context/Index.html#user-aspect
     */
    public static function getFrontendUserAspect(string $propertyName)
    {
        $context = GeneralUtility::makeInstance(Context::class);
        try {
            $value = $context->getPropertyFromAspect('frontend.user', $propertyName);
        } catch (AspectNotFoundException $e) {
            $value = null;
            // do nothing, no frontend user is logged in
        }
        return $value;
    }

    /**
     * Returns the cache key part for the current context
     * @param bool $userCache
     * @return string
     */
    public static function getCacheContextKey($userCache = true): string
    {
        $languageUid = self::getCurrentLanguageId();
        $key = 'l' . $languageUid;
        if ($userCache) {
            $userKey = self::getFrontendUserId() . SessionUtility::getUserProperty('tstamp');
            $key .= '-fu' . $userKey;
        }
        return $key;
    }

    /**
     * Get current language
     *
     * @return int
     */
    public static function getCurrentLanguageId(): int
    {
        $context = GeneralUtility::makeInstance(Context::class);
        try {
            $languageId = (int)$context->getPropertyFromAspect('language', 'contentId');
        } catch (AspectNotFoundException $e) {
            $languageId = 0;
            // do nothing
        }

        return $languageId;
    }
}
