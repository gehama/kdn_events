<?php
/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 ***/

namespace KDN\KdnEvents\Utility;

use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Class ObjectUtility
 */
class ObjectUtility
{

    /**
     * @return TypoScriptFrontendController
     */
    public static function getTyposcriptFrontendController()
    {
        return MiscUtility::initTypoScriptFrontendController();
    }

    /**
     * @return ObjectManager
     */
    public static function getObjectManager()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        return $objectManager;
    }

    /**
     * @return ContentObjectRenderer
     */
    public static function getContentObject()
    {
        /** @var ContentObjectRenderer $contentObject */
        $contentObject = self::getObjectManager()->get(ContentObjectRenderer::class);
        return $contentObject;
    }

    /**
     * @return array
     */
    public static function getFilesArray()
    {
        return (array)$_FILES;
    }

    /**
     * @return LanguageService
     */
    public static function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @param string $className
     * @return Logger
     */
    public static function getLogger($className)
    {
        return GeneralUtility::makeInstance(LogManager::class)->getLogger($className);
    }
}