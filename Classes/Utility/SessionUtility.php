<?php
/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 ***/

namespace KDN\KdnEvents\Utility;

use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;


/**
 * Class SessionUtility
 */
class SessionUtility
{
    private const SESSION_TYPE_USER = 'user';
    private const SESSION_TYPE_SESSION = 'ses';

    /**
     * @return FrontendUserAuthentication|null
     */
    public static function getFeUser(): ?FrontendUserAuthentication
    {
        $feUser = ObjectUtility::getTyposcriptFrontendController()->fe_user;
        if ($feUser instanceof FrontendUserAuthentication) {
            return $feUser;
        }
        return null;
    }

    /**
     * Returns the value for the given user property
     * @param string $property The user property (e.g. email, last name etc.)
     * @return mixed|null
     */
    public static function getUserProperty(string $property)
    {
        // Should maybe be loaded from the Context
        return self::getFeUser()->user[$property] ?? null;
    }

    /**
     * @param string $sessionKey
     * @param bool $persistIfAuthenticated Only use user storage if values shall be persisted
     * @param bool $fallbackUserToSession If user value is null, load the value from the session
     * @return mixed|null
     */
    public static function get(
        string $sessionKey,
        bool $persistIfAuthenticated = true,
        bool $fallbackUserToSession = true
    ) {
        $userAuth = self::getFeUser();
        $sessionType = self::getSessionType($persistIfAuthenticated);
        $value = $userAuth->getKey($sessionType, $sessionKey);
        // Fallback to session data if user data are not set
        if (empty($value) && $fallbackUserToSession
            && $persistIfAuthenticated && $sessionType === self::SESSION_TYPE_USER) {
            $value = $userAuth->getKey(self::SESSION_TYPE_SESSION, $sessionKey);
            // Reset session value and store current value in user data
            if (!empty($value)) {
                self::set($sessionKey, $value, false);
                self::set($sessionKey, $value);
            }
        }
        return $value;
    }

    /**
     * @param string $sessionKey
     * @param array $default Default value
     * @param bool $persistIfAuthenticated Only use user storage if values shall be persisted
     * @return array
     */
    public static function getArray(string $sessionKey, $default = [], bool $persistIfAuthenticated = true): array
    {
        $sessionData = self::get($sessionKey, $persistIfAuthenticated);
        if (null !== $sessionData && is_array($sessionData)) {
            $sanitizedSessionData = $sessionData;
        } else {
            $sanitizedSessionData = $default;
        }
        return $sanitizedSessionData;
    }

    /**
     * Returns the current session id
     * @return string
     */
    public static function getSessionId(): string
    {
        $userAuth = self::getFeUser();
        return $userAuth->getSessionId();
    }

    /**
     *
     * @param string $sessionKey
     * @param mixed $data
     * @param bool $persistIfAuthenticated Only use user storage if values shall be persisted
     * @return void
     */
    public static function set(string $sessionKey, $data, bool $persistIfAuthenticated = true): void
    {
        $userAuth = self::getFeUser();
        $userAuth->setKey(self::getSessionType($persistIfAuthenticated), $sessionKey, $data);
        $userAuth->storeSessionData();
    }

    /**
     * Returns the session storage type
     *
     * @param bool $persistIfAuthenticated Only use user storage if values shall be persisted
     * @return string
     */
    private static function getSessionType(bool $persistIfAuthenticated = true): string
    {
        return $persistIfAuthenticated && self::isFrontendLogin() ? self::SESSION_TYPE_USER : self::SESSION_TYPE_SESSION;
    }

    /**
     * Check if FE user is logged in
     *
     * @return bool
     */
    public static function isFrontendLogin(): bool
    {
        $context = GeneralUtility::makeInstance(Context::class);
        return $context->getPropertyFromAspect('frontend.user', 'isLoggedIn', false);
    }
}
