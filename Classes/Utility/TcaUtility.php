<?php
/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 ***/

namespace KDN\KdnEvents\Utility;

use RuntimeException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\TypoScript\TemplateService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\RootlineUtility;


/**
 * Class TcaUtility
 */
class TcaUtility
{
    public const EXT_NAME = 'kdn_events';
    public const EVENTS_PLUGIN_NAME = 'Items';
    public const EXT_CONFIG_KEY = 'tx_kdnevents.';


    // some abbreviations for the db table names
    public const TABLE_EVENTS = 'tx_kdnevents_domain_model_event';
    public const TABLE_REGISTRATIONS = 'tx_kdnevents_domain_model_registration';
    public const TABLE_EVENT_TIMES = 'tx_kdnevents_domain_model_time';
    public const TABLE_INVITATIONS = 'tx_kdnevents_domain_model_invitation';

    /**
     * @return string
     */
    public static function getExtName()
    {
        return GeneralUtility::underscoredToUpperCamelCase(self::EXT_NAME);
    }

    /**
     * @return string
     */
    public static function getPluginName()
    {
        return self::EVENTS_PLUGIN_NAME;
    }


    /**
     * @param string $type
     * @param string $scope
     * @return string
     */
    public static function getLLPrefix($type = '', $scope = '')
    {
        $filenameSuffix = ('' !== $type) ? '_' . $type : '';
        if ('' !== $scope) {
            $scope .= '.';
        }
        return 'LLL:EXT:' . self::EXT_NAME . '/Resources/Private/Language/locallang' . $filenameSuffix . '.xlf:' . $scope;
    }

    /**
     * Returns configuration for given key as defined in ext_conf_template.txt
     *
     * @param string $path
     * @return bool|string|array
     */
    public static function getExtConf(string $path = '')
    {
        /** @var ExtensionConfiguration $extensionConfiguration */
        $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        try {
            return $extensionConfiguration->get(self::EXT_NAME, $path);
        } catch (ExtensionConfigurationExtensionNotConfiguredException $e) {
            unset($e);
        } catch (ExtensionConfigurationPathDoesNotExistException $e) {
            unset($e);
        }
        return false;
    }

    /**
     * Returns the domain name that is e.g. used for console commands
     *
     * @param string|bool $protocol
     * @return bool|string|array
     */
    public static function getDomain($protocol = 'https'): string
    {
        try {
            $currentHost = (string)GeneralUtility::getIndpEnv('HTTP_HOST');
        } catch (\UnexpectedValueException $e) {
            $currentHost = '';
        }
        if (empty($currentHost)) {
            $currentHost = str_replace(['https://', 'http://', 'ssl://',], '', trim((string) self::getExtConf('domain')));
        }
        if ($currentHost) {
            if (true === $protocol) {
                $reqHost = (string)GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST');
                if ($reqHost) {
                    $currentHost = $reqHost;
                } else {
                    $currentHost = 'https://' . $currentHost;
                }
            }
            if (false !== $protocol) {
                $currentHost = $protocol . '://' . $currentHost;
            }
            $currentHost = rtrim($currentHost, '/') . '/';
        }
        return $currentHost;
    }

    /**
     * @param string $identifier
     * @param bool $userCache
     * @return string
     */
    public static function getCacheIdentifier($identifier, $userCache = true)
    {
        $contextKey = MiscUtility::getCacheContextKey($userCache);
        return self::EXT_NAME . '-' . $contextKey . '-cache-' . $identifier;
    }

    /**
     * Returns the template service for the given page
     *
     * @param int|null $pageUid
     *
     * @return TemplateService
     */
    public static function getTemplateServiceForPage(?int $pageUid): TemplateService
    {
        // Get the root line
        $rootLine = [];
        if ((int)$pageUid > 0) {
            try {
                $rootLine = GeneralUtility::makeInstance(RootlineUtility::class, (int)$pageUid)->get();
            } catch (RuntimeException $e) {
                $rootLine = [];
            }
        }
        /** @var TemplateService $template */
        $template = GeneralUtility::makeInstance(TemplateService::class);
        // do not log time-performance information
        $template->tt_track = false;
        // Explicitly trigger processing of extension static files
        $template->setProcessExtensionStatics(true);
        // This generates the constants/config + hierarchy info for the template.
        $template->runThroughTemplates($rootLine, 0);
        $template->generateConfig();
        return $template;
    }
}
