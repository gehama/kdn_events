<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\FormEngine\FieldControl;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Invitation;
use KDN\KdnEvents\Domain\Model\Time;
use KDN\KdnEvents\Service\MarkerService;
use KDN\KdnEvents\Utility\TcaUtility;
use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class EventMarkerControl extends AbstractFormElement
{
    public function render()
    {
        $pid = $this->data['effectivePid'] ?? ($this->data['parentPageRow']['pid'] ?? $this->data['databaseRow']['pid']);
        $event = new Event();
        $event->setUid(1);
        $event->setPid($pid);
        $event->setTitle('Lorem ipsum');
        $event->setDescription('Lorem ipsum sit dolor amet');
        $event->setEventType(2);
        $event->setIsTopEvent(true);
        $event->setPrice(54);
        $event->setBillTitle('Lorem ipsum Rechnung');
        $eventTimes = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $time = new Time();
        $time->setTimeStart(date_create(date('Y-m-d')));
        $time->setTimeEnd(date_create(date('Y-m-d', strtotime('+1 day'))));
        $eventTimes->attach($time);
        $event->setEventTimes($eventTimes);
        $event->setRegistrationStart(date_create(date('Y-m-d', strtotime('-10 day'))));
        $event->setRegistrationEnd(date_create(date('Y-m-d', strtotime('-1 day'))));
        $event->setMaxParticipants(30);
        $status = new \KDN\KdnEvents\Domain\Model\Status();
        $status->setName('Verfügbar');
        $event->setStatus($status);
        $event->setRegistrations(7);
        $organizer = new \KDN\KdnEvents\Domain\Model\Organizer();
        $organizer->setName('Testveranstalter');
        $organizer->setEmail('organizer@example.com');
        $event->setOrganizer($organizer);
        $location = new \KDN\KdnEvents\Domain\Model\Location();
        $location->setName('Testort');
        $location->setStreet('Teststraße 1');
        $location->setZip('12345');
        $location->setTitle('Testgelände');
        $location->setDescription('Parken hinter dem Haus.');
        $location->setRoom('23');
        $event->setLocation($location);
        $feUser = new FrontendUser();
        $feUser->setFirstName('Marie');
        $feUser->setLastName('Mustermann');
        $feUser->setEmail('example@example.com');
        $feUser->setPid($pid);
        $registration = new \KDN\KdnEvents\Domain\Model\Registration();
        $registration->setCrdate(date_create('now'));
        $registration->setEmail('example@example.com');
        $registration->setSalutation(2);
        $registration->setFirstName('Marie');
        $registration->setLastName('Mustermann');
        $registration->setPid($pid);
        $registration->setBirthDate(date_create(date('Y-m-d', strtotime('-20 years'))));
        $registration->setCustomerMessage('Lorem ipsum');
        $registration->setOccupation('Tester');
        $registration->setOrganisation('Testfirma');
        $registration->setStreet('Teststr. 1');
        $registration->setZipcode('12345');
        $registration->setTown('Testort');
        $registration->setOrganisationStreet('Teststr. 2');
        $registration->setOrganisationEmail('example2@example.com');
        $registration->setOrganisationZipcode('23456');
        $registration->setOrganisationTown('Teststadt');
        $registration->setPhone('123456');
        $registration->setWorkExperience('45');
        $registration->setAgreeToTerms(true);
        $registration->setNewsletter(true);
        $registration->setEvent($event);
        $registration->setHash(md5(time() . random_int(1, 99999999)));
        $registration->setUser($feUser);
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var MarkerService $markerService */
        $markerService = $objectManager->get(MarkerService::class);
        $markerService->setMode('BE');
        $markers = $markerService->getRegistrationMarkers($registration);
        $html = '<table class="table-legend">';
        foreach ($markers as $key => $value) {
            $html .= '<tr><td>' . $key . '</td>
            <td style="padding-left: 5px;">' . $value . '</td></tr>';
        }
        if (TcaUtility::getExtConf('enableInvitations')) {
            $invitation = new Invitation();
            $invitation->setCrdate(date_create('now'));
            $invitation->setEmail('example@example.com');
            $invitation->setSalutation(2);
            $invitation->setFirstName('Marie');
            $invitation->setLastName('Mustermann');
            $invitation->setPid($pid);
            $invitation->setPhone('123456');
            $invitation->setEvent($event);
            $invitation->setInvitedBy($feUser);
            $invitation->setHash(md5(time() . random_int(1, 99999999)));
            $invitation->setMessage('Lorem ipsum');
            $invitationMarkers = $markerService->getInvitationMarkers($invitation, null, false);
            foreach ($invitationMarkers as $key => $value) {
                $html .= '<tr><td>' . $key . '</td>
            <td style="padding-left: 5px;">' . $value . '</td></tr>';
            }
        }
        $html .= '</table>';

        $formField = '<div style="padding: 5px;">';
        $formField .= $html;
        $formField .= '</div>';
        $result = $this->initializeResultArray();
        $result['html'] = $formField;
        return $result;
    }
}