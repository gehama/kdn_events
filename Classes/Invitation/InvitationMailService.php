<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Invitation;

use KDN\KdnEvents\Domain\Model\Invitation;
use KDN\KdnEvents\Domain\Model\Time;
use KDN\KdnEvents\Service\MailService;

/**
 * Class invitation mail service
 */
class InvitationMailService extends MailService
{
    public const TEMPLATE_KEY_INVITATION_EMAIL_PROSPECT = 'invitation_email_prospect';
    public const TEMPLATE_KEY_INVITATION_EMAIL_SERVICE = 'invitation_email_service';

    /**
     * @param Invitation $invitation
     * @param string $recipient Recipient email
     * @param array $mailSettings
     * @param string $emailTemplateKey Optional email template key (instead of subject and body)
     * @param array $attachments
     * @param Time|null $eventTime
     * @return bool
     */
    public function sendInvitationMail(
        Invitation $invitation,
        $recipient,
        $mailSettings,
        $emailTemplateKey = null,
        array $attachments = [],
        Time $eventTime = null
    ): bool
    {
        $markerService = $this->markerService;
        $markerService->setMode($this->mode);
        $markers = $markerService->getInvitationMarkers($invitation, $eventTime);
        return $this->sendMarkerMail(
            $invitation,
            $markers,
            $recipient,
            $mailSettings,
            $emailTemplateKey ?? self::TEMPLATE_KEY_INVITATION_EMAIL_PROSPECT,
            null,
            null,
            false,
            $attachments
        );
    }
}
