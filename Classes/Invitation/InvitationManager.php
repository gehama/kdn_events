<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Invitation;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Invitation;
use KDN\KdnEvents\Service\ConfigurationService;
use KDN\KdnEvents\Utility\DbUtility;
use KDN\KdnEvents\Utility\MiscUtility;
use KDN\KdnEvents\Utility\TcaUtility;
use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Class invitation service
 */
class InvitationManager extends ConfigurationService
{
    public const ENABLE_FIELDS = ['message'];

    /**
     * Event repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\EventRepository
     */
    protected $eventRepository;

    /**
     * @var \KDN\KdnEvents\Service\FrontendSessionHandlerService
     */
    protected $frontendSessionHandlerService;

    /**
     * @var \KDN\KdnEvents\Invitation\InvitationMailService
     */
    protected $mailService;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Invitation repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\InvitationRepository
     */
    protected $invitationRepository;

    public function injectEventRepository(\KDN\KdnEvents\Domain\Repository\EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * DI for frontend session handler service
     *
     * @param \KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService
     */
    public function injectFrontendSessionHandlerService(\KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService)
    {
        $this->frontendSessionHandlerService = $frontendSessionHandlerService;
    }

    /**
     * DI for invitation repository
     *
     * @param \KDN\KdnEvents\Domain\Repository\InvitationRepository $invitationRepository
     */
    public function injectInvitationRepository(\KDN\KdnEvents\Domain\Repository\InvitationRepository $invitationRepository)
    {
        $this->invitationRepository = $invitationRepository;
    }

    /**
     * DI for mail service
     *
     * @param \KDN\KdnEvents\Invitation\InvitationMailService $mailService
     */
    public function injectMailService(\KDN\KdnEvents\Invitation\InvitationMailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * Initialize parameters for submit action view
     *
     * @param Event $event
     * @param Invitation $invitation
     * @param string|null $notes Used as Honeypot field for spammers
     * @param string|null $confirmation
     * @param array $settings
     * @return array The show action parameters
     * @throws InvitationValidationException
     */
    public function initializeSubmitViewParameters(
        Event $event,
        Invitation $invitation,
        ?string $notes,
        ?string $confirmation,
        array $settings
    ): array
    {
        $invitation->setEvent($event);
        $this->assertInvitationValid($invitation);
        return array(
            'event' => $event,
            'invitation' => $invitation,
            'notes' => (string)$notes,
            'enabledFields' => self::ENABLE_FIELDS,
            'requiredFields' => [],
            'cryptKey' => $confirmation,
        );
    }

    /**
     * @return array
     */
    public function getAvailableEnableProperties()
    {
        return self::ENABLE_FIELDS;
    }

    /**
     * Initialize parameters for submit action view
     *
     * @param Event $event
     * @param Invitation $invitation
     * @param array $settings
     * @return void
     * @throws InvitationValidationException
     */
    public function processInvitation(
        Event $event,
        Invitation $invitation,
        array $settings
    )
    {
        $invitation->setEvent($event);
        $customerMessage = trim(strip_tags((string)$invitation->getMessage()));
        $invitation->setMessage($customerMessage);
        if (null === $invitation->getEventTime() && null !== $firstActiveEventTime = $event->getFirstActiveEventTime()) {
            $invitation->setEventTime($firstActiveEventTime);
        }
        $this->assertInvitationValid($invitation);

        $frontendUser = $this->frontendSessionHandlerService->getCurrentFrontendUser();
        if (null !== $frontendUser) {
            $invitation->setInvitedBy($frontendUser);
        }

        $this->invitationRepository->generateUniqueHash($invitation);

        $dispatchEvent = new InvitationBeforePersistEvent($invitation);
        $this->eventDispatcher->dispatch($dispatchEvent);

        $this->invitationRepository->add($invitation);

        /** @var PersistenceManager $persistenceManager */
        $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
        $persistenceManager->persistAll();
    }

    /**
     * Returns true if invitations are enabled fot the given event
     * @param Event|null $event
     * @param bool $skipLoginCheck Disable check if user is authenticated
     * @return bool
     */
    public function isInvitationEnabledForEvent(?Event $event, bool $skipLoginCheck = false)
    {
        if (self::staticIsInvitationEnabledForEvent($event, $skipLoginCheck)) {
            $dispatchEvent = new InvitationCheckEnabledEvent($event, true);
            $this->eventDispatcher->dispatch($dispatchEvent);
            return $dispatchEvent->isEnabled();
        }
        return false;
    }

    /**
     * Returns true if invitations are enabled fot the given event
     * @param Event|null $event
     * @param bool $skipLoginCheck Disable check if user is authenticated
     * @return bool
     */
    public static function staticIsInvitationEnabledForEvent(?Event $event, bool $skipLoginCheck = false)
    {
        if (null !== $event && TcaUtility::getExtConf('enableInvitations')
            && ($skipLoginCheck || 0 < MiscUtility::getFrontendUserId())) {
            /** @var Event $event */
            if (!$event->getPublished() || $event->isRegistrationDisabled() || !$event->getRegistrationIsOpen()) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Returns true if invitations are enabled fot the given event
     * @param Event|null $event
     * @return bool
     */
    public static function isAcceptInvitationEnabledForEvent(?Event $event)
    {
        return 0 >= MiscUtility::getFrontendUserId() && self::staticIsInvitationEnabledForEvent($event, true);
    }

    /**
     * Initialize parameters for submit action view
     *
     * @param Event $event
     * @param Invitation $invitation
     * @param array $mailSettings
     * @return void
     */
    public function sendInvitationNotifications(
        Event $event,
        Invitation $invitation,
        array $mailSettings
    )
    {
        if (!$this->isInvitationEnabledForEvent($event)) {
            return;
        }
        $adminMailSettings = $mailSettings;
        $dispatchEvent = new InvitationBeforeEmailEvent($invitation, $mailSettings);
        $this->eventDispatcher->dispatch($dispatchEvent);
        $mailSettings = $dispatchEvent->getMailSettings();
        $attachFiles = $dispatchEvent->getAttachedFiles();
        $mailService = $this->mailService;

        if ((null !== $invitedBy = $invitation->getInvitedBy()) && $invitedByEmail = $invitedBy->getEmail()) {
            if (!empty($mailSettings['ccEmail']) && !is_array($mailSettings['ccEmail'])) {
                $mailSettings['ccEmail'] = [$mailSettings['ccEmail']];
            }
            $mailSettings['ccEmail'][] = $invitedByEmail;
        }
        $mailService->sendInvitationMail(
            $invitation,
            $invitation->getEmail(),
            $mailSettings,
            $dispatchEvent->getTemplateEmailCustomer(),
            $attachFiles
        );

        // Send email to admin
        $adminMailSettings['is_admin_email'] = true;
        $dispatchEvent = new InvitationBeforeEmailEvent($invitation, $adminMailSettings);
        $this->eventDispatcher->dispatch($dispatchEvent);
        $adminMailSettings = $dispatchEvent->getMailSettings();
        $attachFiles = $dispatchEvent->getAttachedFiles();
        $mailService->sendInvitationMail(
            $invitation,
            $adminMailSettings['adminEmail'],
            $adminMailSettings,
            InvitationMailService::TEMPLATE_KEY_INVITATION_EMAIL_SERVICE,
            $attachFiles
        );
    }

    /**
     * Additional validation for invitation values
     * If any value is invalid, an exception is thrown
     *
     * @param Invitation $invitation
     * @throws InvitationValidationException
     */
    protected function assertInvitationValid(Invitation $invitation): void
    {
        $email = $invitation->getEmail();
        $firstName = $invitation->getFirstName();
        $lastName = $invitation->getLastName();
        if (empty($email) || empty($firstName)
            || empty($lastName)) {
            throw new InvitationValidationException(
                'Form is not valid',
                InvitationValidationException::CODE_MISSING_REQUIRED_FIELDS
            );
        }

        // Redirect if the username exists
        if (null !== $this->invitationRepository->findDuplicate($invitation)) {
            $exception = new InvitationValidationException(
                'Form data are not unique',
                InvitationValidationException::CODE_NOT_UNIQUE
            );
            $exception->setMessageLabelKey('message_already_invited');
            throw $exception;
        }
        $dispatchEvent = new InvitationValidationEvent($invitation);
        $this->eventDispatcher->dispatch($dispatchEvent);
        if (null !== $customException = $dispatchEvent->getException()) {
            throw $customException;
        }
        if (!GeneralUtility::validEmail($email)) {
            $exception = new InvitationValidationException(
                'Email is not valid',
                InvitationValidationException::CODE_NOT_UNIQUE
            );
            $exception->setMessageLabelKey('message_invalid_email');
            throw $exception;
        }
    }

    /**
     * @param string $hash
     * @return Invitation|null
     */
    public function getInvitationByHash(string $hash): ?Invitation
    {
        if (empty($hash)) {
            return null;
        }
        /** @noinspection PhpUnnecessaryLocalVariableInspection */
        $invitation = $this->invitationRepository->findOneByHash($hash);
        /** @var Invitation|null $invitation */
        return $invitation;
    }

    /**
     * @param Invitation $invitation
     * @param bool $autoSave
     * @return void
     */
    public function assertHasUniqueHash(Invitation $invitation, bool $autoSave = false)
    {
        if (!$invitation->getHash()) {
            $this->invitationRepository->generateUniqueHash($invitation);
            if ($autoSave) {
                $identifier = ['uid' => (int)$invitation->getUid()];
                $invitationBind = [
                    'hash' => $invitation->getHash(),
                ];
                $connection = DbUtility::getConnectionForTable(TcaUtility::TABLE_INVITATIONS);
                $connection->update(TcaUtility::TABLE_INVITATIONS, $invitationBind, $identifier);
            }
        }
    }

    /**
     * Wrapper for repository access
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findActive()
    {
        return $this->invitationRepository->findActive();
    }
}
