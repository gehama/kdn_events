<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\AbstractPersonModel;
use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Invitation;
use KDN\KdnEvents\Domain\Model\PersonInterface;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Domain\Model\Time;
use KDN\KdnEvents\Event\AddCustomEventMarkersEvent;
use KDN\KdnEvents\Event\AddCustomInvitationMarkersEvent;
use KDN\KdnEvents\Event\AddCustomRegistrationMarkersEvent;
use KDN\KdnEvents\Utility\MiscUtility;
use KDN\KdnEvents\Utility\TcaUtility;
use KDN\KdnEvents\Utility\TimeFormatUtility;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Page\CacheHashCalculator;

/**
 * Create event and registration markers
 */
class MarkerService extends AbstractService implements SingletonInterface
{
    /**
     * @var int
     */
    private $eventListPid;

    /**
     * @var int
     */
    private $invitationPid;

    /**
     * @return int
     */
    protected function getEventListPid()
    {
        if (null === $this->eventListPid) {
            $this->eventListPid = (int)TcaUtility::getExtConf('eventsListPid');
        }
        return $this->eventListPid;
    }

    /**
     * @param int $eventListPid
     */
    public function setEventListPid($eventListPid)
    {
        $this->eventListPid = $eventListPid;
    }

    /**
     * @return int
     */
    public function getInvitationPid(): int
    {
        if (null === $this->invitationPid) {
            $this->invitationPid = (int)TcaUtility::getExtConf('invitationPid');
        }
        return $this->invitationPid;
    }

    /**
     * @param int $invitationPid
     */
    public function setInvitationPid(int $invitationPid): void
    {
        $this->invitationPid = $invitationPid;
    }

    /**
     * Returns a marker array for the given registration instance
     *
     * @param Registration $registration
     * @param Time|null $eventTime
     * @return array
     */
    public function getRegistrationMarkers(Registration $registration, Time $eventTime = null): array
    {
        $event = $registration->getEvent();
        if ($event !== null) {
            $markers = $this->getEventMarkers($event, $eventTime);
        } else {
            $markers = [];
        }
        $ignoreFields = ['uid', 'pid', 'hash', 'internalRemarks',];
        $this->addObjectMarkers($registration, $markers, 'CUSTOMER', $ignoreFields);

        $this->addGreetingMarker($markers, $registration);
        $this->addTimespanMarkers($markers, 'CUSTOMER_EVENT', $registration->getEventStart(), $registration->getEventEnd());

        $message = LocalizationUtility::translate('event_form.new.form.agreeToTerms', 'kdn_events');
        $markers['###CUSTOMER_AGREE_TO_TERMS###'] = $message;

        $message = LocalizationUtility::translate('registration.new.form.newsletter', 'kdn_events');
        $markers['###CUSTOMER_NEWSLETTER###'] = $message;

        $markers['###CUSTOMER_STATUS###'] = $this->translate('status.' . $registration->getStatus());

        $this->addRegistrationLinkMarkers($markers, $registration);
        $dispatchEvent = new AddCustomRegistrationMarkersEvent($markers, $registration);
        $this->eventDispatcher->dispatch($dispatchEvent);
        return $dispatchEvent->getMarkers();
    }

    /**
     * Returns a marker array for the given invitation instance
     *
     * @param Invitation $invitation
     * @param Time|null $eventTime
     * @param bool $addEventMarkers Toggle adding of event markers (disabled for marker preview)
     * @return array
     */
    public function getInvitationMarkers(Invitation $invitation, Time $eventTime = null, bool $addEventMarkers = true): array
    {
        $event = $invitation->getEvent();
        if ($addEventMarkers && null !== $event) {
            $markers = $this->getEventMarkers($event, $eventTime);
        } else {
            $markers = [];
        }
        $ignoreFields = ['uid', 'pid', 'hash', 'internalRemarks',];
        $this->addObjectMarkers($invitation, $markers, 'INVITATION', $ignoreFields);

        $this->addInvitationLinkMarkers($markers, $invitation);
        $dispatchEvent = new AddCustomInvitationMarkersEvent($markers, $invitation);
        $this->eventDispatcher->dispatch($dispatchEvent);
        return $dispatchEvent->getMarkers();
    }

    public function addObjectMarkers($object, &$markers, $markerGroup, array $ignoreFields = ['uid', 'pid', 'hash',])
    {
        $objectGetters = $this->getEntityGetterFieldMap($object, $ignoreFields);
        foreach ($objectGetters as $fieldName => $getter) {
            $fieldValue = $object->$getter();
            $this->addFieldValueMarker($markers, $fieldName, $fieldValue, $markerGroup);
        }
        if ($object instanceof AbstractPersonModel) {
            $this->addGreetingMarker($markers, $object, $markerGroup . '_GREETING');
        }
    }

    private function addInvitationLinkMarkers(&$markers, Invitation $invitation)
    {
        $isBackend = $this->mode === 'BE' || !isset($GLOBALS['TSFE']);
        $eventsListPid = $this->getEventListPid();
        $event = $invitation->getEvent();
        if (!empty($eventsListPid)) {
            if ($isBackend) {
                $this->initFrontend($eventsListPid);
            }
            $detailsUrl = $this->createEventActionLink($eventsListPid, 'show', ['event' => $event], $isBackend);
            $markers['###URL_EVENT_DETAILS###'] = $detailsUrl;
        } else {
            $markers['###URL_EVENT_DETAILS###'] = '';
        }
        // TODO: get all page id's from TypoScript (where possible)
        $invitationPid = $this->getInvitationPid();
        if (!empty($invitationPid)) {
            if ($isBackend) {
                $this->initFrontend($invitationPid);
            }
            $arguments = ['hash' => $invitation->getHash()];
            $cancelUrl = $this->createInvitationActionLink($invitationPid, 'accept', $arguments, $isBackend);
            $markers['###URL_EVENT_INVITATION###'] = $cancelUrl;
        } else {
            $markers['###URL_EVENT_INVITATION###'] = '';
        }
    }

    private function addRegistrationLinkMarkers(&$markers, Registration $registration)
    {
        $isBackend = $this->mode === 'BE' || !isset($GLOBALS['TSFE']);
        $eventsListPid = $this->getEventListPid();
        $event = $registration->getEvent();
        if (!empty($eventsListPid)) {
            if ($isBackend) {
                $this->initFrontend($eventsListPid);
            }
            $detailsUrl = $this->createEventActionLink($eventsListPid, 'show', ['event' => $event], $isBackend);
            $markers['###URL_EVENT_DETAILS###'] = $detailsUrl;
        }
        $cancelPid = (int)TcaUtility::getExtConf('registrationCancelPid');
        if (!empty($cancelPid)) {
            if ($isBackend) {
                $this->initFrontend($cancelPid);
            }
            $arguments = ['hash' => $registration->getHash()];
            $cancelUrl = $this->createEventActionLink($cancelPid, 'cancel', $arguments, $isBackend);
            $markers['###URL_REGISTRATION_CANCEL###'] = $cancelUrl;
        }
    }

    private function createEventActionLink($pageUid, $action, $arguments, $isBackend)
    {
        return $this->createActionLink(
            $pageUid,
            $action,
            'items',
            'Event',
            $arguments,
            $isBackend
        );
    }

    private function createInvitationActionLink($pageUid, $action, $arguments, $isBackend)
    {
        return $this->createActionLink(
            $pageUid,
            $action,
            'invitation',
            'Invitation',
            $arguments,
            $isBackend
        );
    }

    private function createActionLink($pageUid, string $action, string $pluginName, string $controller, $arguments, $isBackend)
    {
        if ($isBackend) {
            return $this->createFeLinkFromBackend($pageUid, $action, $arguments);
        }
        $extensionName = 'kdnevents';
        $uriBuilder = $this->getUriBuilder();
        if (null !== $uriBuilder) {
            $url = $uriBuilder
                ->reset()
                ->setTargetPageUid($pageUid)
                ->setLinkAccessRestrictedPages(true)
                //->setArguments($additionalParams)
                ->setCreateAbsoluteUri(true)
                //->setAddQueryString($addQueryString)
                ->uriFor($action, $arguments, $controller, $extensionName, $pluginName);
            if (strpos($url, 'http') !== 0) {
                $url = TcaUtility::getDomain() . ltrim($url, '/');
            }
            return $url;
        }
        return null;
    }

    private function createFeLinkFromBackend($pageUid, $action, $arguments)
    {
        $extensionName = 'kdnevents';
        $pluginName = 'items';
        $controller = 'Event';
        $group = 'tx_' . $extensionName . '_' . $pluginName;
        /** @var ContentObjectRenderer $cObj */
        $cObj = GeneralUtility::makeInstance(ContentObjectRenderer::class);
        // set host name for command line calls
        if (empty($_SERVER['HTTP_HOST'])) {
            $_SERVER['HTTP_HOST'] = TcaUtility::getDomain();
            $_SERVER['SERVER_PORT'] = 443;
            $_SERVER['HTTPS'] = 'on';
        }
        $currentHost = (string)GeneralUtility::getIndpEnv('HTTP_HOST');
        if (empty($currentHost)) {
            GeneralUtility::flushInternalRuntimeCaches();
            /** @noinspection PhpUnusedLocalVariableInspection */
            $currentHost = (string)GeneralUtility::getIndpEnv('HTTP_HOST');
        }
        $additionalParams = [
            $group . '[action]' => $action,
            $group . '[controller]' => $controller,
        ];
        foreach ($arguments as $key => $value) {

            if ($value instanceof AbstractEntity) {
                $paramValue = $value->getUid();
            } else {
                $paramValue = $value;
            }
            $additionalParams[$group . '[' . $key . ']'] = $paramValue;
        }
        /** @var CacheHashCalculator $cacheHashCalculator */
        $cacheHashCalculator = GeneralUtility::makeInstance(CacheHashCalculator::class);
        $queryStr = GeneralUtility::implodeArrayForUrl('', array_merge($additionalParams, ['id' => $pageUid]));
        $cHashParameters = $cacheHashCalculator->getRelevantParameters($queryStr);
        $additionalParams['cHash'] = $cacheHashCalculator->calculateCacheHash($cHashParameters);
        $linkConf = array(
            'parameter' => $pageUid,
            'forceAbsoluteUrl' => 1,
            'additionalParams' => GeneralUtility::implodeArrayForUrl(NULL, $additionalParams),
            'linkAccessRestrictedPages' => 1
        );
        $url = $cObj->typolink_URL($linkConf);
        // No url prefix can be set if the mail is send from the command line
        if (strpos($url, 'http') !== 0) {
            $url = TcaUtility::getDomain() . ltrim($url, '/');
        } elseif (strpos($url, 'http://') === 0) {
            $url = str_replace('http://', 'https://', $url);
        }
        return $url;
    }

    /*
     * init frontend to render frontend links in task
     */
    protected function initFrontend($pageUid)
    {
        MiscUtility::initTypoScriptFrontendController($pageUid);
    }

    private function addGreetingMarker(&$markers, AbstractPersonModel $registration, string $markerGroup = 'GREETING'): void
    {
        $personGender = $registration->getSalutation();
        $name = $registration->getLastName();
        $namePersonal = $registration->getFirstName();
        switch ($personGender) {
            case PersonInterface::SALUTATION_MALE:
                $labelKey = 'email_greeting.male';
                $labelKeyPersonal = 'email_greeting_personal.male';
                break;
            case PersonInterface::SALUTATION_FEMALE:
                $labelKey = 'email_greeting.female';
                $labelKeyPersonal = 'email_greeting_personal.female';
                break;
            case PersonInterface::SALUTATION_DIVERSE:
                $labelKey = 'email_greeting.diverse';
                $labelKeyPersonal = 'email_greeting_personal.diverse';
                $name = trim($registration->getFirstName() . ' ' . $name);
                break;
            default:
                $labelKey = 'email_greeting.default';
                $labelKeyPersonal = 'email_greeting_personal.default';
                break;
        }
        $greetingLabel = LocalizationUtility::translate($labelKey, 'kdn_events');
        $greetingLabelPersonal = LocalizationUtility::translate($labelKeyPersonal, 'kdn_events');
        if (!empty($personGender) && !empty($name)) {
            $title = $registration->getTitle();
            if (strpos($title, 'Dipl') !== false && strpos($title, 'Dr') === false) {
                $title = '';
            }
            if (!empty($title)) {
                $greetingLabel .= ' ' . $title;
            }
            $greetingLabel .= ' ' . $name;
            $greetingLabelPersonal .= ' ' . ($namePersonal ?: $name);
        }
        $markers['###'.$markerGroup.'###'] = $greetingLabel;
        $markers['###'.$markerGroup.'_PERSONAL###'] = $greetingLabelPersonal;
    }

    /**
     * Returns a map of the entity fields an the corresponding getter functions
     *
     * @param AbstractEntity $entity
     * @param array $ignoreFields List of fields to be ignored
     * @return array
     */
    private function getEntityGetterFieldMap(AbstractEntity $entity, array $ignoreFields = array()): array
    {
        $classMethods = get_class_methods($entity);
        $entityGetters = [];
        foreach ($classMethods as $methodName) {
            if (strpos($methodName, 'get') === 0) {
                $getter = $methodName;
                $fieldName = lcfirst(substr($getter, 3));
                if (!in_array($fieldName, $ignoreFields, false)) {
                    $entityGetters[$fieldName] = $getter;
                }
            }
        }
        return $entityGetters;
    }

    /**
     * Returns a marker array for the given event instance
     *
     * @param Event $event
     * @param Time|null $eventTime
     * @return array
     */
    public function getEventMarkers(Event $event, Time $eventTime = null): array
    {
        $markers = array();
        $eventFields = array(
            'title', 'description', 'eventStart', 'eventEnd',
            'registrationStart', 'registrationEnd', 'maxParticipants', 'status', 'registrations',
            'organizer', 'location', 'locationFull', 'videoConferenceUrl', 'videoConferenceTestUrl',
            'videoConferenceComment',
        );
        foreach ($eventFields as $fieldName) {
            $getter = 'get' . ucfirst($fieldName);
            $fieldValue = $event->$getter();
            $this->addFieldValueMarker($markers, $fieldName, $fieldValue, 'EVENT');
        }
        if (null !== $eventTime) {
            $this->addFieldValueMarker($markers, 'eventStart', $eventTime->getTimeStart(), 'EVENT');
            $this->addFieldValueMarker($markers, 'eventEnd', $eventTime->getTimeEnd(), 'EVENT');
        }

        $this->addTimespanMarkers($markers, 'EVENT', $event->getEventStart(), $event->getEventEnd());

        $dispatchEvent = new AddCustomEventMarkersEvent($markers, $event);
        $this->eventDispatcher->dispatch($dispatchEvent);
        return $dispatchEvent->getMarkers();
    }

    /**
     * @param array $markers The marker array
     * @param string $markerGroup The marker name group prefix
     * @param \DateTime|string $startDate
     * @param \DateTime|string $endDate
     * @throws \Exception
     */
    private function addTimespanMarkers(&$markers, $markerGroup, $startDate, $endDate)
    {
        $markerNameStart = '###' . strtoupper($markerGroup);
        $timeFormatUtility = new TimeFormatUtility();
        $format = '%d. %B %Y';
        $markers[$markerNameStart . '_TIMESPAN_DATE###'] = $timeFormatUtility->render(
            $startDate,
            $endDate,
            $format,
            false
        );
        $markers[$markerNameStart . '_TIMESPAN###'] = $timeFormatUtility->render(
            $startDate,
            $endDate,
            $format,
            true
        );
        $markers[$markerNameStart . '_TIMESPAN_LONG###'] = $timeFormatUtility->render(
            $startDate,
            $endDate
        );
    }

    /**
     * Adds a marker for the given field and the field value to the marker group
     *
     * @param array $markers The marker array
     * @param string $fieldName
     * @param mixed $fieldValue
     * @param string $markerGroup
     */
    private function addFieldValueMarker(&$markers, $fieldName, $fieldValue, $markerGroup): void
    {
        if (is_object($fieldValue)) {
            if ($fieldValue instanceof \DateTime) {
                $dateValue = $fieldValue->format('d.m.Y');
                $this->addMarker($markers, $markerGroup, $fieldName . '_date', $dateValue);
            }
            if ($fieldValue instanceof FrontendUser) {
                $this->addMarker($markers, $markerGroup, $fieldName . '_firstName', $fieldValue->getFirstName());
                $this->addMarker($markers, $markerGroup, $fieldName . '_lastName', $fieldValue->getLastName());
            }
        }
        $convertedValue = $this->convertValueToString($fieldValue);
        $this->addMarker($markers, $markerGroup, $fieldName, $convertedValue);
    }

    /**
     * Convert the given field value to string
     *
     * @param mixed $fieldValue
     * @return string
     */
    private function convertValueToString($fieldValue): string
    {
        $convertedValue = $fieldValue;
        if (is_object($fieldValue)) {
            if ($fieldValue instanceof \DateTime) {
                $convertedValue = $fieldValue->format('d.m.Y H:i');
            } elseif ($fieldValue instanceof ObjectStorage) {
                $subValues = [];
                foreach ($fieldValue as $childValue) {
                    $subValues[] = $this->convertValueToString($childValue);
                }
                $convertedValue = implode(', ', array_filter($subValues));
            } elseif ($fieldValue instanceof FrontendUser) {
                $convertedValue = trim($fieldValue->getFirstName() . ' ' . $fieldValue->getLastName());
            } elseif (method_exists($fieldValue, 'getName')) {
                $convertedValue = $fieldValue->getName();
            } elseif (method_exists($fieldValue, 'getTitle')) {
                $convertedValue = $fieldValue->getTitle();
            }
        }
        return (string)$convertedValue;
    }


    /**
     * Convert the given input in camel case format into underscore format
     *
     * @param array $markers The marker array
     * @param string $prefix The prefix value for the marker
     * @param string $fieldName
     * @param string $fieldValue
     */
    private function addMarker(&$markers, $prefix, $fieldName, $fieldValue): void
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $fieldName, $matches);
        $ret = $matches[0];
        foreach ($matches[0] as $key => $match) {
            $matches[0][$key] = $match === strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        $markerKey = $prefix . '_' . str_replace($prefix . '_', '', strtoupper(implode('_', $ret)));
        $markers['###' . $markerKey . '###'] = $fieldValue;
    }
}
