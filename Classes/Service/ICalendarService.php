<?php


namespace KDN\KdnEvents\Service;


use KDN\KdnEvents\Utility\TcaUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

class ICalendarService
{
    /**
     * The object manager
     *
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;
    /**
     * The configuration manager
     *
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
     */
    protected $configurationManager;
    /**
     * DI for $configurationManager
     *
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager
     */
    public function injectConfigurationManager(
        \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager
    ) {
        $this->configurationManager = $configurationManager;
    }

    /**
     * DI for $objectManager
     *
     * @param \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager
     */
    public function injectObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    /**
     * Initiates the ICS download for the given event
     *
     * @param \KDN\KdnEvents\Domain\Model\Event $event The event
     * @param array $viewConfig The view configuration (template paths)
     * @return void
     *@throws \Exception Exception
     */
    public function downloadiCalendarFile(\KDN\KdnEvents\Domain\Model\Event $event, array $viewConfig)
    {
        $content = $this->getICalendarContent($event, $viewConfig);
        header('Content-Disposition: attachment; filename="event' . $event->getUid() . '.ics"');
        header('Content-Type: text/calendar');
        header('Content-Length: ' . strlen($content));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: no-cache');
        echo $content;
    }
    /**
     * Returns the rendered iCalendar entry for the given event according to RFC 2445
     *
     * @param \KDN\KdnEvents\Domain\Model\Event $event The event
     * @param array $viewConfig The view configuration (template paths)
     *
     * @return string
     */
    protected function getICalendarContent(\KDN\KdnEvents\Domain\Model\Event $event, array $viewConfig)
    {
        /** @var StandaloneView $icalView */
        $icalView = GeneralUtility::makeInstance(StandaloneView::class);
        $icalView->setFormat('txt');
        $icalView->setTemplateRootPaths($viewConfig['templateRootPaths']);
        $icalView->setLayoutRootPaths($viewConfig['layoutRootPaths']);
        $icalView->setPartialRootPaths($viewConfig['partialRootPaths']);
        $icalView->setTemplate('Event/ICalendar.txt');
        $icalView->assignMultiple([
            'event' => $event,
            'typo3Host' => TcaUtility::getDomain(),
            'domain' => TcaUtility::getDomain(),
        ]);
        // Render view and remove empty lines
        $icalContent = preg_replace('/^\h*\v+/m', '', $icalView->render());
        // Finally replace new lines with CRLF
        $icalContent = str_replace(chr(10), chr(13) . chr(10), $icalContent);
        return $icalContent;
    }
}