<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Domain\Model\Registration as RegistrationModel;
use KDN\KdnEvents\Domain\Model\Time;
use KDN\KdnEvents\Domain\Repository\EventRepository;
use KDN\KdnEvents\Domain\Repository\RegistrationRepository;
use KDN\KdnEvents\Domain\Repository\StatusRepository;
use KDN\KdnEvents\Domain\Repository\TimeRepository;
use KDN\KdnEvents\Utility\DbUtility;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Manage event registration wait-list
 */
class WaitlistService extends AbstractService implements SingletonInterface
{

    /**
     * Updates the waitlist after a registration is cancelled or deleted
     *
     * @param array $cancelledRecord
     */
    public function updateRegistrationWaitlist(array $cancelledRecord): void
    {
        /** @var RegistrationRepository $repository */
        $repository = $this->objectManager->get(RegistrationRepository::class);
        /** @var \KDN\KdnEvents\Domain\Model\Registration $registrationEntity */
        $registrationEntity = $repository->findByUid((int)$cancelledRecord['uid']);
        if (null !== $registrationEntity) {
            $event = $registrationEntity->getEvent();
        } else {
            $eventId = (int)$cancelledRecord['event'];
            /** @var \KDN\KdnEvents\Domain\Repository\EventRepository $eventRepository */
            $eventRepository = $this->objectManager->get(EventRepository::class);
            /** @var \KDN\KdnEvents\Domain\Model\Event $event */
            $event = $eventRepository->findByUid($eventId);
        }

        if (null !== $event) {
            $this->updateEventWaitlist($event);
        }
        if (null !== $registrationEntity) {
            $this->sendCancelInfo($registrationEntity);
        }
    }

    /**
     * @param int|Event|null $eventModelOrId
     * @return int $eventRegistrationCount
     */
    public function updateRegistrationCountAndWaitlist($eventModelOrId): int
    {
        $eventData = $this->transformEventModelOrId($eventModelOrId);
        $eventId = (int) $eventData['eventId'];
        $event = $eventData['event'];
        if (null !== $event) {
            $eventRegistrationCount = $this->updateEventWaitlist($event, 1);
        } else {
            $eventRegistrationCount = $this->updateEventRegistrationCount($eventId);
        }
        return $eventRegistrationCount;
    }

    /**
     * @param Event $event
     * @param int $subtract
     * @return int $eventRegistrationCount
     */
    public function updateEventWaitlist(Event $event, $subtract = 0): int
    {
        $eventId = $event->getUid();
        $eventRegistrationCount = $this->updateEventRegistrationCount($event);
        $maxParticipants = $event->getMaxParticipants();
        $totalRegistrations = $event->getRegistrations();
        // Optionally subtract record to be deleted from registered count
        $registeredCount = max($this->getStatusCountForEvent($eventId) - $subtract, 0);
        $waitlistCount = $totalRegistrations - $registeredCount;
        $limit = '';
        if ($waitlistCount === 0) {
            $limit = 0;
        } elseif (!empty($maxParticipants)) {
            $limit = max($maxParticipants - $registeredCount, 0);
        }
        $waitlistItems = array();
        if ($limit > 0) {
            $waitlistItems = $this->getEventWaitlistItems($eventId, $limit);
        }
        foreach ($waitlistItems as $waitlistItem) {
            $pid = $waitlistItem->getPid();
            $pluginTs = self::loadEventsTS($pid);
            if (empty($pluginTs['mail.'])) {
                $pluginTs['mail.'] = array(
                    'fromName' => 'KDN Events',
                    'fromEmail' => 'info@gerthammes.de',
                    'adminEmail' => 'info@gerthammes.de',
                );
            }
            /** @var MailService $mailService */
            $mailService = $this->objectManager->get(MailService::class);
            $mailSettings = $pluginTs['mail.'];
            $userMailSettings = $mailSettings;
            $emailTemplateWuc = $mailService->updateMailSettingsFromEmailTemplate(
                $userMailSettings,
                MailService::TEMPLATE_KEY_WAITLIST_UPDATE_CUSTOMER,
                $pid
            );
            $mailIsSent = false;
            if (null !== $emailTemplateWuc) {
                $mailIsSent = $mailService->sendRegistrationMail(
                    $waitlistItem,
                    $waitlistItem->getEmail(),
                    $userMailSettings,
                    $emailTemplateWuc
                );
            }
            if ($mailIsSent) {
                $identifier = [
                    'uid' => (int)$waitlistItem->getUid()
                ];
                $bind = [
                    'status' => RegistrationModel::STATUS_REGISTERED,
                ];
                $connection = DbUtility::getConnectionForTable($this->tables['registration']);
                $connection->update($this->tables['registration'], $bind, $identifier);
                $adminMailSettings = $mailSettings;
                $adminMailSettings['is_admin_mail'] = true;
                $emailTemplateWus = $mailService->updateMailSettingsFromEmailTemplate(
                    $adminMailSettings,
                    MailService::TEMPLATE_KEY_WAITLIST_UPDATE_SERVICE,
                    $pid
                );
                if (null !== $emailTemplateWus) {
                    $mailService->sendRegistrationMail(
                        $waitlistItem,
                        $adminMailSettings['adminEmail'],
                        $adminMailSettings,
                        MailService::TEMPLATE_KEY_WAITLIST_UPDATE_SERVICE
                    );
                }
            }
        }
        return $eventRegistrationCount;
    }

    /**
     * Send user email for cancelled registration
     *
     * @param Registration $registrationEntity
     */
    private function sendCancelInfo(Registration $registrationEntity): void
    {
        $pid = (int)$registrationEntity->getPid();
        $pluginTs = self::loadEventsTS($pid);
        if (empty($pluginTs['mail.'])) {
            // $logMessage = 'No TypoScript could be loaded for page id: ' . $pid;
            $pluginTs['mail.'] = array(
                'fromName' => 'KDN Events',
                'fromEmail' => 'info@gerthammes.de',
                'adminEmail' => 'info@gerthammes.de',
            );
        }

        /** @var MailService $mailService */
        $mailService = $this->objectManager->get(MailService::class);
        $mailSettings = $pluginTs['mail.'];
        $userMailSettings = $mailSettings;
        $mailService->updateMailSettingsFromEmailTemplate(
            $userMailSettings,
            MailService::TEMPLATE_KEY_WAITLIST_UPDATE_CUSTOMER,
            $pid
        );
        $mailService->sendRegistrationMail(
            $registrationEntity,
            $registrationEntity->getEmail(),
            $userMailSettings,
            MailService::TEMPLATE_KEY_CANCEL_INFO_CUSTOMER
        );
    }

    /**
     * @param int $eventId
     * @param string $limit
     * @return RegistrationModel[] $records
     */
    private function getEventWaitlistItems($eventId, $limit = ''): array
    {
        $table = $this->tables['registration'];
        $waitlistItem = null;

        $queryBuilder = DbUtility::getQueryBuilderForTable($table);
        /** @var DeletedRestriction $deletedRestriction */
        $deletedRestriction = GeneralUtility::makeInstance(DeletedRestriction::class);
        /** @var HiddenRestriction $hiddenRestriction */
        $hiddenRestriction = GeneralUtility::makeInstance(HiddenRestriction::class);
        $queryBuilder->getRestrictions()->removeAll()
            ->add($deletedRestriction)
            ->add($hiddenRestriction);
        $queryBuilder->select('uid');
        $queryBuilder->from($table);
        $queryBuilder->where($queryBuilder->expr()->eq('event', $queryBuilder->createNamedParameter($eventId, \PDO::PARAM_INT)));
        $queryBuilder->andWhere($queryBuilder->expr()->eq('status', $queryBuilder->createNamedParameter(RegistrationModel::STATUS_WAITLIST, \PDO::PARAM_INT)));
        $queryBuilder->orderBy('crdate', 'ASC');
        if (!empty($limit)) {
            $queryBuilder->setMaxResults((int)$limit);
        }
        $idRows = $queryBuilder->execute()->fetchAll();

        /** @var \KDN\KdnEvents\Domain\Repository\RegistrationRepository $repository */
        $repository = $this->objectManager->get(RegistrationRepository::class);
        $records = [];
        foreach ($idRows as $record) {
            $waitlistItem = $repository->findByUid($record['uid']);
            if ($waitlistItem !== null) {
                $records[] = $waitlistItem;
            }
        }
        return $records;
    }

    private function getStatusCountForEvent($eventId, $status = RegistrationModel::STATUS_REGISTERED, $statusNegate = false): int
    {
        return $this->getStatusCountForIdentifiers(['event' => $eventId], $status, $statusNegate);
    }

    private function getStatusCountForIdentifiers($identifiers, $status = RegistrationModel::STATUS_REGISTERED, $statusNegate = false): int
    {
        $queryBuilder = DbUtility::getQueryBuilderForTable($this->tables['registration']);
        /** @var DeletedRestriction $deletedRestriction */
        $deletedRestriction = GeneralUtility::makeInstance(DeletedRestriction::class);
        /** @var HiddenRestriction $hiddenRestriction */
        $hiddenRestriction = GeneralUtility::makeInstance(HiddenRestriction::class);
        $queryBuilder->getRestrictions()->removeAll()
            ->add($deletedRestriction)
            ->add($hiddenRestriction);

        $queryBuilder
            ->count('*')
            ->from($this->tables['registration']);
        foreach ($identifiers as $field => $value) {
            $type = is_int($value) ? \PDO::PARAM_INT : \PDO::PARAM_STR;
            $eventParameter = $queryBuilder->createNamedParameter($value, $type);
            $queryBuilder->andWhere($queryBuilder->expr()->eq($field, $eventParameter));
        }
        $statusParameter = $queryBuilder->createNamedParameter($status, \PDO::PARAM_INT);
        if ($statusNegate) {
            $statusExpr = $queryBuilder->expr()->neq('status', $statusParameter);
        } else {
            $statusExpr = $queryBuilder->expr()->eq('status', $statusParameter);
        }
        $queryBuilder->andWhere($statusExpr);
        return (int)$queryBuilder->execute()->fetchColumn(0);
    }

    /**
     * @param int|Event|null $eventModelOrId
     */
    public function updateEventRegistrationCount($eventModelOrId): int
    {
        $eventData = $this->transformEventModelOrId($eventModelOrId);
        $eventId = (int) $eventData['eventId'];
        $event = $eventData['event'];
        if (null !== $event) {
            /** @var TimeRepository $timeRepository */
            $timeRepository = $this->objectManager->get(TimeRepository::class);
            /** @var \KDN\KdnEvents\Domain\Model\Event $event */
            $countRegNotCancelled = $this->getStatusCountForEvent($eventId, RegistrationModel::STATUS_CANCELLED, true);
            $eventNeedsUpdate = false;
            if ($countRegNotCancelled !== $event->getRegistrations()) {
                $event->setRegistrations($countRegNotCancelled);
                $eventNeedsUpdate = true;
            }
            if ($eventNeedsUpdate || $this->updateEventRegistrationStatus($event)) {
                /** @var EventRepository $eventRepository */
                $eventRepository = $this->objectManager->get(EventRepository::class);
                $eventRepository->update($event);
            }
            $eventTimes = $event->getEventTimes();
            foreach ($eventTimes as $eventTime) {
                /** @var Time $eventTime */
                $regCount = $this->getStatusCountForIdentifiers(['event_time' => $eventTime->getUid()], RegistrationModel::STATUS_CANCELLED, true);
                if ($regCount !== $eventTime->getRegistrations()) {
                    $eventTime->setRegistrations($regCount);
                    $timeRepository->update($eventTime);
                }
            }
            /** @var PersistenceManager $persistenceManager */
            $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
            $persistenceManager->persistAll();
            return $countRegNotCancelled;
        }
        return 0;
    }

    public function updateEventRegistrationStatus(\KDN\KdnEvents\Domain\Model\Event $event): bool
    {
        if ($event->isWaitlistAvailable() && $event->getStatus()->getRegistrationPossible()) {
            // Open wait list if status is "published"
            $eventIsFull = $event->getMaxParticipantsReached();
            /** @var StatusRepository $repository */
            $statusRepository = $this->objectManager->get(StatusRepository::class);
            $nextEventStatus = $statusRepository->findNextStatus($event->getStatus(), $eventIsFull);
            if ($nextEventStatus !== null && $event->getStatus() !== $nextEventStatus) {
                /** @var \KDN\KdnEvents\Domain\Model\Status $nextEventStatus */
                $event->setStatus($nextEventStatus);
                return true;
            }
        }
        return false;
    }

}
