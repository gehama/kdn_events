<?php

namespace KDN\KdnEvents\Service;

/***
 *
 * This file is part of the "KDN Events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 ***/

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Registration;

if (!class_exists('KDN\\KdnEvents\\Utility\\Pdf')) {

    require_once(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('kdn_events').'Classes/Utility/Pdf.php');
}
use KDN\KdnEvents\Utility\Pdf;

class PdfCreator extends AbstractExportService
{
    /**
     * @var Pdf|\TCPDF
     */
    private $pdf;

    /**
     * @var Event
     */
    private $event;

    /**
     * PDF constructor.
     */
    public function __construct()
    {
        $this->pdf = new Pdf();
    }

    /**
     * @param Event $event
     */
    public function setEvent(Event $event): void
    {
        $this->event = $event;
    }


    /**
     * @param string $sheetName
     * @param Registration[] $rows
     * @throws \Exception
     */
    public function create($sheetName, $rows): void
    {
        if ($sheetName === 'export.pdf.participant_list') {
            $this->createParticipantList($this->event, $rows);
        }

    }

    /**
     * Create the participant list for the current event
     *
     * @param Event $event
     * @param Registration[] $registrations
     */
    private function createParticipantList(Event $event, $registrations): void
    {
        /** @var Pdf|\TCPDF $pdf */
        $pdf = $this->pdf;
        $pdf->SetMargins(20, 10, 20);
        $pdf->AddPage();

        $pdf->SetTextColor(0, 0, 0);

        $pdf->SetFontSize(16);
        $pdf->MultiCell(0, 0, $event->getTitle(), 0, 'L', 0, 1);
        $rowHeight = 8;

        $eventTimes = $event->getEventTimes();
        if ($event->isSeriesEvent() && count($eventTimes) > 1) {
            $groupedRegistrations = [];
            foreach ($registrations as $registration) {
                $start = $registration->getEventStart();
                $startTimestamp = null !== $start ? $start->getTimestamp() : time();
                if (!isset($groupedRegistrations[$startTimestamp])) {
                    $groupedRegistrations[$startTimestamp] = [
                        'start' => $start,
                        'end' => $registration->getEventEnd(),
                        'registrations' => [],
                    ];
                }
                $groupedRegistrations[$startTimestamp]['registrations'][] = $registration;
            }
            ksort($groupedRegistrations);
            $pageHeight = $pdf->getPageHeight() - $pdf->getBreakMargin();
            foreach ($groupedRegistrations as $groupData) {
                $timePeriodHeight = 22;
                $endHeight = $pdf->GetY() + $timePeriodHeight + $rowHeight * count($groupData['registrations']);
                if ($endHeight > $pageHeight) {
                    $pdf->AddPage();
                }
                $pdf->Ln(12);
                $timePeriod = $this->formatTimePeriod($groupData['start'], $groupData['end']);
                $pdf->SetFontSize(12);
                $pdf->Cell(0, 0, $timePeriod, 0, 1, 'L');
                $pdf->Ln(5);
                $this->addRegistrationRows($pdf, $groupData['registrations'], $rowHeight);
            }
        } else {
            $pdf->Ln(2);
            $timePeriod = $this->formatTimePeriod($event->getEventStart(), $event->getEventEnd());
            $pdf->SetFontSize(12);
            $pdf->Cell(0, 0, $timePeriod, 0, 1, 'L');
            $pdf->Ln(8);
            $this->addRegistrationRows($pdf, $registrations, $rowHeight);
        }
    }

    /**
     * @param string $fileBaseName File name without file type ending
     * @param string $fileType The file type
     */
    public function sendFile($fileBaseName, $fileType): void
    {
        $fileName = $this->filterFileName(str_replace('.', '_', $fileBaseName)) . '.' . $fileType;
        if ($fileType === 'pdf') {
            $this->pdf->Output($fileName, 'D');
            exit;
        }
    }

    /**
     * @param Pdf $pdf
     * @param Registration[] $registrations
     * @param int $rowHeight
     */
    private function addRegistrationRows(Pdf $pdf, $registrations, $rowHeight): void
    {
        $nameWidth = 90;
        foreach ($registrations as $registration) {
            $personName = $registration->getFirstName() . ' ' . $registration->getLastName();
            $pdf->Cell($nameWidth, $rowHeight, $personName, 'B', 0, 'L');
            $pdf->SetX($pdf->GetX() + 5);
            $pdf->Cell(0, $rowHeight, '', 'B', 1, 'L');
            $pdf->Ln(2);
        }
    }

    private function formatTimePeriod(\DateTime $periodStart, \DateTime $periodEnd = null): string
    {
        $startDate = date('d.m.Y', $periodStart->getTimestamp());
        $startTime = date('H:i', $periodStart->getTimestamp());
        $timePeriod = $startDate . ' ' . $startTime;
        if (null !== $periodEnd) {
            $endDate = date('d.m.Y', $periodEnd->getTimestamp());
            $endTime = date('H:i', $periodEnd->getTimestamp());
            if ($endDate !== $startDate) {
                $timePeriod .= ' - ' . $endDate . ' ' . $endTime;

            } elseif ($startTime !== $endTime) {
                $timePeriod .= ' - ' . $endTime;
            }
        }
        return $timePeriod;
    }

}