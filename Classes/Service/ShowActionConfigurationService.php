<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Invitation;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Domain\Model\WorkArea;
use KDN\KdnEvents\Event\ShowActionConfigurationEvent;
use KDN\KdnEvents\Invitation\InvitationManager;
use KDN\KdnEvents\Seo\EventTitleProvider;
use KDN\KdnEvents\Utility\MiscUtility;
use KDN\KdnEvents\Utility\TcaUtility;
use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class show action service
 */
class ShowActionConfigurationService extends ConfigurationService
{

    /**
     * @var \KDN\KdnEvents\Service\FrontendSessionHandlerService
     */
    protected $frontendSessionHandlerService;

    /**
     * Registration repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\RegistrationRepository
     */
    protected $registrationRepository;

    /**
     * Facility repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\FacilityRepository
     */
    protected $facilityRepository;

    /**
     * Work area repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\WorkAreaRepository
     */
    protected $workAreaRepository;

    /**
     * Invitation service
     *
     * @var InvitationManager
     */
    protected $invitationService;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * DI for $icalendarService
     *
     * @param \KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService
     */
    public function injectFrontendSessionHandlerService(\KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService)
    {
        $this->frontendSessionHandlerService = $frontendSessionHandlerService;
    }

    public function injectRegistrationRepository(\KDN\KdnEvents\Domain\Repository\RegistrationRepository $registrationRepository)
    {
        $this->registrationRepository = $registrationRepository;
    }

    public function injectFacilityRepository(\KDN\KdnEvents\Domain\Repository\FacilityRepository $facilityRepository)
    {
        $this->facilityRepository = $facilityRepository;
    }

    public function injectWorkAreaRepository(\KDN\KdnEvents\Domain\Repository\WorkAreaRepository $workAreaRepository)
    {
        $this->workAreaRepository = $workAreaRepository;
    }

    public function injectInvitationManager(InvitationManager $invitationService)
    {
        $this->invitationService = $invitationService;
    }

    /**
     * Initialize parameters for show action view
     * @param Event $event
     * @param Registration|null $registration
     * @param array $settings
     * @return array The show action parameters
     */
    public function initializeShowViewParameters(Event $event, ?Registration $registration, array $settings): array
    {
        $feSessionHandler = $this->frontendSessionHandlerService;
        if (null === $registration) {
            $registration = new Registration();
            $isNew = true;
            $feSessionHandler->prefillSessionRegistrationDataIfSet($registration);
        } else {
            $isNew = false;
        }
        $registration->setEvent($event);
        $enableRegistration = !$event->isRegistrationDisabled();
        $fixedProperties = [];
        $showDownloads = true;
        $showLoginForm = false;
        $userLoggedIn = null;
        $invitation = $this->initInvitation($registration);
        // TODO: load invitation
        $requiresLogin = !empty($settings['enableRegistrationOnLogin']) && null === $invitation;
        if ($enableRegistration) {
            if ($requiresLogin) {
                $frontendUser = $feSessionHandler->getCurrentFrontendUser();
                $userLoggedIn = null !== $frontendUser;
                $enableRegistration = null !== $frontendUser;
                $showDownloads = $enableRegistration;
                $showLoginForm = !$enableRegistration;
            }
            // Invitation can only be !== null if no user is authenticated!
            if (null !== $invitation) {
                // Registration values already set in registration model setInvitation
                $fixedProperties = [
                    'email',
                    'firstName',
                    'lastName',
                    'salutation',
                ];
            } else {
                $isFixedUserData = $feSessionHandler->isFixedUserData();
                if ($isFixedUserData || $isNew) {
                    $changedProperties = $feSessionHandler->prefillPersonDataForUserIfSet($registration);
                    if ($isFixedUserData) {
                        $checkFixedProperties = FrontendSessionHandlerService::FIXED_USER_PROPERTIES;
                        $fixedProperties = array_intersect($changedProperties, $checkFixedProperties);
                    }
                }
            }
        }
        if ($this->isFieldEnabledForEvent('facility', $event)) {
            $facilityChoices = $this->facilityRepository->findAll();
        } else {
            $facilityChoices = null;
        }
        if ($this->isFieldEnabledForEvent('work_areas', $event)) {
            $workAreaChoices = [];
            $allWorkAreaChoices = $this->workAreaRepository->findAll();
            $workAreaStr = (string) $event->getEnabledWorkAreas();
            if (!empty($workAreaStr)) {
                $enabledWorkAreas = GeneralUtility::intExplode(',', $workAreaStr);
            } else {
                $enabledWorkAreas = [];
            }
            if (!empty($enabledWorkAreas)) {
                /** @var WorkArea $workArea */
                foreach ($allWorkAreaChoices as $workArea) {
                    if (in_array($workArea->getUid(), $enabledWorkAreas, false)) {
                        $workAreaChoices[] = $workArea;
                    }
                }
            } else {
                $workAreaChoices = $allWorkAreaChoices;
            }
        } else {
            $workAreaChoices = null;
        }

        $encryptionService = new EncryptionService();
        /** @var EventTitleProvider $provider */
        $provider = GeneralUtility::makeInstance(EventTitleProvider::class);
        $providerConfiguration = [];
        $provider->setTitleByEvent($event, $providerConfiguration);
        if (TcaUtility::getExtConf('disablePrivacyTermsForAuthenticatedUsers') && MiscUtility::getFrontendUserId() > 0) {
            $registration->setPrivacyTerms(true);
            $fixedProperties[] = 'privacyTerms';
        }

        $parameters = [
            'event' => $event,
            'registration' => $registration,
            'invitation' => $invitation,
            'enableInvitation' => $this->invitationService->isInvitationEnabledForEvent($event),
            'facilityChoices' => $facilityChoices,
            'workAreaChoices' => $workAreaChoices,
            'cryptKey' => $encryptionService->encrypt(date('Y-m-d H:i:s')),
            'enabledFields' => $this->getEnabledFields($event, $registration),
            'requiredFields' => $this->getRequiredFields($event, $registration),
            'enableRegistration' => $enableRegistration,
            'showDownloads' => $showDownloads,
            'notes' => '',
            'showLoginForm' => $showLoginForm,
            'userLoggedIn' => $userLoggedIn,
            'fixedProperties' => $fixedProperties,
            'registrationCancelPid' => (int)TcaUtility::getExtConf('registrationCancelPid'),
        ];
        $dispatchEvent = new ShowActionConfigurationEvent($parameters, $event);
        $this->eventDispatcher->dispatch($dispatchEvent);
        return $dispatchEvent->getViewParameters();
    }

    /**
     * Initialize invitation for registration if feature is enabled and invitation is valid for current event
     *
     * @param Registration $registration
     * @return Invitation|null
     */
    public function initInvitation(Registration $registration): ?Invitation
    {
        if ((null !== $event = $registration->getEvent())
            && InvitationManager::isAcceptInvitationEnabledForEvent($event)
            && $invitationHash = $this->frontendSessionHandlerService->getInvitationHash()) {
            $invitation = $this->invitationService->getInvitationByHash($invitationHash);
            if (null !== $invitation && $invitation->getEvent() === $event) {
                $registration->setInvitation($invitation);
                return $invitation;
            }
        }
        return null;
    }
}
