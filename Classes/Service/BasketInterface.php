<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

/**
 * Basket interface
 */
interface BasketInterface
{
    public const ITEM_PRICE_FIELD_DEFAULT = 'price';
    public const ITEM_PRICE_FIELD_GROUP_2 = 'price_member_type_2';
    public const ITEM_PRICE_FIELD_GROUP_3 = 'price_member_type_3';
    public const ITEM_PRICE_FIELD_GROUP_4 = 'price_member_type_4';
    public const ITEM_PRICE_FIELD_GROUP_5 = 'price_member_type_5';
}
