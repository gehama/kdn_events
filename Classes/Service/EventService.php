<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Utility\DbUtility;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Manage event
 */
class EventService extends AbstractService implements SingletonInterface
{

    /**
     * Load record from database
     *
     * @param int $registrationId
     *
     * @return array Table row array; null if empty
     */
    public function fetchRegistrationRow($registrationId)
    {
        return $this->fetchRowByFields($this->tables['registration'], ['uid' => $registrationId]);
    }


    /**
     * @param int|Event|null $eventModelOrId
     * @return int $eventRegistrationCount
     */
    public function updateEventRegistrationCount($eventModelOrId): int
    {
        /** @var WaitlistService $waitlistService */
        $waitlistService = $this->objectManager->get(WaitlistService::class);
        return $waitlistService->updateRegistrationCountAndWaitlist($eventModelOrId);
    }

    public function updateRegistrationWaitlist($record)
    {
        /** @var WaitlistService $waitlistService */
        $waitlistService = $this->objectManager->get(WaitlistService::class);
        $waitlistService->updateRegistrationWaitlist($record);
    }

    public function updateEventTimespan($id)
    {
        $row = $this->fetchRowByFields($this->tables['event'], ['uid' => $id]);
        $minTime = $this->fetchRowByFields(
            $this->tables['time'],
            ['event' => $id,],
            ['time_start' => 'ASC']
        );
        $maxTime = $this->fetchRowByFields(
            $this->tables['time'],
            ['event' => $id,],
            ['time_start' => 'DESC']
        );
        $bind = [];
        $now = time();
        if (!empty($minTime) && !empty($maxTime)) {
            if ((int)$minTime['uid'] !== (int)$maxTime['uid']) {
                if ($row['is_series_event'] && $minTime['time_start'] < $now && $maxTime['time_end'] > $now) {
                    $futureMinTime = $this->fetchRowByFields(
                        $this->tables['time'],
                        ['event' => $id],
                        ['time_start' => 'ASC'],
                        ['time_start > UNIX_TIMESTAMP()']
                    );
                    if (!empty($futureMinTime)) {
                        $minTime = $futureMinTime;
                    }
                }
            }
            $start = $minTime['time_start'];
            $end = $maxTime['time_end'];
            if (($start !== $row['event_start'] || $end !== $row['event_end'])) {
                $bind['event_start'] = $start;
                $bind['event_end'] = $end;
                if ($row['registration_end'] > $end) {
                    $bind['registration_end'] = $end;
                }
                if ($row['registration_start'] > $start) {
                    $bind['registration_start'] = $start;
                }
            }
            if ($end < $now && $row['published']) {
                $bind['published'] = 0;
            }
        }
        if (!empty($bind)) {
            $identifier = [
                'uid' => (int)$id
            ];
            $connection = DbUtility::getConnectionForTable($this->tables['event']);
            $connection->update($this->tables['event'], $bind, $identifier);
        }
        /*
        $sql = 'UPDATE tx_kdnevents_domain_model_event e
                SET e.event_start = (SELECT MIN(t.time_start) FROM tx_kdnevents_domain_model_time t WHERE t.time_start > 0 AND t.event = ?),
                    e.event_end = (SELECT MAX(t.time_end) FROM tx_kdnevents_domain_model_time t WHERE t.time_end > 0 AND t.event = ?),
                    e.registration_end = LEAST(e.registration_end, e.event_start),
                    e.registration_start = LEAST(e.registration_start, e.registration_end)
                WHERE e.uid = ?';
        $connection->executeUpdate($sql, [(int) $id, (int) $id, (int) $id]);*/
    }

    /**
     * Returns the page info for all pages containing event registrations
     *
     * @return array $pagesWithRegistrations
     */
    public function getRegistrationPagesInfo()
    {
        $table = $this->tables['registration'];
        $waitlistItem = null;
        $queryBuilder = DbUtility::getQueryBuilderForTable($table);
        /** @var DeletedRestriction $restriction */
        $restriction = GeneralUtility::makeInstance(DeletedRestriction::class);
        $queryBuilder->getRestrictions()->removeAll()->add($restriction);
        $queryBuilder->select('uid', 'pid', 'event');
        $queryBuilder->from($table);
        $rows = $queryBuilder->execute()->fetchAll();
        $pagesWithRegistrations = [];
        foreach ($rows as $record) {
            $pagesWithRegistrations[$record['pid']][$record['event']] = $record['event'];
        }
        return $pagesWithRegistrations;
    }

}
