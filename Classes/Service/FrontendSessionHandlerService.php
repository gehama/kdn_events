<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\AbstractEventPersonModel;
use KDN\KdnEvents\Domain\Model\Invitation;
use KDN\KdnEvents\Domain\Model\PersonInterface;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Domain\Model\WorkArea;
use KDN\KdnEvents\Utility\MiscUtility;
use KDN\KdnEvents\Utility\SessionUtility;
use KDN\KdnEvents\Utility\TcaUtility;
use Symfony\Component\PropertyAccess\PropertyAccess;
use TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;

/**
 * Frontend session handler
 */
class FrontendSessionHandlerService extends SessionHandlerService
{
    private const REGISTRATION_DATA_KEY = 'submittedRegistrationData';
    private const INVITATION_DATA_KEY = 'submittedInvitationData';
    private const INVITATION_HASH_KEY = 'acceptedInvitationHash';

    public const FIXED_USER_PROPERTIES = ['salutation', 'firstName', 'lastName', 'email', 'memberType'];

    /**
     * @var string
     */
    protected $mode = "FE";

    /**
     * User repository
     *
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     */
    protected $userRepository;

    /**
     * Work area repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\WorkAreaRepository
     */
    protected $workAreaRepository;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $frontendUser;

    /**
     * Skip properties when storing or loading from session
     *
     * @var array
     */
    protected $skipProperties = ['event', 'status', 'eventTime', 'invitedBy'];

    public function injectUserRepository(\TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function injectWorkAreaRepository(\KDN\KdnEvents\Domain\Repository\WorkAreaRepository $workAreaRepository)
    {
        $this->workAreaRepository = $workAreaRepository;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser|null
     */
    public function getCurrentFrontendUser()
    {
        if (null === $this->frontendUser && $this->sessionObject instanceof FrontendUserAuthentication
            && !empty($this->sessionObject->user['uid'])) {
            $this->frontendUser = $this->userRepository->findByUid($this->sessionObject->user['uid']);
        }
        return $this->frontendUser;
    }

    /**
     * Saves the registration data
     *
     * @param Registration|array $data
     */
    public function storeRegistrationData($data)
    {
        $this->storeSubmittedData($data, self::REGISTRATION_DATA_KEY);
    }

    /**
     * Saves the registration data
     *
     * @param Invitation|array $data
     */
    public function storeInvitationData($data)
    {
        $this->storeSubmittedData($data, self::INVITATION_DATA_KEY);
    }

    /**
     * Saves the registration data
     *
     * @param AbstractEventPersonModel|array $data
     * @param string $key The storage key
     * @throws \ReflectionException
     */
    private function storeSubmittedData($data, string $key)
    {
        if ($data instanceof AbstractEventPersonModel) {
            $flatObjectVars = ['_type' => get_class($data)];
            $reflectionClass = new \ReflectionClass($data);
            $properties = $reflectionClass->getProperties();
            $propertyAccess = PropertyAccess::createPropertyAccessor();
            foreach ($properties as $reflectionProperty) {
                $property = $reflectionProperty->getName();
                if (!in_array($property, $this->skipProperties, false)
                    && $propertyAccess->isReadable($data, $property)) {
                    $value = $propertyAccess->getValue($data, $property);
                    $this->addSessionValue($flatObjectVars, $property, $value);
                }
            }
            $this->store($key, $flatObjectVars);
        } else {
            $data['_type'] = 'array';
            $this->store($key, $data);
        }
    }

    public function storeInvitationHash(?string $hash = null)
    {
        if (null === $hash) {
            $this->delete(self::INVITATION_HASH_KEY);
        } else {
            $this->store(self::INVITATION_HASH_KEY, $hash);
        }
    }

    public function getInvitationHash(): string
    {
        return (string)$this->get(self::INVITATION_HASH_KEY);
    }

    /**
     * Add the given property value to the session data array
     *
     * @param array $flatObjectVars The object property values
     * @param string $property
     * @param $value
     */
    private function addSessionValue(array &$flatObjectVars, string $property, $value)
    {
        if (is_object($value)) {
            if ($value instanceof AbstractDomainObject) {
                $flatObjectVars[$property] = $value->getUid();
                $flatObjectVars['_' . $property] = get_class($value);
            } elseif ($value instanceof ObjectStorage) {
                $mappedArray = [];
                $objectClass = null;
                foreach ($value as $offset => $entity) {
                    $this->addSessionValue($mappedArray, $offset, $entity);
                    if (null === $objectClass) {
                        $objectClass = get_class($entity);
                    }
                }
                if (!empty($mappedArray)) {
                    $flatObjectVars[$property] = $mappedArray;
                    $flatObjectVars['_' . $property] = ObjectStorage::class;
                    $flatObjectVars['_storageObjectClass_' . $property] = $objectClass;
                }
            }
        } elseif (is_array($value)) {
            $mappedArray = [];
            foreach ($value as $vKey => $vSubVal) {
                $this->addSessionValue($mappedArray, $vKey, $vSubVal);
            }
            if (!empty($mappedArray)) {
                $flatObjectVars[$property] = $mappedArray;
                $flatObjectVars['_' . $property] = 'array';
            }
        } elseif (null !== $value) {
            $flatObjectVars[$property] = $value;
        }
    }

    /**
     * Pre fill user data in registration for new registration entities if user exists
     *
     * @param Registration $registration
     * @param bool $clearAfter Clear session data after values are set
     */
    public function prefillSessionRegistrationDataIfSet(Registration $registration, bool $clearAfter = true)
    {
        $this->prefillModelSessionDataIfSet($registration, $clearAfter, self::REGISTRATION_DATA_KEY);
    }

    /**
     * Pre fill user data in registration for new registration entities if user exists
     *
     * @param Invitation $invitation
     * @param bool $clearAfter Clear session data after values are set
     */
    public function prefillSessionInvitationDataIfSet(Invitation $invitation, bool $clearAfter = true)
    {
        $this->prefillModelSessionDataIfSet($invitation, $clearAfter, self::INVITATION_DATA_KEY);
    }

    /**
     * Pre fill user data in registration for new registration entities if user exists
     *
     * @param AbstractEventPersonModel $model
     * @param bool $clearAfter Clear session data after values are set
     * @param string $storageKey The storage key
     */
    private function prefillModelSessionDataIfSet(AbstractEventPersonModel $model, bool $clearAfter, string $storageKey)
    {
        $submittedData = $this->get($storageKey);
        if ($submittedData) {
            $propertyAccess = PropertyAccess::createPropertyAccessor();
            foreach ($submittedData as $key => $value) {
                if (!is_array($value) && strpos($key, '_') !== 0
                    && !in_array($key, $this->skipProperties, false)
                    && $propertyAccess->isWritable($model, $key)) {
                    $propertyAccess->setValue($model, $key, $value);
                } elseif ($key === 'workAreas' && $model instanceof Registration
                    && is_array($value) && !empty($value)) {
                    foreach ($value as $modelId) {
                        if (null !== $workAreaModel = $this->workAreaRepository->findByUid((int)$modelId)) {
                            /** @var WorkArea $workAreaModel */
                            $model->addWorkArea($workAreaModel);
                        }
                    }
                }
            }
            if ($clearAfter) {
                $this->delete($storageKey);
            }
        }
    }

    /**
     * Returns true, if the personal data for the logged in user cannot be changed (only in case a user is logged in)
     *
     * @return bool
     */
    public function isFixedUserData(): bool
    {
        return (bool)TcaUtility::getExtConf('disableAuthenticatedUserDataChange') && 0 <= MiscUtility::getFrontendUserId();
    }

    /**
     * Pre fill user data in registration for new registration entities if user exists
     *
     * @param AbstractEventPersonModel $model
     * @return array The field names that were set from the user data
     */
    public function prefillPersonDataForUserIfSet(AbstractEventPersonModel $model): array
    {
        $changedProperties = [];
        $user = $this->sessionObject->user;
        if (!empty($user)) {
            // Custom fields for gender or salutation (value must be either: 0 => male, 1 => female)
            $gender = SessionUtility::getUserProperty('gender');
            if (null === $gender) {
                $gender = SessionUtility::getUserProperty('salutation');
            }
            $disableAuthenticatedUserDataChange = $this->isFixedUserData();
            if (in_array($gender, [0, 1], false)) {
                $targetValue = $model->getSalutation();
                if ($disableAuthenticatedUserDataChange || null === $targetValue) {
                    $sourceValue = (int)$gender;
                    $salutation = $sourceValue === 1 ? PersonInterface::SALUTATION_FEMALE : PersonInterface::SALUTATION_MALE;
                    $model->setSalutation($salutation);
                    $changedProperties[] = 'salutation';
                }
            }
            $mapUserFields = [
                'email' => 'email',
                'title' => 'title',
                'first_name' => 'firstName',
                'last_name' => 'lastName',
                //'company' => 'organisation',
                'address' => 'street',
                'zip' => 'zipcode',
                'city' => 'town',
                'telephone' => 'phone',
                // Custom fields that don't normally exist in the fe_users table but MAY BE defined by custom extensions
                'mobile' => 'mobile',
                'member_type' => 'memberType',
            ];
            $changedProperties = array_merge($changedProperties, $this->setPropertiesFromUserData($model, $user, $mapUserFields));
        }
        return $changedProperties;
    }

    private function setPropertiesFromUserData(AbstractEventPersonModel $model, array $user, array $mapProperties): array
    {
        $changedProperties = [];
        $disableAuthenticatedUserDataChange = $this->isFixedUserData();
        $propertyAccess = PropertyAccess::createPropertyAccessor();
        foreach ($mapProperties as $sourceField => $targetProperty) {
            $sourceValue = $user[$sourceField] ?? null;
            if (null !== $sourceValue && $propertyAccess->isWritable($model, $targetProperty)) {
                $targetValue = $propertyAccess->getValue($model, $targetProperty);
                $useFixedUserValue = $disableAuthenticatedUserDataChange
                    && in_array($targetProperty, self::FIXED_USER_PROPERTIES, false);
                if ($useFixedUserValue || (empty($targetValue) && !empty($sourceValue))) {
                    $propertyAccess->setValue($model, $targetProperty, $sourceValue);
                    $changedProperties[] = $targetProperty;
                }
            }
        }
        return $changedProperties;
    }
}