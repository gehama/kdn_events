<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

if (!class_exists('PHPExcel')) {

    $libBaseDir = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('kdn_events') . 'Resources/Private/PHP/PHPExcel/';
    require_once $libBaseDir . 'PHPExcel.php';
    //Needed for date formatting
    require_once $libBaseDir . 'PHPExcel/Cell/AdvancedValueBinder.php';
    require_once $libBaseDir . 'PHPExcel/Style/NumberFormat.php';
    //require_once("vendor/autoload.php");
}

/**
 * Abstract export service
 */
class ExcelExportService extends AbstractExportService
{

    /**
     * @var \PHPExcel
     */
    private $phpExcel;

    public function __construct()
    {
        $this->phpExcel = new \PHPExcel();
    }

    public function create($sheetName, $rows)
    {

        $sheet = $this->phpExcel->setActiveSheetIndex(0);
        $sheet->setTitle($sheetName);
        $sheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $sheet->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $sheet->getPageSetup()->setFitToWidth(1);

        $style = $this->phpExcel->getDefaultStyle();
        $font = $style->getFont();
        $font->setName(self::FONT_NAME);
        $font->setSize(self::FONT_SIZE);
        \PHPExcel_Cell::setValueBinder(new \PHPExcel_Cell_AdvancedValueBinder());

        $fields = $this->fields;
        $columnWidths = array();
        $maxColWidth = self::MAX_COL_WIDTH;

        $rowNr = 1;
        $setBold = true;

        foreach ($fields as $offset => $field) {
            $heading = $this->translate($field);
            $cell = $sheet->getCellByColumnAndRow($offset, $rowNr);
            $cellId = $cell->getCoordinate();
            $cell->setValue($heading);
            $this->setCellFont($cellId, 10, $setBold, 'center', 'center');
            $this->setCellBorder($cellId, 'left', 'CCCCCC');
            $this->enableTextWrap($cellId);
            $columnWidths[$cell->getColumn()] = max(9, round(strlen($heading) * 1.25));
        }

        /** @var \KDN\KdnEvents\Domain\Model\Registration $row */
        foreach ($rows as $row) {
            ++$rowNr;
            foreach ($fields as $offset => $field) {
                $fieldVal = $this->getRowFieldVal($row, $field);
                $cell = $sheet->getCellByColumnAndRow($offset, $rowNr);
                $cellId = $cell->getCoordinate();
                $cell->setValue($fieldVal);
                $this->setCellFont($cellId, 10, false, 'left', 'top');
                $this->enableTextWrap($cellId);

                $columnWidths[$cell->getColumn()] = max(
                    $columnWidths[$cell->getColumn()],
                    min($maxColWidth, round(strlen($fieldVal) * 1.25))
                );
            }
        }

        foreach ($columnWidths as $colId => $colWidth) {
            $this->setColWidth($colId, $colWidth);
        }

        // Set date format for crdate column
        $dateFormat = 'dd.mm.yyyy hh:mm';
        $this->setNumberFormat('I1:I' . $rowNr, $dateFormat);
    }

    /**
     * Set font for given table cell
     *
     * @param string $cellNr
     * @param int $fontSize
     * @param bool $setBold
     * @param string $hAlign
     * @param string $vAlign
     * @param bool $underline
     */
    private function setCellFont(
        $cellNr,
        $fontSize,
        $setBold = false,
        $hAlign = 'left',
        $vAlign = 'top',
        $underline = false
    )
    {
        $style = $this->phpExcel->getActiveSheet()->getStyle($cellNr);
        $style->getFont()->setSize($fontSize)->setBold($setBold);
        $style->getAlignment()->setHorizontal($hAlign);
        $style->getAlignment()->setVertical($vAlign);
        if ($underline) {
            $style->getFont()->setUnderline(\PHPExcel_Style_Font::UNDERLINE_SINGLE);
        }
    }

    /**
     * Add border for given table cell
     *
     * @param string $cellNr
     * @param string $borderType top, left, bottom, right, outline
     * @param string|null $color
     * @param string|null $lineStyle
     */
    private function setCellBorder($cellNr, $borderType = 'outline', $color = null, $lineStyle = null)
    {
        if ($lineStyle === null) {
            $lineStyle = \PHPExcel_Style_Border::BORDER_HAIR;
        }
        if ($color === null) {
            $color = '000000';
        }
        $borderStyle = [
            'borders' => [
                $borderType => [
                    'style' => $lineStyle,
                    'color' => ['argb' => 'FF' . $color],
                ],
            ],
        ];
        $this->getActiveSheet()->getStyle($cellNr)->applyFromArray($borderStyle);
    }

    private function getActiveSheet()
    {
        return $this->phpExcel->getActiveSheet();
    }

    /**
     * Set number format for given cell
     *
     * @param string $cellNr
     * @param string $format
     */
    private function setNumberFormat($cellNr, $format = \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1)
    {
        $style = $this->getActiveSheet()->getStyle($cellNr);
        $style->getNumberFormat()->setFormatCode($format);
    }

    /**
     * Set column width for given column
     *
     * @param string $colName
     * @param float $colWidth
     */
    private function setColWidth($colName, $colWidth)
    {
        $this->getActiveSheet()->getColumnDimension($colName)->setWidth($colWidth);
    }

    /**
     * Enable text wrap for given cell
     *
     * @param string $cellNr
     */
    private function enableTextWrap($cellNr)
    {
        $this->getActiveSheet()->getStyle($cellNr)->getAlignment()->setWrapText(true);
    }

    /**
     * @param string $fileBaseName File name without file type ending
     * @param string $fileType The file type
     *
     * @throws \PHPExcel_Reader_Exception
     */
    public function sendFile($fileBaseName, $fileType)
    {
        $fileName = $this->filterFileName(str_replace('.', '_', $fileBaseName)) . '.' . $fileType;
        $writerType = 'Excel2007';
        if ($fileType === 'xlsx') {
            // Redirect output to a client's web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        } elseif ($fileType === 'pdf') {
            header('Content-Type: application/pdf');
            $writerType = 'PDF';
        }
        header('Content-Disposition: attachment;filename="' . basename($fileName) . '"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($this->phpExcel, $writerType);
        $objWriter->save('php://output');
        exit;
    }
}
