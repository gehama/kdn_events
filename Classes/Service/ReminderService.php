<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Domain\Model\Time;
use KDN\KdnEvents\Domain\Repository\RegistrationRepository;
use KDN\KdnEvents\Domain\Repository\TimeRepository;
use KDN\KdnEvents\Utility\ObjectUtility;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Reminders for event registration
 */
class ReminderService extends AbstractService implements SingletonInterface
{
    public function sendReminders($daysBefore = 7)
    {
        // Manually set language for command controller calls
        ObjectUtility::getLanguageService()->init('de');
        /** @var TimeRepository $eventTimeRepository */
        $eventTimeRepository = $this->objectManager->get(TimeRepository::class);
        $eventTimes = $eventTimeRepository->findForReminders($daysBefore);
        /** @var RegistrationRepository $repository */
        $registrationRepository = $this->objectManager->get(RegistrationRepository::class);
        $filterDuplicates = [];
        foreach ($eventTimes as $eventTime) {
            $event = $eventTime->getEvent();
            if (!in_array($event->getUid(), $filterDuplicates, false)) {
                $filterDuplicates[] = $event->getUid();
                /** @var Registration[] $registrations */
                $registrations = $event->getRegistrationList();
                foreach ($registrations as $registration) {
                    if ($registration->getStatus() === Registration::STATUS_REGISTERED
                        && $registration->isReminderEnabled()) {
                        if ($this->sendRegistrationNotification($registration, $eventTime)) {
                            $registration->setReminderStatus(Registration::REMINDER_STATUS_SENT);
                            $registrationRepository->update($registration);
                        }
                    }
                }
            }
        }
        /** @var PersistenceManager $persistenceManager */
        $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
        $persistenceManager->persistAll();
    }

    private function sendRegistrationNotification(Registration $registration, Time $eventTime)
    {
        $emailTemplateKey = MailService::TEMPLATE_KEY_REMINDER_EMAIL_CUSTOMER;
        /** @var MailService $mailService */
        $mailService = $this->objectManager->get(MailService::class);
        $mailService->setMode('BE');
        $pid = $registration->getPid();
        $pluginTs = self::loadEventsTS($pid);
        $mailSettings = $pluginTs['mail.'];
        $userMailSettings = $mailSettings;
        $emailTemplate = $mailService->updateMailSettingsFromEmailTemplate($userMailSettings, $emailTemplateKey, $pid);
        $recipient = $registration->getEmail();
        return $mailService->sendMailWithTemplate(
            $registration,
            $recipient,
            $userMailSettings,
            $emailTemplateKey,
            [],
            $eventTime
        );
    }
}
