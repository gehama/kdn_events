<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * Abstract export service
 */
abstract class AbstractExportService extends AbstractService
{
    protected $fields = [
        'last_name',// A
        'first_name',// B
        'salutation',//C
        'title',//D
        'email',// E
        'birth_date',// F
        'street',// G
        'zipcode',// H
        'town',// I
        'phone',// J
        'occupation',// K
        'work_experience',// L
        'occupation',// M
        'facility',// N
        'status',// O
        'event',// P
        'event_start',// Q
        'event_end',// R
        'crdate',// S
        'organisation',// T
        'organisation_street',// U
        'organisation_zipcode',// V
        'organisation_town',// W
        'organisation_email',// X
        'organisation_phone',// Y
        'customer_message',// Z
        'internal_remarks',// AA
        'work_areas',//BA
    ];

    public const FONT_NAME = 'Arial';
    public const FONT_SIZE = 10;
    public const MAX_COL_WIDTH = 25;

    abstract public function create($sheetName, $rows);
    abstract public function sendFile($fileBaseName, $fileType);

    /**
     * Returns the string value of the given object property
     * @param object $row
     * @param string $field
     * @return string
     */
    protected function getRowFieldVal($row, $field): string
    {
        $getter = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $field)));
        $fieldVal = $getter;
        if (method_exists($row, $getter)) {
            $fieldVal = $this->convertValueToString($row->$getter());
            if ($field === 'status' || $field === 'salutation') {
                $fieldVal = $this->translate($field . '.' . $fieldVal);
            }
        }
        return (string) $fieldVal;
    }

    /**
     * Convert the given field value to string
     *
     * @param mixed $fieldValue
     * @return string
     */
    private function convertValueToString($fieldValue): string
    {
        $convertedValue = $fieldValue;
        if (is_object($fieldValue)) {
            if ($fieldValue instanceof \DateTime) {
                $convertedValue = $fieldValue->format('d.m.Y H:i');
            } elseif ($fieldValue instanceof ObjectStorage) {
                $subValues = [];
                foreach ($fieldValue as $childValue) {
                    $subValues[] = $this->convertValueToString($childValue);
                }
                $convertedValue = implode(', ', array_filter($subValues));
            } elseif (method_exists($fieldValue, 'getName')) {
                $convertedValue = $fieldValue->getName();
            } elseif (method_exists($fieldValue, 'getTitle')) {
                $convertedValue = $fieldValue->getTitle();
            }
        }
        return (string) $convertedValue;
    }

    /**
     * Removes all special characters from a given file name.
     * E.g. Useful for auto-generated file names or renaming files
     *
     * @param string $fileName Current filename
     *
     * @return string New clean file name
     */
    protected function filterFileName($fileName): string
    {
        $dir = dirname($fileName);
        if (strlen($dir) > 1) {
            $dir .= '/';
        } else {
            $dir = '';
        }
        $basename = basename($fileName);
        $replace = [
            " " => '_',
            "%20" => "_",
            'ß' => 'ss',
            'ü' => 'ue',
            'Ü' => 'Ue',
            'ä' => 'ae',
            'Ä' => 'Ae',
            'ö' => 'oe',
            'Ö' => 'Oe',
        ];
        $search = array_keys($replace);
        // clean up file name
        $newFileName = str_replace($search, $replace, $basename);
        $newFileName = $dir
            . preg_replace("/[^0-9a-zA-Z\-_\.]+/", "_", $newFileName);
        $newFileName = trim(str_replace('__', '_', $newFileName), '_');
        return $newFileName;
    }
}
