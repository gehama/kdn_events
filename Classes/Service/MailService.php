<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\AbstractEventPersonModel;
use KDN\KdnEvents\Domain\Model\EmailTemplate;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Domain\Model\Time;
use KDN\KdnEvents\Domain\Repository\EmailTemplateRepository;
use Psr\Log\LoggerInterface;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;

/**
 * Abstract away sending emails into a service:
 */
class MailService extends AbstractService implements SingletonInterface
{

    public const TEMPLATE_KEY_CANCEL_INFO_CUSTOMER = 'cancel_info_customer';
    public const TEMPLATE_KEY_REGISTRATION_EMAIL_CUSTOMER = 'registration_email_customer';
    public const TEMPLATE_KEY_REGISTRATION_EMAIL_CUSTOMER_WAITLIST = 'registration_email_customer_waitlist';
    public const TEMPLATE_KEY_REGISTRATION_EMAIL_SERVICE = 'registration_email_service';
    public const TEMPLATE_KEY_REGISTRATION_CANCEL_EMAIL_SERVICE = 'registration_cancel_email_service';
    public const TEMPLATE_KEY_WAITLIST_UPDATE_CUSTOMER = 'waitlist_update_customer';
    public const TEMPLATE_KEY_REMINDER_EMAIL_CUSTOMER = 'reminder_email_customer';
    public const TEMPLATE_KEY_WAITLIST_UPDATE_SERVICE = 'waitlist_update_service';

    /**
     * @var MarkerService
     */
    protected $markerService;

    /**
     * @param MarkerService $markerService
     */
    public function injectMarkerService(MarkerService $markerService)
    {
        $this->markerService = $markerService;
    }

    /**
     * Send a mail via html mailer
     *
     * @param string $recipient
     * @param string $subject
     * @param string $messageHtml
     * @param string $messagePlain
     * @param array $mailSettings
     * @param array $attachments
     * @return bool
     */
    private function send(
        $recipient,
        $subject,
        $messageHtml,
        $messagePlain,
        $mailSettings,
        array $attachments = []
    ): bool
    {
        $status = TRUE;
        /** @var MailMessage $mailer */
        $mailer = GeneralUtility::makeInstance(MailMessage::class);
        try {
            $mailer->setFrom(array($mailSettings['fromEmail'] => $mailSettings['fromName']));
            if (!empty($mailSettings['forceSenderEmail'])) {
                $mailer->setSender($mailSettings['forceSenderEmail']);
                $mailer->setReplyTo($mailSettings['fromEmail']);
            } else {
                $mailer->setSender($mailSettings['fromEmail'], $mailSettings['fromName']);
            }
            if (!empty($mailSettings['replyTo'])) {
                $mailer->setReplyTo($mailSettings['replyTo']);
            }
            if (!empty($mailSettings['returnPath'])) {
                $mailer->setReturnPath($mailSettings['returnPath']);
            }
            $mailer->setTo($recipient)
                ->setSubject($subject);
            if (!empty($mailSettings['bccEmail'])) {
                $mailer->setBcc($mailSettings['bccEmail']);
            }
            if (!empty($mailSettings['ccEmail'])) {
                $mailer->setCc($mailSettings['ccEmail']);
            }
            $messagePlain = str_replace(['&ndash;', '&amp;', '&nbsp;'], ['-', '&', ' '], $messagePlain);
            $this->setMailParts($mailer, $messageHtml, $messagePlain, $attachments, $mailSettings);
            $mailer->send();
        } catch (\Exception $e) {
            $this->getLogger()->log(LogLevel::ERROR, $e->getMessage(), ['extension' => 'kdn_events']);
            $status = !$status;
        }

        return $status;
    }

    protected function isGteVersion10(): bool
    {
        $cmsVersion = (string)\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Information\Typo3Version::class);
        return \TYPO3\CMS\Core\Utility\VersionNumberUtility::convertVersionNumberToInteger($cmsVersion) > 10000000;
    }

    protected function setMailParts(
        MailMessage $mailer,
        string $messageHtml,
        string $messagePlain,
        array $attachments,
        array $mailSettings
    )
    {
        if ($this->isGteVersion10()) {
            if (!empty($messageHtml)) {
                if ($mailSettings['embedImages']) {
                    $messageHtml = $this->embedImages($mailer, $messageHtml);
                }
                $mailer->html($messageHtml);
                $mailer->text($messagePlain);
            } else {
                $mailer->text($messagePlain);
            }
            if (!empty($attachments)) {
                foreach ($attachments as $absPath) {
                    $mailer->attachFromPath($absPath);
                }
            }
        } else {
            if (!empty($messageHtml)) {
                if ($mailSettings['embedImages']) {
                    $messageHtml = $this->embedImages($mailer, $messageHtml);
                }
                $mailer->setBody($messageHtml, 'text/html');
                $mailer->addPart($messagePlain, 'text/plain');
            } else {
                $mailer->setBody($messagePlain, 'text/plain');
            }
            if (!empty($attachments)) {
                foreach ($attachments as $absPath) {
                    $mailer->attach(\Swift_Attachment::fromPath($absPath));
                }
            }
        }
    }

    protected function getLogger(): LoggerInterface
    {
        /** @var LogManager $logManager */
        $logManager = GeneralUtility::makeInstance(LogManager::class);
        return $logManager->getLogger(__CLASS__);
    }

    /**
     * Embed image in mail instead of only sending the link to the images.
     * This is necessary when the email contains temporary images
     *
     * @param MailMessage $mailer
     * @param string $htmlPart
     * @return string
     */
    private function embedImages(MailMessage $mailer, $htmlPart): string
    {
        $matches = array();
        //Search for all src attributes
        $pattern = '/src=[\'"]?([^\'" >]+)[\'" >]/';
        preg_match_all($pattern, $htmlPart, $matches);
        if (!empty($matches[1])) {
            $imgSrcArr = $matches[1];
            $baseUrl = isset($GLOBALS['TSFE']) && $GLOBALS['TSFE']->tmpl->setup['config.']['baseURL']
                ? $GLOBALS['TSFE']->tmpl->setup['config.']['baseURL']
                : GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
            $relDirs = array('fileadmin/', 'uploads/', 'typo3temp/', 'typo3temp/');
            foreach ($imgSrcArr as $imgSrc) {
                //if image is used more than once, make sure it is embedded only once
                if (strpos($htmlPart, $imgSrc) !== false) {
                    $imgRelPath = str_replace($baseUrl, '', $imgSrc);
                    foreach ($relDirs as $subDir) {
                        $relDirStartPos = strpos($imgRelPath, $subDir);
                        if ($relDirStartPos > 0) {
                            $imgRelPath = substr($imgRelPath, $relDirStartPos);
                            break;
                        }
                    }
                    $absolutePath = Environment::getPublicPath() . DIRECTORY_SEPARATOR
                        . ltrim($imgRelPath, DIRECTORY_SEPARATOR);
                    if ($this->isGteVersion10()) {
                        $cid = $mailer->attachFromPath($absolutePath);
                    } else {
                        $cid = $mailer->embed(\Swift_Image::fromPath($absolutePath));
                    }
                    $htmlPart = str_replace($imgSrc, $cid, $htmlPart);
                }
            }
        }
        return $htmlPart;
    }

    /**
     * Loads the email template with the given template key
     *
     * @param string $emailTemplateKey
     * @param integer|null $pid Storage page id for email templates
     * @return EmailTemplate|null
     */
    private function findEmailTemplate($emailTemplateKey, ?int $pid = null): ?EmailTemplate
    {
        $emailTemplate = null;
        /** @var EmailTemplateRepository $repository */
        $repository = $this->objectManager->get(EmailTemplateRepository::class);
        /** @var Typo3QuerySettings $defaultQuerySettings */
        $defaultQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $defaultQuerySettings->setRespectStoragePage(FALSE);
        // don't add fields from enable fields constraint
        //$defaultQuerySettings->setRespectEnableFields(FALSE);
        // don't add sys_language_uid constraint
        $defaultQuerySettings->setRespectSysLanguage(FALSE);
        $repository->setDefaultQuerySettings($defaultQuerySettings);
        $emailTemplateList = $repository->findByTemplateKey($emailTemplateKey);
        if (count($emailTemplateList) > 0) {
            foreach ($emailTemplateList as $checkTemplate) {
                /** @var EmailTemplate $checkTemplate */
                if (null === $emailTemplate) {
                    $emailTemplate = $checkTemplate;
                }
                if ((int)$pid > 0 && $checkTemplate->getPid() === (int)$pid) {
                    $emailTemplate = $checkTemplate;
                    break;
                }
            }
            /** @var EmailTemplate|null $emailTemplate */
        }
        return $emailTemplate;
    }

    /**
     * Updates the mail settings with the values of the given email template
     *
     * @param array $mailSettings The default mail settings
     * @param string $emailTemplateKey
     * @param integer|null $pid Storage page id for email templates
     * @return \KDN\KdnEvents\Domain\Model\EmailTemplate|null
     */
    public function updateMailSettingsFromEmailTemplate(array &$mailSettings, $emailTemplateKey, $pid = null): ?\KDN\KdnEvents\Domain\Model\EmailTemplate
    {
        $emailTemplate = $this->findEmailTemplate($emailTemplateKey, $pid);
        if ($emailTemplate !== null) {
            if ($emailTemplate->getAdminEmail()) {
                $mailSettings['adminEmail'] = $emailTemplate->getAdminEmail();
                $mailSettings['bccEmail'] = $emailTemplate->getAdminEmail();
            }
            if ($emailTemplate->getSenderEmail()) {
                $mailSettings['fromEmail'] = $emailTemplate->getSenderEmail();
            }
            if ($emailTemplate->getSenderName()) {
                $mailSettings['fromName'] = $emailTemplate->getSenderName();
            }
            if ($emailTemplate->getReplyToEmail()) {
                $mailSettings['replyTo'] = $emailTemplate->getReplyToEmail();
            }
        }
        return $emailTemplate;
    }

    /**
     * @param Registration $registration
     * @param string $recipient Recipient email
     * @param array $mailSettings
     * @param EmailTemplate|string $emailTemplateKey Optional email template key (instead of subject and body)
     * @param string $subjectTpl Template content for email subject (may contain markers)
     * @param string $bodyTpl Template content for email body (may contain markers)
     * @param bool $isHtml Set to true if body content is HTML code
     * @param array $attachments
     * @param Time|null $eventTime
     * @return bool
     */
    public function sendRegistrationMail(
        Registration $registration,
        $recipient,
        $mailSettings,
        $emailTemplateKey = null,
        $subjectTpl = null,
        $bodyTpl = null,
        $isHtml = false,
        array $attachments = [],
        Time $eventTime = null
    ): bool
    {
        $markerService = $this->markerService;
        $markerService->setMode($this->mode);
        $markers = $markerService->getRegistrationMarkers($registration, $eventTime);
        return $this->sendMarkerMail(
            $registration,
            $markers,
            $recipient,
            $mailSettings,
            $emailTemplateKey,
            $subjectTpl,
            $bodyTpl,
            $isHtml,
            $attachments
        );
    }

    /**
     * @param Registration $registration
     * @param string $recipient Recipient email
     * @param array $mailSettings
     * @param EmailTemplate|string $emailTemplateKey Optional email template key (instead of subject and body)
     * @param array $attachments
     * @param Time|null $eventTime
     * @return bool
     */
    public function sendMailWithTemplate(
        Registration $registration,
        $recipient,
        $mailSettings,
        $emailTemplateKey = null,
        array $attachments = array(),
        Time $eventTime = null
    ): bool
    {
        $markerService = $this->markerService;
        $markerService->setMode($this->mode);
        $markers = $markerService->getRegistrationMarkers($registration, $eventTime);
        return $this->sendMarkerMail(
            $registration,
            $markers,
            $recipient,
            $mailSettings,
            $emailTemplateKey,
            null,
            null,
            false,
            $attachments
        );
    }

    /**
     * @param AbstractDomainObject $model
     * @param array $markers
     * @param string $recipient Recipient email
     * @param array $mailSettings
     * @param EmailTemplate|string $emailTemplateKey Optional email template key (instead of subject and body)
     * @param string $subjectTpl Template content for email subject (may contain markers)
     * @param string $bodyTpl Template content for email body (may contain markers)
     * @param bool $isHtml Set to true if body content is HTML code
     * @param array $attachments
     * @return bool
     */
    protected function sendMarkerMail(
        AbstractDomainObject $model,
        array $markers,
        $recipient,
        $mailSettings,
        $emailTemplateKey = null,
        $subjectTpl = null,
        $bodyTpl = null,
        $isHtml = false,
        array $attachments = array()
    ): bool
    {
        $search = array_keys($markers);

        if ($emailTemplateKey instanceof EmailTemplate) {
            $emailTemplate = $emailTemplateKey;
        } elseif (!empty($emailTemplateKey)) {
            $emailTemplate = $this->findEmailTemplate($emailTemplateKey, $model->getPid());
        } else {
            $emailTemplate = null;
        }
        if (null !== $emailTemplate) {
            $subjectTpl = $emailTemplate->getSubject();
            $bodyTpl = $emailTemplate->getBody();
            if (!empty($mailSettings['is_admin_email']) && $emailTemplate->getAdminEmail()) {
                $recipient = $emailTemplate->getAdminEmail();
            }/* else {
                    $mailSettings['bccEmail'] = $emailTemplate->getAdminEmail();
                }*/
            if ($emailTemplate->getReplyToEmail()) {
                $mailSettings['replyTo'] = $emailTemplate->getReplyToEmail();
            }
            if ($emailTemplate->getSenderEmail()) {
                $mailSettings['fromEmail'] = $emailTemplate->getSenderEmail();
            }
            if ($emailTemplate->getSenderName()) {
                $mailSettings['fromName'] = $emailTemplate->getSenderName();
            }
            if (!empty($mailSettings['override']) && is_array($mailSettings['override'])) {
                foreach ($mailSettings['override'] as $oKey => $oValue) {
                    $mailSettings[$oKey] = $oValue;
                }
                if (!empty($mailSettings['is_admin_email']) && array_key_exists('adminEmail', $mailSettings['override'])) {
                    $recipient = $mailSettings['override']['adminEmail'];
                }
            }
            $this->addAttachmentsFromTemplate($emailTemplate, $attachments);
        } elseif (null !== $emailTemplateKey) {
            $message = sprintf('No email template found for key %s', $emailTemplate);
            $this->getLogger()->log(LogLevel::ERROR, $message, ['extension' => 'kdn_events']);
            if ($emailTemplateKey === self::TEMPLATE_KEY_REGISTRATION_EMAIL_SERVICE) {
                $subjectTpl = 'New event registration';
                $bodyTpl = '';
                foreach ($search as $marker) {
                    $bodyTpl = str_replace('#', '', $marker) . ': ' . $marker . "\n";
                }
            }
        }
        $status = false;
        if (!empty($subjectTpl) && !empty($bodyTpl)) {
            $subject = str_replace($search, $markers, $subjectTpl);
            $messagePlain = str_replace($search, $markers, $bodyTpl);
            if ($isHtml
                || strpos($messagePlain, '<br>') !== false
                || strpos($messagePlain, '</p>') !== false) {
                $messageHtml = $messagePlain;
                $messagePlain = trim(strip_tags($messagePlain));
            } else {
                $messageHtml = nl2br($messagePlain);
            }
            $lines = explode(PHP_EOL, str_replace($search, $markers, $bodyTpl));
            $emptyLineCount = 0;
            // Remove more than two consecutive empty lines in plain email text
            $messagePlain = '';
            foreach ($lines as $line) {
                $trimmedLine = trim($line);
                if (empty($trimmedLine)) {
                    ++$emptyLineCount;
                    if ($emptyLineCount < 2) {
                        $messagePlain .= $trimmedLine . PHP_EOL;
                    }
                } else {
                    $emptyLineCount = 0;
                    $messagePlain .= $trimmedLine . PHP_EOL;
                }
            }
            if (strpos($messageHtml, '<body') === false) {
                $messageHtml = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml" lang="de">
                     <head>
                      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                      <title>' . $subject . '</title>
                      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                    </head>
                    <body style="margin: 0; padding: 0;">' . $messageHtml . '</body>
                    </html>';
            }
            if (($model instanceof AbstractEventPersonModel || method_exists($model, 'getEmail'))
                && (strpos($model->getEmail(), 'gerthammes') !== false
                    || strpos($model->getEmail(), '@example.org') !== false
                    || strpos($model->getEmail(), '@example.com') !== false)) {
                $messagePlain .= "\n\nrecipient: " . $recipient;
                $messagePlain .= "\n\nmailSettings: " . print_r($mailSettings, true);
                $messagePlain .= "\n\nregistration: " . $model->getUid();
                $recipient = $model->getEmail();
                $mailSettings['bccEmail'] = [];
                $mailSettings['ccEmail'] = [];
            }
            // Code for using rich text editor
            //$bodyRte = str_replace($search, $markers, $bodyTpl);
            //$body = $contentObject->parseFunc($bodyRte, array(), '< ' . 'lib.parseFunc_RTE');
            //$bodyPlain = trim(strip_tags($body));
            $status = $this->send($recipient, $subject, $messageHtml, $messagePlain, $mailSettings, $attachments);
        }
        return $status;
    }

    public function addAttachmentsFromTemplate(?EmailTemplate $emailTemplate, array &$attachments)
    {
        if (null !== $emailTemplate) {
            $publicBasePath = Environment::getPublicPath() . '/';
            foreach ($emailTemplate->getAttachments() as $templateAttachment) {
                /** @var \TYPO3\CMS\Extbase\Domain\Model\FileReference $templateAttachment */
                $absAttachmentPath = $publicBasePath . $templateAttachment->getOriginalResource()->getPublicUrl();
                if (file_exists($absAttachmentPath)) {
                    $attachments[] = $absAttachmentPath;
                }
            }
        }
    }
}
