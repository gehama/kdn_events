<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Repository\EventRepository;
use KDN\KdnEvents\Utility\DbUtility;
use KDN\KdnEvents\Utility\TcaUtility;
use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Abstract event service
 */
abstract class AbstractService
{
    protected $tables = array(
        'event' => 'tx_kdnevents_domain_model_event',
        'registration' => 'tx_kdnevents_domain_model_registration',
        'time' => 'tx_kdnevents_domain_model_time',
    );

    protected $translationPrefix = 'tx_kdnevents_domain_model_registration.';

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var UriBuilder
     */
    protected $uriBuilder;

    /**
     * @var string
     */
    protected $mode = '';

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager
     */
    public function injectObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager
     */
    public function setObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * Load record from database
     *
     * @param string $table
     * @param array $identifiers
     * @param array $orderBy
     * @param string[] $extraConditions
     * @return array Table row array; null if empty
     */
    public function fetchRowByFields($table, $identifiers, $orderBy = [], $extraConditions = []): ?array
    {
        return DbUtility::fetchRowByFields($table, $identifiers, $orderBy, $extraConditions);
    }

    protected function translate($key): ?string
    {
        return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($this->translationPrefix . $key, 'kdn_events');
    }

    /**
     * @param UriBuilder $uriBuilder
     */
    public function setUriBuilder(UriBuilder $uriBuilder)
    {
        $this->uriBuilder = $uriBuilder;
    }

    /**
     * Returns the uri builder for creating frontend links
     *
     * @return UriBuilder|null
     */
    protected function getUriBuilder(): ?UriBuilder
    {
        if (null === $this->uriBuilder && null !== $this->objectManager) {
            /** @var ConfigurationManager $configurationManager */
            $configurationManager = $this->objectManager->get(ConfigurationManager::class);
            /** @var ContentObjectRenderer $contentObjectRenderer */
            $contentObjectRenderer = $this->objectManager->get(ContentObjectRenderer::class);
            $configurationManager->setContentObject($contentObjectRenderer);
            /** @var UriBuilder $uriBuilder */
            $uriBuilder = $this->objectManager->get(UriBuilder::class);
            $uriBuilder->injectConfigurationManager($configurationManager);
            $this->uriBuilder = $uriBuilder;
        }
        return $this->uriBuilder;
    }

    /**
     * Loads the event TypoScript for the given page
     *
     * @param int|null $pageUid
     * @param bool $convertToDefaultArray
     * @param string $type
     * @return array|null
     */
    public static function loadEventsTS(?int $pageUid, bool $convertToDefaultArray = false, string $type = 'plugin.'): ?array
    {
        $settings = null;
        $template = TcaUtility::getTemplateServiceForPage($pageUid);

        if (!empty($template->setup[$type][TcaUtility::EXT_CONFIG_KEY]['settings.'])) {
            $settings = $template->setup[$type][TcaUtility::EXT_CONFIG_KEY]['settings.'];
            if ($convertToDefaultArray) {
                /** @var TypoScriptService $tsService */
                $tsService = GeneralUtility::makeInstance(TypoScriptService::class);
                $settings = $tsService->convertTypoScriptArrayToPlainArray($settings);
            }
        }
        return $settings;
    }


    /**
     * @param int|Event|null $eventModelOrId
     * @return array<int, Event>
     */
    protected function transformEventModelOrId($eventModelOrId): array
    {
        if ($eventModelOrId instanceof Event) {
            $eventId = $eventModelOrId->getUid();
            $event = $eventModelOrId;
        } else {
            $eventId = (int)$eventModelOrId;
            if ($eventId > 0) {
                /** @var EventRepository $eventRepository */
                $eventRepository = $this->objectManager->get(EventRepository::class);
                /** @var \KDN\KdnEvents\Domain\Model\Event $event */
                $event = $eventRepository->findByUid($eventId);
            } else {
                $event = null;
            }
        }
        return [
            'eventId' => $eventId,
            'event' => $event,
        ];
    }
}
