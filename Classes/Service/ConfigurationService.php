<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Utility\TcaUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Extension configuration service
 */
class ConfigurationService
{
    public const FIELD_ALWAYS_ENABLED = 1;
    public const FIELD_ALWAYS_DISABLED = -1;

    /**
     * Properties must be enabled for billing if no address exists (for registered user)
     * Use lower camel case property names here!
     *
     * @var string[]
     */
    private $billingPropertyRequiredMap = [
        'street' => true,
        'zipcode' => true,
        'town' => true,
        'organisation' => false,
    ];

    /**
     * Available optional fields for registration (defined in TCA)
     *
     * Use snake camel case field names here!
     * @var string[]
     */
    private $availableTcaEnableFields = null;

    /**
     * Available optional fields for registration
     *
     * Use snake camel case field names here!
     * @var string[]
     */
    private $availableEnableFields = [
        'title', 'street', 'zipcode', 'town', 'occupation', 'organisation',
        'organisation_email', 'organisation_phone', 'facility', 'customer_message',
        'work_areas',
        //'birth_date', 'work_experience',
        //'organisation_street', 'organisation_zipcode', 'organisation_town',
    ];

    /**
     * @var array
     */
    private $fieldsAlwaysVisible;

    /**
     * @var array
     */
    private $fieldsAlwaysExcluded;

    /**
     * Initialize the globally defined always visible fields
     * @return void
     */
    private function initAlwaysVisibleFields()
    {
        if (null === $this->fieldsAlwaysVisible) {
            $baAddFieldsAlwaysVisibleFields = (string) TcaUtility::getExtConf('baAddFieldsAlwaysVisibleFields');
            $this->fieldsAlwaysVisible = [];
            if (!empty($baAddFieldsAlwaysVisibleFields)) {
                $fieldList = GeneralUtility::trimExplode(',', $baAddFieldsAlwaysVisibleFields);
                foreach ($fieldList as $tmpField) {
                    $field = trim(strip_tags($tmpField));
                    if (!empty($field)) {
                        $this->fieldsAlwaysVisible[$field] = false;
                    }
                }
            }
            $baAddFieldsAlwaysRequiredFields = (string) TcaUtility::getExtConf('baAddFieldsAlwaysRequiredFields');
            if (!empty($baAddFieldsAlwaysRequiredFields)) {
                $fieldList = GeneralUtility::trimExplode(',', $baAddFieldsAlwaysRequiredFields);
                foreach ($fieldList as $tmpField) {
                    $field = trim(strip_tags($tmpField));
                    if (!empty($field)) {
                        $this->fieldsAlwaysVisible[$field] = true;
                    }
                }
            }
        }
    }

    /**
     * Returns whether the given field is marked as always included or excluded in the extension settings
     *
     * @param string $fieldName
     * @return int 0 if no setting for field exists; -1 if field is excluded; 1 if field is always included
     */
    private function getExtensionFieldSetting($fieldName)
    {
        if (null === $this->fieldsAlwaysExcluded) {
            $this->initAlwaysVisibleFields();
            $baAddFieldsAlwaysExcluded = (string) TcaUtility::getExtConf('baAddFieldsAlwaysExcluded');
            $this->fieldsAlwaysExcluded = [];
            if (!empty($baAddFieldsAlwaysExcluded)) {
                $fieldList = GeneralUtility::trimExplode(',', $baAddFieldsAlwaysExcluded);
                foreach ($fieldList as $tmpField) {
                    $field = trim(strip_tags($tmpField));
                    if (!empty($field)) {
                        $this->fieldsAlwaysExcluded[] = $field;
                    }
                }
            }
        }
        $result = 0;
        if (in_array($fieldName, $this->fieldsAlwaysExcluded, false)) {
            $result = self::FIELD_ALWAYS_DISABLED;
        } elseif (array_key_exists($fieldName, $this->fieldsAlwaysVisible)) {
            $result = self::FIELD_ALWAYS_ENABLED;
        }
        return $result;
    }

    /**
     * @return array
     */
    private function getAvailableEnableFields()
    {
        if (null === $this->availableTcaEnableFields) {
            $this->availableTcaEnableFields = [];
            $tcaFields = ['enable_required_fields', 'enable_optional_fields'];
            foreach ($tcaFields as $fieldName) {
                $fieldConfig = $GLOBALS['TCA']['tx_brainevents_domain_model_event']['columns'][$fieldName]['config'];
                foreach ($fieldConfig['items'] as $item) {
                    $this->availableTcaEnableFields[] = $item[1];
                }
            }
            $this->availableEnableFields = array_merge($this->availableEnableFields, $this->availableTcaEnableFields);
            $this->availableEnableFields = array_unique($this->availableEnableFields);
        }
        return $this->availableEnableFields;
    }

    /**
     * @return array
     */
    public function getAvailableEnableProperties()
    {
        $properties = [];
        $availableFields = $this->getAvailableEnableFields();
        foreach ($availableFields as $field) {
            $propertyName = $this->getPropertyName($field);
            $properties[] = $propertyName;
        }
        return $properties;
    }

    private function isExcludeField($field)
    {
        $enabledStatus = $this->getExtensionFieldSetting($field);
        return $enabledStatus !== self::FIELD_ALWAYS_DISABLED;
    }

    public function isFieldEnabledForEvent($field, Event $event)
    {
        $enabledStatus = $this->getExtensionFieldSetting($field);
        return $enabledStatus !== self::FIELD_ALWAYS_DISABLED
            && ($enabledStatus === self::FIELD_ALWAYS_ENABLED
                || $event->isOptionalFieldEnabled($field) || $event->isRequiredFieldEnabled($field));
    }

    /**
     * Checks if the given field is enabled for the registration
     *
     * @param string $field The snake case field name
     * @param Registration $registration The registration model
     * @return bool
     */
    public function isFieldEnabledForRegistration(string $field, Registration $registration)
    {
        $billingFields = $this->getEnabledBillingFields($registration, false);
        $propertyName = $this->getPropertyName($field);
        if (!empty($billingFields) && in_array($propertyName, $billingFields, false)) {
            return true;
        }
        $event = $registration->getEvent();
        return null !== $event && $this->isFieldEnabledForEvent($field, $event);
    }

    /**
     * Returns the list of available fields for the given event
     * @param Event $event
     * @return array|false|string[]
     */
    protected function getAvailableFieldsForEvent(Event $event)
    {
        $availableFields = $this->getAvailableEnableFields();
        $eventRequiredFields = $event->getEnableRequiredFields();
        $eventOptionalFields = $event->getEnableOptionalFields();
        if (!empty($eventRequiredFields)) {
            $availableFields = array_merge($availableFields, explode(',', $eventRequiredFields));
        }
        if (!empty($eventOptionalFields)) {
            $availableFields = array_merge($availableFields, explode(',', $eventOptionalFields));
        }
        return $availableFields;
    }

    public function getEnabledFields(Event $event, Registration $registration): array
    {
        $availableFields = $this->getAvailableFieldsForEvent($event);
        $fieldList = [];
        foreach ($availableFields as $field) {
            if ($this->isFieldEnabledForEvent($field, $event)) {
                $fieldList[] = $field;
                $propertyName = $this->getPropertyName($field);
                if ($propertyName !== $field) {
                    $fieldList[] = $propertyName;
                }
            }
        }
        $billingFields = $this->getEnabledBillingFields($registration, false);
        if (!empty($billingFields)) {
            $fieldList = array_unique(array_merge($fieldList, $billingFields));
        }
        return $fieldList;
    }

    public function getRequiredFields(Event $event, Registration $registration): array
    {
        $availableFields = $this->getAvailableFieldsForEvent($event);
        $fieldList = [];
        foreach ($availableFields as $field) {
            if ($this->isExcludeField($field) && $event->isRequiredFieldEnabled($field)) {
                $fieldList[] = $field;
                $propertyName = $this->getPropertyName($field);
                if ($propertyName !== $field) {
                    $fieldList[] = $propertyName;
                }
            }
        }
        $billingFields = $this->getEnabledBillingFields($registration, true);
        if (!empty($billingFields)) {
            $fieldList = array_unique(array_merge($fieldList, $billingFields));
        }
        $this->initAlwaysVisibleFields();
        foreach ($this->fieldsAlwaysVisible as $field => $isRequired) {
            if ($isRequired && ($propertyName = $this->getPropertyName($field))
                && !in_array($propertyName, $fieldList, false)) {
                $fieldList[] = $propertyName;
            }
        }
        return $fieldList;
    }

    /**
     * Returns the enabled fields for billing, depending on the registration data
     *
     * @param Registration $registration
     * @param bool $onlyRequired
     * @return array
     */
    private function getEnabledBillingFields(Registration $registration, bool $onlyRequired = false): array
    {
        $billingProperties = [];
        if (TcaUtility::getExtConf('enableChargeableEvents')) {
            $event = $registration->getEvent();
            /** @var OrderItemPriceCalculator $priceCalculator */
            $priceCalculator = GeneralUtility::makeInstance(OrderItemPriceCalculator::class);
            if (null !== $event && 0.0 < (float) $priceCalculator->getItemPrice($event)) {
                if ($onlyRequired) {
                    foreach ($this->billingPropertyRequiredMap as $billingProperty => $isRequired) {
                        if ($isRequired) {
                            $billingProperties[] = $billingProperty;
                        }
                    }
                } else {
                    $billingProperties = array_keys($this->billingPropertyRequiredMap);
                }
            }
        }
        return $billingProperties;
    }

    /**
     * Convert underscore field name to lower camel case property name
     * @param string $field
     * @return string
     */
    protected function getPropertyName($field)
    {
        return lcfirst(trim(str_replace(' ', '', ucwords(str_replace('_', ' ', $field)))));
    }
}