<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

/**
 * PhpOffice\PhpSpreadsheet export service
 */
class SpreadsheetExportService extends AbstractExportService
{

    private $phpSpreadsheet;

    public function __construct()
    {
        $this->phpSpreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
    }

    public function create($sheetName, $rows): void
    {
        ini_set('max_execution_time', 900);
        $mem = trim(ini_get("memory_limit"), 'M');
        if ($mem < 512) {
            ini_set('memory_limit', '512M');
        }
        $sheet = $this->phpSpreadsheet->setActiveSheetIndex(0);
        $sheet->setTitle($sheetName);
        $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        $sheet->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
        $sheet->getPageSetup()->setFitToWidth(1);

        $style = $this->phpSpreadsheet->getDefaultStyle();
        $font = $style->getFont();
        $font->setName(self::FONT_NAME);
        $font->setSize(self::FONT_SIZE);
        \PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder(new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder());

        $fields = $this->fields;
        $columnWidths = [];
        $maxColWidth = self::MAX_COL_WIDTH;

        $rowNr = 1;
        $setBold = true;

        foreach ($fields as $offset => $field) {
            $heading = $this->translate($field);
            $columnIndex = $offset + 1;
            $cell = $sheet->getCellByColumnAndRow($columnIndex, $rowNr);
            $cellId = $cell->getCoordinate();
            $cell->setValue($heading);
            $this->setCellFont($cellId, 10, $setBold, 'center', 'center');
            $this->setCellBorder($cellId, 'left', 'CCCCCC');
            $this->enableTextWrap($cellId);
            $columnWidths[$cell->getColumn()] = max(9, round(strlen($heading) * 1.25));
        }

        /** @var \KDN\KdnEvents\Domain\Model\Registration $row */
        foreach ($rows as $row) {
            ++$rowNr;
            foreach ($fields as $offset => $field) {
                $fieldVal = $this->getRowFieldVal($row, $field);
                $columnIndex = $offset + 1;
                $cell = $sheet->getCellByColumnAndRow($columnIndex, $rowNr);
                $cellId = $cell->getCoordinate();
                $cell->setValue($fieldVal);
                $this->setCellFont($cellId, 10, false, 'left', 'top');
                $this->enableTextWrap($cellId);
                $cellColumn = $cell->getColumn();
                $columnWidths[$cellColumn] = max(
                    $columnWidths[$cellColumn],
                    min($maxColWidth, round(strlen($fieldVal) * 1.25))
                );
            }
        }

        foreach ($columnWidths as $colId => $colWidth) {
            $this->setColWidth($colId, $colWidth);
        }

        // Set date format for crdate column
        $dateFormat = 'dd.mm.yyyy hh:mm';
        $this->setNumberFormat('I1:I' . $rowNr, $dateFormat);
    }

    /**
     * Set font for given table cell
     *
     * @param string $cellNr
     * @param int $fontSize
     * @param bool $setBold
     * @param string $hAlign
     * @param string $vAlign
     * @param bool $underline
     */
    private function setCellFont(
        $cellNr,
        $fontSize,
        $setBold = false,
        $hAlign = 'left',
        $vAlign = 'top',
        $underline = false
    ): void
    {
        $style = $this->phpSpreadsheet->getActiveSheet()->getStyle($cellNr);
        $style->getFont()->setSize($fontSize)->setBold($setBold);
        $style->getAlignment()->setHorizontal($hAlign);
        $style->getAlignment()->setVertical($vAlign);
        if ($underline) {
            $style->getFont()->setUnderline(\PhpOffice\PhpSpreadsheet\Style\Font::UNDERLINE_SINGLE);
        }
    }

    /**
     * Add border for given table cell
     *
     * @param string $cellNr
     * @param string $borderType top, left, bottom, right, outline
     * @param string|null $color
     * @param string|null $lineStyle
     */
    private function setCellBorder($cellNr, $borderType = 'outline', $color = null, $lineStyle = null): void
    {
        if ($lineStyle === null) {
            $lineStyle = \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_HAIR;
        }
        if ($color === null) {
            $color = '000000';
        }
        $borderStyle = [
            'borders' => [
                $borderType => [
                    'style' => $lineStyle,
                    'color' => ['argb' => 'FF' . $color],
                ],
            ],
        ];
        $this->getActiveSheet()->getStyle($cellNr)->applyFromArray($borderStyle);
    }

    private function getActiveSheet(): \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
    {
        return $this->phpSpreadsheet->getActiveSheet();
    }

    /**
     * Set number format for given cell
     *
     * @param string $cellNr
     * @param string $format
     */
    private function setNumberFormat($cellNr, $format = \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1): void
    {
        $style = $this->getActiveSheet()->getStyle($cellNr);
        $style->getNumberFormat()->setFormatCode($format);
    }

    /**
     * Set column width for given column
     *
     * @param string $colName
     * @param float $colWidth
     */
    private function setColWidth($colName, $colWidth): void
    {
        $this->getActiveSheet()->getColumnDimension($colName)->setWidth($colWidth);
    }

    /**
     * Enable text wrap for given cell
     *
     * @param string $cellNr
     */
    private function enableTextWrap($cellNr): void
    {
        $this->getActiveSheet()->getStyle($cellNr)->getAlignment()->setWrapText(true);
    }

    /**
     * @param string $fileBaseName File name without file type ending
     * @param string $fileType The file type
     */
    public function sendFile($fileBaseName, $fileType): void
    {
        $fileName = $this->filterFileName(str_replace('.', '_', $fileBaseName)) . '.' . $fileType;
        $writerType = 'Xlsx';
        if ($fileType === 'xlsx') {
            // Redirect output to a client's web browser (Excel2007)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        } elseif ($fileType === 'pdf') {
            header('Content-Type: application/pdf');
            $writerType = 'Mpdf';
        }
        header('Content-Disposition: attachment;filename="' . basename($fileName) . '"');
        header('Cache-Control: max-age=0');
        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($this->phpSpreadsheet, $writerType);
        $objWriter->save('php://output');
        exit;
    }
}
