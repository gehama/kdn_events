<?php

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Utility\SessionUtility;
use KDN\KdnEvents\Utility\TaxCalculator;
use KDN\KdnEvents\Utility\TcaUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class OrderItemPriceCalculator implements SingletonInterface
{
    private const DEFAULT_PRICE_TYPE_USER_PROPERTY = 'usergroup';
    public const USER_GROUP_PRICE_TYPE_PROPERTY = 'tx_kdnevents_price_type';

    protected $userTypePriceMapping = [];

    /**
     * Returns true if the article prices are visible; false otherwise
     * @return bool
     */
    public static function pricesAreVisible(): bool
    {
        return (bool)TcaUtility::getExtConf('enableChargeableEvents');
    }

    /**
     * Returns the item price depending on the user group(s)
     * @param Event $event
     * @param bool $checkVisibility Check if prices are enabled for current user
     * @return array
     */
    public function getItemPriceWithTaxes(Event $event, $checkVisibility = true): array
    {
        $performanceDate = $event->getEventStart() ?? date_create('now');
        $price = $this->getItemPrice($event, $checkVisibility);
        if (null !== $price) {
            $calcTstamp = $performanceDate->getTimestamp();
            $costData = [
                'priceGross' => $price,
                'taxAmount' => TaxCalculator::calculateTaxes($price, $event->getTaxType(), $calcTstamp),
                'priceNet' => TaxCalculator::calculateAmountNet($price, $event->getTaxType(), $calcTstamp),
                'taxRate' => TaxCalculator::getTaxValue($event->getTaxType(), $calcTstamp),
                'performanceDate' => $performanceDate,
                'isFreeOfCharge' => $price === 0.0,
                'isChargeable' => $price > 0.0,
                'enabled' => true,
            ];
        } else {
            $costData = [
                'priceGross' => false,
                'taxAmount' => false,
                'priceNet' => false,
                'taxRate' => false,
                'performanceDate' => $performanceDate,
                'isFreeOfCharge' => true,
                'isChargeable' => false,
                'enabled' => false,
            ];
        }
        return $costData;
    }

    /**
     * Returns the item price depending on the user group(s)
     * @param Event $event
     * @param bool $checkVisibility Check if prices are enabled for current user
     * @return float|null
     */
    public function getItemPrice(Event $event, $checkVisibility = true): ?float
    {
        if ($checkVisibility && !self::pricesAreVisible()) {
            return null;
        }
        $price = (float)$event->getPrice();
        $userTypeField = TcaUtility::getExtConf('userTypeField');
        if (empty($userTypeField)) {
            $userTypeField = self::DEFAULT_PRICE_TYPE_USER_PROPERTY;
        }
        $userPropertyValue = SessionUtility::getUserProperty($userTypeField);
        $item = [
            BasketInterface::ITEM_PRICE_FIELD_DEFAULT => (float) $event->getPrice(),
        ];
        $prepareItem = [
            BasketInterface::ITEM_PRICE_FIELD_GROUP_2 => (float) $event->getPriceMemberType2(),
            BasketInterface::ITEM_PRICE_FIELD_GROUP_3 => (float) $event->getPriceMemberType3(),
            BasketInterface::ITEM_PRICE_FIELD_GROUP_4 => (float) $event->getPriceMemberType4(),
            BasketInterface::ITEM_PRICE_FIELD_GROUP_5 => (float) $event->getPriceMemberType5(),
        ];
        // Support disabling the price fields
        $enabledCustomPriceFields = GeneralUtility::trimExplode(',', TcaUtility::getExtConf('enabledCustomPriceFields'), true);
        foreach ($enabledCustomPriceFields as $customPriceField) {
            if (array_key_exists($customPriceField, $prepareItem)) {
                $item[$customPriceField] = $prepareItem[$customPriceField];
            }
        }
        if (!empty($userPropertyValue)) {
            $isDefaultPrice = true;
            $valueList = GeneralUtility::trimExplode(',', $userPropertyValue);
            if ($userTypeField === self::DEFAULT_PRICE_TYPE_USER_PROPERTY) {
                $groupPriceMap = $this->cacheOrderDetails($valueList);
                foreach ($groupPriceMap as $field) {
                    if (!empty($field) && !empty($item[$field]) && ($isDefaultPrice || (float)$item[$field] < $price)) {
                        $price = $item[$field];
                        $isDefaultPrice = false;
                    }
                }
            } else {
                // All other fields must match the numbering of the price fields
                foreach ($valueList as $key) {
                    $property = 'price_member_type_' . $key;
                    if (!empty($item[$property]) && ($isDefaultPrice || (float)$item[$property] < $price)) {
                        $price = $item[$property];
                        $isDefaultPrice = false;
                    }
                }
            }
        } else {
            $price = max($item);
        }
        return $price;
    }

    /**
     * @param int[]|array $userGroupList
     * @return array
     */
    public function cacheOrderDetails($userGroupList): array
    {
        $groupPriceMap = [];
        $loadGroups = [];
        foreach ($userGroupList as $groupId) {
            if (array_key_exists($groupId, $this->userTypePriceMapping)) {
                $groupPriceMap[$groupId] = $this->userTypePriceMapping[$groupId];
            } else {
                $loadGroups[] = $groupId;
            }
        }
        if (!empty($loadGroups)) {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('fe_groups');

            $selectFields = ['g.uid', 'g.' . self::USER_GROUP_PRICE_TYPE_PROPERTY];
            $queryBuilder = $queryBuilder
                ->select(...$selectFields)
                ->from('fe_groups', 'g')
                ->where(
                    $queryBuilder->expr()->in(
                        'g.uid',
                        $loadGroups
                    )
                );
            $statement = $queryBuilder->execute();
            while ($row = $statement->fetch()) {
                $groupId = $row['uid'];
                $mapChoiceToField = [
                    0 => '',
                    2 => BasketInterface::ITEM_PRICE_FIELD_GROUP_2,
                    3 => BasketInterface::ITEM_PRICE_FIELD_GROUP_3,
                    4 => BasketInterface::ITEM_PRICE_FIELD_GROUP_4,
                    5 => BasketInterface::ITEM_PRICE_FIELD_GROUP_5,
                ];
                $mapToField = $mapChoiceToField[$row[self::USER_GROUP_PRICE_TYPE_PROPERTY]] ?? '';
                $this->userTypePriceMapping[$groupId] = $mapToField;
                $groupPriceMap[$groupId] = $this->userTypePriceMapping[$groupId];
            }
        }
        return $groupPriceMap;
    }
}
