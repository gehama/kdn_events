<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

use KDN\KdnEvents\Domain\Model\DemandInterface;
use KDN\KdnEvents\Domain\Model\Dto\AbstractDemand;
use KDN\KdnEvents\Domain\Model\Dto\EventDemand;
use KDN\KdnEvents\Domain\Model\Status;

/**
 * Class demand service
 */
class DemandService
{

    /**
     * Status repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\StatusRepository
     */
    protected $statusRepository;

    public function injectStatusRepository(\KDN\KdnEvents\Domain\Repository\StatusRepository $statusRepository)
    {
        $this->statusRepository = $statusRepository;
    }

    /**
     *
     * @param DemandInterface|AbstractDemand|null $demand
     * @param array $settings
     * @param string $reqOrder
     *
     * @return DemandInterface|AbstractDemand
     * @throws \ReflectionException
     */
    public function initDemandSettings(?DemandInterface $demand, array $settings, $reqOrder = null): DemandInterface
    {
        if (null === $demand) {
            $demand = new EventDemand();
            $demand->setStatus(null);
        } else {
            $demand->setSubmitted(true);
        }
        $visibleStatusChoices = [];
        //$statusChoices = [];
        $statusList = $this->statusRepository->findAll();
        foreach ($statusList as $status) {
            /** @var Status $status */
            //$statusChoices[$status->getUid()] = $status->getName();
            if ($status->isEventVisible()) {
                $visibleStatusChoices[] = $status->getUid();
            }
        }
        $demand->setRespectStoragePid(true);
        if ($demand instanceof EventDemand) {
            $demand->setPublishedState(EventDemand::PUBLISHED_ONLY);
            $demand->setIgnoreHideInListFlag(false);
            $demand->setMinStartTimestamp(time());
            $demand->setOrganizers($settings['list']['organizers'] ?? []);
            $demand->setTopEventsRestriction((int)$settings['topEventsRestriction']);
            $demand->setEventTypeRestriction($settings['eventTypeRestriction'] ?? []);
            if (!empty($reqOrder)) {
                $orderParts = explode(':', trim(strip_tags($reqOrder)));
                if (!empty($orderParts[0]) && in_array($orderParts[0], $demand->getValidSortProperties(), false)) {
                    $order = $orderParts[0] . ' ' . ((int)$orderParts[1] === 1 ? 'DESC' : 'ASC');
                    $demand->setOrder($order);
                }
            } else {
                $demand->setOrder('eventStart ASC');
            }
        } else {
            $demand->setOrder('crdate ASC');
        }
        $demand->setAllowedStatusList($visibleStatusChoices);
        $demand->setCategories($settings['list']['categories'] ?? []);
        return $demand;
    }
}
