<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Service;

/**
 * Abstract away sending emails into a service:
 */
class EncryptionService
{
    private $encryptionType = 'default';

    private $encryptionKey = 'kdnEvents!20190608';

    public function __construct()
    {
        if (function_exists('openssl_encrypt')) {
            $this->encryptionType = 'openssl';
        }
    }


    /**
     * Encrypt given string with given $key
     *
     * @param string $string The key used for encryption
     * @return string The encrypted string
     * */
    public function encrypt($string)
    {
        $key = $this->encryptionKey;
        $i = $this->encryptionType;
        if ($i === 'openssl') {
            $encryptedStr = '01:' . $this->encryptOpenSsl($key, $string);
        } else {
            $encryptedStr = $this->encryptDefault($key, $string);
        }
        return $encryptedStr;
    }

    /**
     * Decryption of string with given $key
     *
     * @param string $encStr Encrypted string
     * @return string The decrypted string
     */
    public function decrypt($encStr)
    {
        $key = $this->encryptionKey;
        // Decrypt function depends on data provided
        $encryptionType = 'default';
        if (strpos($encStr, '01:') === 0) {
            $encStr = substr($encStr, 3);
            $encryptionType = 'openssl';
        }
        if ($encryptionType === 'openssl') {
            $decryptedStr = $this->decryptOpenSsl($key, $encStr);
        } else {
            $decryptedStr = $this->decryptDefault($key, $encStr);
        }
        return $decryptedStr;
    }

    /**
     * OpenSSL encryption for given string with given $key
     *
     * @param string $key The key used for encryption
     * @param string $string The key used for encryption
     * @return string The encrypted string
     */
    private function encryptOpenSsl($key, $string)
    {
        $key = hash('sha256', substr($key, 0, 16));
        /** @noinspection PhpComposerExtensionStubsInspection */
        $iv = openssl_random_pseudo_bytes(16, $cryptoStrong);
        if (false === $iv || !$cryptoStrong) {
            $iv = substr(hash('sha256', substr($key, 16)), 0, 16);
        }
        $ciphertext = openssl_encrypt($string, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $iv);
        return base64_encode($ciphertext);
    }

    /**
     * OpenSSL decryption of given string with given $key
     *
     * @param string $key The key used for decryption
     * @param string $encStr Encrypted string
     * @return string The decrypted string
     */
    private function decryptOpenSsl($key, $encStr)
    {
        $key = hash('sha256', substr($key, 0, 16));
        /** @noinspection PhpComposerExtensionStubsInspection */
        $iv = openssl_random_pseudo_bytes(16, $cryptoStrong);
        if (false === $iv || !$cryptoStrong) {
            $iv = substr(hash('sha256', substr($key, 16)), 0, 16);
        }
        return openssl_decrypt(base64_decode($encStr), 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $iv);
    }

    /**
     * Fallback encryption for given string with given $key
     *
     * @param string $key The key used for encryption
     * @param string $string The key used for encryption
     * @return string The encrypted string
     */
    private function encryptDefault($key, $string)
    {
        $out = '';
        $cryptLen = strlen($key);
        for ($a = 0, $n = strlen($string); $a < $n; $a++) {
            $xorVal = ord($key{($a % $cryptLen)});
            $out.= chr(ord($string{$a}) ^ $xorVal);
        }

        $str = base64_encode($out);
        $strHash = substr(md5($key . ':' . $str), 0, 10);
        return $strHash . ':' . $str;
    }

    /**
     * Fallback decryption of given string with given $key
     *
     * @param string $key The key used for decryption
     * @param string $encStr Encrypted string
     * @return string The decrypted string
     */
    private function decryptDefault($key, $encStr)
    {
        $dcrStr = '';
        $parts = explode(':', $encStr);
        $hash = $parts[0];
        $encData = $parts[1] ?? '';

        $checkHash = substr(md5($key . ':' . $encData), 0, 10);
        if ($hash === $checkHash) {
            $dcrStr = base64_decode($encData);
            $strLen = strlen($dcrStr);
            $cryptLen = strlen($key);
            if ($cryptLen > 0) {
                $out = '';
                for ($a = 0; $a < $strLen; $a++) {
                    $xorVal = ord($key{($a % $cryptLen)});
                    $out .= chr(ord($dcrStr{$a}) ^ $xorVal);
                }
                $dcrStr = $out;
            }
        }
        return $dcrStr;
    }
}