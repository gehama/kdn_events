<?php

namespace KDN\KdnEvents\KeSearchHooks;

use KDN\KdnEvents\Utility\DbUtility;
use PDO;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Database\Query\Restriction\HiddenRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;


class CustomIndexer
{
    // Set a key for your indexer configuration.
    // Add this in Configuration/TCA/Overrides/tx_kesearch_indexerconfig.php, too!
    protected $indexerConfigurationKey = 'eventindexer';

    /**
     * @var array
     */
    private $locations = [];

    /**
     * Adds the custom indexer to the TCA of indexer configurations, so that
     * it's selectable in the backend as an indexer type when you create a
     * new indexer configuration.
     *
     * @param array $params
     * @param object $pObj
     */
    public function registerIndexerConfiguration(&$params, $pObj)
    {
        // Set a name and an icon for your indexer.
        $customIndexer = array(
            '[CUSTOM] Event-Indexer (ext:kdn_events)',
            $this->indexerConfigurationKey,
            'EXT:kdn_events/ext_icon.gif'
        );
        $params['items'][] = $customIndexer;
    }
    /**
     * Custom indexer for ke_search.
     *
     * @param   array $indexerConfig Configuration from TYPO3 Backend.
     * @param   \Tpwd\KeSearch\Indexer\IndexerRunner $indexerObject Reference to indexer class.
     * @return  string Message containing indexed elements.
     * @author  Christian Buelter <christian.buelter@pluswerk.ag>
     */
    public function customIndexer(&$indexerConfig, $indexerObject)
    {
        $content = '';
        if ($indexerConfig['type'] === $this->indexerConfigurationKey) {
            // Get all the entries to index.
            // Don't index hidden or deleted elements, but get the elements
            // with frontend user group access restrictions or time (start / stop)
            // restrictions in order to copy those restrictions to the index.

            $fields = ['*']; // Array of table fields.
            $table = 'tx_kdnevents_domain_model_event';

            // Doctrine DBAL using Connection Pool.
            $connection = DbUtility::getConnectionForTable($table);
            $queryBuilder = $connection->createQueryBuilder();

            /** @var DeletedRestriction $deletedRestriction */
            $deletedRestriction = GeneralUtility::makeInstance(DeletedRestriction::class);
            /** @var HiddenRestriction $hiddenRestriction */
            $hiddenRestriction = GeneralUtility::makeInstance(HiddenRestriction::class);
            $queryBuilder->getRestrictions()->removeAll()
                ->add($deletedRestriction)
                ->add($hiddenRestriction);
            $queryBuilder
                ->select(...$fields)
                ->from($table)
                ->where($queryBuilder->expr()->eq('published', 1))
                ->andWhere($queryBuilder->expr()->gte('event_start', time()));

            if (!empty($indexerConfig['sysfolder'])) {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->in(
                        'pid',
                        $queryBuilder->createNamedParameter($indexerConfig['sysfolder'], PDO::PARAM_INT)
                    )
                );
            }
            $statement = $queryBuilder->execute();
                // Loop through the records and write them to the index.
            $counter = 0;

            while ($record = $statement->fetch()) {
                // Compile the information, which should go into the index.
                // The field names depend on the table you want to index!
                $title    = trim(strip_tags($record['title']));
                $tutor = trim(strip_tags($record['tutor']));
                $content  = trim(strip_tags($record['description']));
                $timespan = '';
                if (!empty($record['event_start'])) {
                    $timespan = date('d.m.Y', $record['event_start']);
                    if ($record['event_end'] > 0 && $timespan !== $endDay = date('d.m.Y', $record['event_end'])) {
                        $timespan .= ' ' . $endDay;
                    }
                }
                $location = $this->getLocationName($record['location']);
                $fullContent = trim($timespan . "\n" . $title . "\n" . $tutor . "\n" . $location . "\n" . $content);
                $abstract = '';
                $eventStart = empty($record['event_start']) ? $record['registration_start'] : $record['event_start'];
                // Link to detail view
                $params = '&tx_kdnevents_items[event]=' . $record['uid']
                    . '&tx_kdnevents_items[controller]=Event&tx_kdnevents_items[action]=show';
                // Tags
                // If you use Sphinx, use "_" instead of "#" (configurable in the extension manager).
                $tags = '#kdnevents#,#events_'.date('Y', $eventStart).'#';
                // Additional information
                $additionalFields = array(
                    'sortdate' => empty($eventStart) ? $record['crdate'] : $eventStart,
                    'orig_uid' => $record['uid'],
                    'orig_pid' => $record['pid'],
                );
                $feGroup = !empty($record['fe_group']) ? $record['fe_group'] : '';
                // Add something to the title, just to identify the entries
                // in the frontend.
                $title = '[Events] ' . $title;
                // ... and store the information in the index
                $indexerObject->storeInIndex(
                    $indexerConfig['storagepid'],   // storage PID
                    $title,                         // record title
                    $this->indexerConfigurationKey, // content type
                    $indexerConfig['targetpid'],    // target PID: where is the single view?
                    $fullContent,                   // indexed content, includes the title (linebreak after title)
                    $tags,                          // tags for faceted search
                    $params,                        // typolink params for singleview
                    $abstract,                      // abstract; shown in result list if not empty
                    $record['sys_language_uid'],    // language uid
                    $record['starttime'],           // starttime
                    $record['endtime'],             // endtime
                    $feGroup,            // fe_group
                    false,                          // debug only?
                    $additionalFields               // additionalFields
                );

                $counter++;
            }
            $content =
                '<p><b>Custom Indexer "'
                . $indexerConfig['title'] . '": ' . $counter
                . ' Elements have been indexed.</b></p>';
        }
        return $content;
    }

    private function getLocationName($locationId)
    {
        $name = '';
        if (!empty($locationId)) {
            if (!isset($this->locations[$locationId])) {
                $row = $this->fetchRowByFields('tx_kdnevents_domain_model_location', ['uid' => $locationId]);
                if (!empty($row)) {
                    if (!empty($row['title'])) {
                        $name = $row['title'];
                    }
                    if (!empty($row['street'])) {
                        if (!empty($name)) {
                            $name .= ' ';
                        }
                        $name .= $row['street'];
                    }
                    $zipTown = trim($row['zip'] . ' ' . $row['name']);
                    if (!empty($zipTown)) {
                        if (!empty($name)) {
                            $name .= ' ';
                        }
                        $name .= $zipTown;
                    }
                }
                $this->locations[$locationId] = $name;
            }
            $name = $this->locations[$locationId];
        }
        return $name;
    }

    /**
     * Load record from database
     *
     * @param string $table
     * @param array $identifiers
     * @param array $orderBy
     * @param string[] $extraConditions
     * @return array Table row array; null if empty
     */
    protected function fetchRowByFields($table, $identifiers, $orderBy = [], $extraConditions = [])
    {
        $queryBuilder = DbUtility::getConnectionForTable($table)->createQueryBuilder();
        $queryBuilder
            ->select('*')
            ->from($table)
            ->setMaxResults(1);
        foreach ($identifiers as $field => $value) {
            $type = $field == 'uid' && is_int($value) ? PDO::PARAM_INT : PDO::PARAM_STR;
            $expr = $queryBuilder->expr()->eq($field, $queryBuilder->createNamedParameter($value, $type));
            $queryBuilder->andWhere($expr);
        }
        foreach ($extraConditions as $condition) {
            $queryBuilder->andWhere($condition);
        }
        foreach ($orderBy as $field => $order) {
            $queryBuilder->addOrderBy($field, $order);
        }
        $row = $queryBuilder->execute()->fetch();
        if (empty($row)) {
            return null;
        }
        return $row;
    }
}