<?php
declare(strict_types=1);

namespace KDN\KdnEvents\Seo;

/**
 * This file is part of the "kdn_events" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 */

use KDN\KdnEvents\Domain\Model\Event;
use TYPO3\CMS\Core\PageTitle\AbstractPageTitleProvider;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Generate page title based on properties of the news model
 */
class EventTitleProvider extends AbstractPageTitleProvider
{
    private const DEFAULT_PROPERTIES = 'title';
    private const DEFAULT_GLUE = '" "';

    /**
     * @param Event $event
     * @param array $configuration
     */
    public function setTitleByEvent(Event $event, array $configuration = []): void
    {
        $title = '';
        $fields = GeneralUtility::trimExplode(',', $configuration['properties'] ?? self::DEFAULT_PROPERTIES, true);

        foreach ($fields as $field) {
            $getter = 'get' . ucfirst($field);
            $value = $event->$getter();
            if ($value) {
                $title = $value;
                break;
            }
        }
        if ($title) {
            $this->title = $title;
        }
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }
}
