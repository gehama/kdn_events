<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\ViewHelpers\Format;

use KDN\KdnEvents\Utility\TimeFormatUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\View\Exception;

/**
 * ViewHelper to render the file information
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class TimespanViewHelper extends AbstractViewHelper
{

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('dateStart', 'mixed', 'either a DateTime object or a string that is accepted by DateTime constructor');
        $this->registerArgument('dateEnd', 'mixed', 'either a DateTime object or a string that is accepted by DateTime constructor');
        $this->registerArgument('format', 'string', 'Format for start date (without time part!)');
        $this->registerArgument('showTime', 'boolean', 'Enable time display', false, false);
    }

    /**
     * Render the supplied DateTime object as a formatted date.
     *
     * @return string Formatted date timespan
     * @throws Exception
     */
    public function render()
    {
        $dateStart = $this->arguments['dateStart'];
        $dateEnd = $this->arguments['dateEnd'];
        $format = (string)$this->arguments['format'];
        $showTime = (bool)$this->arguments['showTime'];
        return (new TimeFormatUtility())->render($dateStart, $dateEnd, $format, $showTime);
    }
}
