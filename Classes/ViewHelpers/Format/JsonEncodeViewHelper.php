<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\ViewHelpers\Format;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * ViewHelper to render the file information
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class JsonEncodeViewHelper extends AbstractViewHelper
{

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('value', 'string', 'The value');
        $this->registerArgument('maxCharacters', 'int', 'Max length of string');
    }

    /**
     * Render the supplied DateTime object as a formatted date.
     *
     * @return string Formatted JSON string
     */
    public function render()
    {

        $value = (string)$this->arguments['value'];
        $maxCharacters = (int)$this->arguments['maxCharacters'];
        if (is_string($value)) {
            $value = trim(strip_tags($value));
            if ($maxCharacters > 0) {
                $append = '...';
                $respectWordBoundaries = 1;
                /** @var ContentObjectRenderer $contentObject */
                $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
                $value = $contentObject->crop($value, $maxCharacters . '|' . $append . '|' . $respectWordBoundaries);
            }
        }
        return json_encode($value);
    }
}
