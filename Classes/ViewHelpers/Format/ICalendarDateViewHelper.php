<?php
namespace KDN\KdnEvents\ViewHelpers\Format;


use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class ICalendarDateViewHelper extends AbstractViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('name', 'string', 'The property name', true);
        $this->registerArgument('date', 'datetime', 'The DateTime object', false);
    }

    /**
     * Formats the given date according to rfc5545
     *
     * @see http://tools.ietf.org/html/rfc5545#section-3.3.5
     * @return string
     */
    public function render()
    {
        $name = $this->arguments['name'];
        $date = $this->arguments['date'];
        if ($date === null) {
            $date = $this->renderChildren();
        }
        if ($date instanceof \DateTime) {
            $cDate = clone $date;
            $cDate->setTimezone(new \DateTimeZone('UTC'));
            if ($name === 'DTSTAMP') {
                return $name . ':' . $cDate->format('Ymd\THis\Z');
            }
            return $name . $this->formatDateForWithZone($cDate->getTimestamp(), 'Europe/Berlin');
        }

        return '';
    }

    /**
     * @param int $dateAsUnixTimeStamp
     * @param string $timeZone
     *
     * @return string
     */
    private function formatDateForWithZone($dateAsUnixTimeStamp, $timeZone)
    {
        return ';TZID=' . $timeZone . ':' . strftime('%Y%m%dT%H%M%S', $dateAsUnixTimeStamp);
    }

}
