<?php
namespace KDN\KdnEvents\ViewHelpers\Format;


use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class ICalendarDescriptionViewHelper extends AbstractViewHelper
{
    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('description', 'string', 'The description', false);
        $this->registerArgument('allowHtml', 'boolean', 'Allow html (used for X-ALT-DESC)', false, false);
    }

    /**
     * Formats the given description according to RFC 2445
     *
     * @return string
     */
    public function render()
    {
        $description = $this->arguments['description'];
        $allowHtml = (bool) $this->arguments['allowHtml'];
        if ($description === null) {
            $description = $this->renderChildren();
        }
        $blockTags = ['<p', '<h', '<div', '<de', '<dd', '<dt'];
        $tmpDescription = $description;
        $tmpDescription = preg_replace('/<li[\s\w"=]*>\s+(<p>)?/', '<li>', $tmpDescription);
        $tmpDescription = preg_replace('/<\/p>\s+<\/li>/', '</li>', $tmpDescription);
        $tmpDescription = str_replace('<li', "\r\n".'* <li', $tmpDescription);
        foreach ($blockTags as $blockTag) {
            $tmpDescription = str_replace($blockTag, "\r\n".$blockTag, $tmpDescription);
        }
        $tmpDescription = strip_tags($tmpDescription);
        $tmpDescription = str_replace(['&nbsp;', '&amp;', "\t"], [' ', '&', ' '], $tmpDescription);
        $tmpDescription = html_entity_decode($tmpDescription);
        // Replace carriage return
        $tmpDescription = str_replace(["\r\n", chr(13)], '\n', $tmpDescription);
        $tmpDescription = str_replace(['\n\n\n\n\n\n', '\n\n\n\n'], '\n\n', $tmpDescription);
        $lines = explode("\n", $tmpDescription);
        $filterLines = [];
        $lastLineWasEmpty = false;
        foreach ($lines as $line) {
            $tLine = trim($line);
            if (!empty($tLine) || !$lastLineWasEmpty) {
                $filterLines[] = $tLine;
            }
            $lastLineWasEmpty = empty($tLine);
        }
        $tmpDescription = implode("\n", $filterLines);
            // Strip new lines
        $tmpDescription = str_replace(chr(10), '', $tmpDescription);
        // Glue everything together, so every line is max 75 chars
        if (mb_strlen($tmpDescription) > 75) {
            $newDescription = mb_substr($tmpDescription, 0, 63) . chr(10);
            $tmpDescription = mb_substr($tmpDescription, 63);
            $arrPieces = mb_str_split($tmpDescription, 74);
            foreach ($arrPieces as &$value) {
                $value = ' ' . $value;
            }
            $newDescription .= implode(chr(10), $arrPieces);
        } else {
            $newDescription = $tmpDescription;
        }

        return $newDescription;
    }
}
