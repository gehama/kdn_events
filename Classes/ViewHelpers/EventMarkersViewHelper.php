<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\ViewHelpers;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Service\MarkerService;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * ViewHelper to render the file information
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class EventMarkersViewHelper extends AbstractViewHelper
{
    /**
     * @var MarkerService
     */
    protected $markerService;

    /**
     * Initialize arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('event', Event::class, 'The event instance');
    }

    /**
     * @param MarkerService $markerService
     */
    public function injectMarkerService(MarkerService $markerService)
    {
        $this->markerService = $markerService;
    }


    /**
     * Render the supplied DateTime object as a formatted date.
     *
     * @return string Text with event markers replaced by event data
     */
    public function render()
    {
        /** @var Event $event */
        $event = $this->arguments['event'];
        $markerService = $this->markerService;
        $markerService->setMode('FE');
        $eventMarkers = $markerService->getEventMarkers($event);
        $search = array_keys($eventMarkers);
        $text = $this->renderChildren();
        return str_replace($search, $eventMarkers, $text);
    }
}
