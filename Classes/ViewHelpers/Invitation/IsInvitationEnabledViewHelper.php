<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\ViewHelpers\Invitation;


use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Invitation\InvitationManager;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

/*
 * Class IsInvitationEnabledViewHelper
 * Determines if invitation link is displayed
 */

class IsInvitationEnabledViewHelper extends AbstractConditionViewHelper
{
    /**
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('event', 'object', 'Event', true);
        $this->registerArgument('settings', 'array', 'Settings', true);
    }

    /**
     * This method decides if the condition is TRUE or FALSE. It can be overridden in extending
     * view helpers to adjust functionality.
     *
     * @param array $arguments ViewHelper arguments to evaluate the condition for this ViewHelper, allows for flexibility in overriding this method.
     * @return bool
     */
    protected static function evaluateCondition($arguments = null)
    {
        $settings = $arguments['settings'];
        /** @var array $settings */
        $event = $arguments['event'];
        /** @var Event $event */
        if ((int)$settings['single']['invitationPid'] > 0) {
            return InvitationManager::staticIsInvitationEnabledForEvent($event);
        }
        return false;
    }

}
