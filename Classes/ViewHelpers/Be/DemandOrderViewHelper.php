<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\ViewHelpers\Be;

use KDN\KdnEvents\Domain\Model\DemandInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * ViewHelper to render the file information
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class DemandOrderViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'a';

    /**
     * Arguments initialization
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerUniversalTagAttributes();
        $this->registerTagAttribute('name', 'string', 'Specifies the name of an anchor');
        $this->registerTagAttribute('rel', 'string', 'Specifies the relationship between the current document and the linked document');
        $this->registerTagAttribute('rev', 'string', 'Specifies the relationship between the linked document and the current document');
        $this->registerTagAttribute('target', 'string', 'Specifies where to open the linked document');
        $this->registerArgument('demand', DemandInterface::class, 'The registration demand');
        $this->registerArgument('field', 'string', 'The sort order field');
        $this->registerArgument('action', 'string', 'The target action', false, 'list');
    }

    /**
     * Get order parameters depending on current demand and field
     *
     * @return string Rendered link
     */
    public function render()
    {
        /** @var DemandInterface $demand */
        $demand = $this->arguments['demand'];
        $field = $this->arguments['field'];
        $action = $this->arguments['action'];
        $order = $demand->getOrder();
        $orderKey = 0;
        if (!empty($order)) {
            $orderList = GeneralUtility::trimExplode(',', $order, TRUE);
            foreach ($orderList as $orderField) {
                if (strpos($orderField, $field) !== false) {
                    // If list is currently ordered by this field, reverse the sort order (when user clicks on the link)
                    $orderKey = strpos($orderField, 'DESC') !== false ? 0 : 1;
                    break;
                }
            }
        }

        $arguments = array('order' => $field . ':' . $orderKey);
        $uriBuilder = $this->renderingContext->getControllerContext()->getUriBuilder();
        $uri = $uriBuilder
            ->reset()
            //->setTargetPageUid($pageUid)
            //->setTargetPageType($pageType)
            //->setNoCache($noCache)
            //->setSection($section)
            //->setFormat($format)
            //->setLinkAccessRestrictedPages($linkAccessRestrictedPages)
            //->setArguments($additionalParams)
            //->setCreateAbsoluteUri($absolute)
            //->setAddQueryString($addQueryString)
            //->setArgumentsToBeExcludedFromQueryString($argumentsToBeExcludedFromQueryString)
            //->setAddQueryStringMethod($addQueryStringMethod)
            //->uriFor($action, $arguments, $controller, $extensionName, $pluginName);
            ->uriFor($action, $arguments);
        $this->tag->addAttribute('href', $uri);
        $this->tag->setContent($this->renderChildren());
        $this->tag->forceClosingTag(true);
        return $this->tag->render();
    }
}
