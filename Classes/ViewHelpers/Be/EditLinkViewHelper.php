<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\ViewHelpers\Be;

use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Fluid\ViewHelpers\Be\AbstractBackendViewHelper;

/**
 * ViewHelper to render the file information
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class EditLinkViewHelper extends AbstractBackendViewHelper
{

    /**
     * Arguments initialization
     *
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('table', 'string', 'Edit table name');
        $this->registerArgument('object', 'object', 'The object to be edited');
        $this->registerArgument('action', 'string', 'Edit action (edit, clickmenu or new)');
        $this->registerArgument('pid', 'int', 'Page id (only needed for new items)');
    }

    /**
     * Get backend registration edit link
     *
     * @return string Sort order url parameter
     */
    public function render()
    {
        $table = $this->arguments['table'];
        /** @var AbstractEntity $object */
        $object = $this->arguments['object'];
        $action = $this->arguments['action'];
        $pid = (int)$this->arguments['pid'];
        if ($action === 'clickmenu' && null !== $object) {
            $iconLink = $this->createClickMenuLink($table, $object);
        } else {
            $iconLink = $this->createEditLink($table, $object, $pid);
        }

        return $iconLink;
    }

    private function createEditLink(
        $table,
        AbstractEntity $object = null,
        $pid = 0
    ): string
    {
        if ($object !== null) {
            $id = $object->getUid();
            $action = 'edit';
        } else {
            $action = 'new';
            $id = $pid;
        }
        // Icon list: https://github.com/wmdbsystems/T3.Icons
        $iconMap = array(
            'edit' => 'actions-open',
            'new' => 'actions-document-new',
        );
        $icon = $iconMap[$action];
        /** @var IconFactory $iconFactory */
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        $skinnedIcon = $iconFactory->getIcon($icon, Icon::SIZE_SMALL);
        /** @var UriBuilder $uriBuilder */
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        $params = [
            'edit' => [
                $table => [
                    $id => $action,
                ],
            ],
            'returnNewPageId' => 1,
            'returnUrl' => GeneralUtility::getIndpEnv('REQUEST_URI'),
        ];
        $editUrl = (string)$uriBuilder->buildUriFromRoute('record_edit', $params);
        $jsCode =  'window.location.href=' . GeneralUtility::quoteJSvalue($editUrl) . '; return false;';
        return '<a href="#" onclick="' . htmlspecialchars($jsCode) . '">' . $skinnedIcon . '</a>';
    }

    /**
     * Creates the icon image tag for record from table and wraps it in a link which will trigger the click menu.
     *
     * @param string $table Table name
     * @param AbstractEntity $object
     * @return string
     */
    private function createClickMenuLink($table, AbstractEntity $object): string
    {

        $row = array();
        $rowFields = array('uid', 'pid', 't3ver_state', 't3ver_wsid', 'disabled', 'starttime', 'endtime',);
        foreach ($rowFields as $field) {
            $row[$field] = 0;
        }
        $row['uid'] = $object->getUid();
        $row['pid'] = $object->getPid();
        // Initialization
        //$altText = BackendUtility::getRecordIconAltText($row, $table);
        //$iconFile  = IconUtility::getIcon($table, $row, false);

        /** @var IconFactory $iconFactory */
        $iconFactory = GeneralUtility::makeInstance(IconFactory::class);
        $skinnedIcon = $iconFactory->getIconForRecord($table, $row, Icon::SIZE_SMALL);

        // The icon with link
        return BackendUtility::wrapClickMenuOnIcon($skinnedIcon, $table, $row['uid'], true, '',
            '+info,edit,history');
    }
}
