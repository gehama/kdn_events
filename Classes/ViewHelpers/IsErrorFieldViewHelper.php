<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\ViewHelpers;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractConditionViewHelper;

class IsErrorFieldViewHelper extends AbstractConditionViewHelper {

    /**
     * @return void
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('field', 'string', 'Model field name or custom field name (or comma separated list of names)', true);
        $this->registerArgument('errorList', 'array', 'Fluid variable value: validationResults.flattenedErrors for current object', true);
        $this->registerArgument('fieldGroup', 'string', 'Group name prefix for field)');
    }

    /**
     * This method decides if the condition is TRUE or FALSE. It can be overridden in extending
     * view helpers to adjust functionality.
     *
     * @param array $arguments ViewHelper arguments to evaluate the condition for this ViewHelper, allows for flexibility in overriding this method.
     * @return bool
     */
    protected static function evaluateCondition($arguments = null)
    {
        if (!empty($arguments['field']) && !empty($arguments['errorList'])) {
            $fieldNames = explode(',', $arguments['field']);
            $fieldNamePrefix = '';
            if ($arguments['fieldGroup'] !== '') {
                $fieldNamePrefix = rtrim($arguments['fieldGroup'], '.') . '.';
            }
            $errorList = $arguments['errorList'];
            foreach ($fieldNames as $tmpFieldName) {
                $field = trim(strip_tags($tmpFieldName));
                if (!empty($field)) {
                    $errorKey = $fieldNamePrefix ? $fieldNamePrefix.$field : $field;
                    // Checks for entries in the default validation results
                    if (isset($errorList[$field])
                        || (!empty($fieldNamePrefix) && isset($errorList[$errorKey]))) {
                        return true;
                    }
                    // the results of the object validator are stored in an entry with empty string as key
                    if (isset($errorList[''])) {
                        $objectResult = $errorList[''];
                        // Checks for entries in the object validation results
                        /** @var \TYPO3\CMS\Extbase\Validation\Error $error */
                        foreach ($objectResult as $error) {
                            if ($error->getTitle() === $field) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
	
}
