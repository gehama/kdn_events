<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Controller;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Registration;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class EvaluationController extends BackendController
{

    /**
     * action evaluation
     *
     * @return void
     */
    public function evaluationAction()
    {
    }

    /**
     * action evaluate monthly registrations
     *
     * @return void
     */
    public function evaluateMonthlyAction()
    {
        /** @var Event[] $events */
        $events = $this->eventRepository->findAllForBackend();
        $monthlyRegistrations = [];
        $allMonths = [];
        $minDateTime = date_create('first day of this month -1 year midnight');
        foreach ($events as $event) {
            $eventRegistrationCount = $event->getRegistrations();
            if ($eventRegistrationCount > 0 && $event->getEventStart() >= $minDateTime) {
                $eventRegistrations = $event->getRegistrationList();
                /** @var Registration[] $eventRegistrations */
                if (!isset($monthlyRegistrations[$event->getUid()])) {
                    $monthlyRegistrations[$event->getUid()] = [
                        'title' => $event->getTitle(),
                        'columns' => [],
                    ];
                }
                $monthData =& $monthlyRegistrations[$event->getUid()]['columns'];
                foreach ($eventRegistrations as $registration) {
                    if ($registration->getStatus() === Registration::STATUS_CANCELLED) {
                        continue;
                    }
                    $crdate = $registration->getCrdate();
                    if ($crdate instanceof \DateTime) {
                        $crdate = $crdate->getTimestamp();
                    }
                    $month = date('Ym', $crdate);
                    if (!isset($allMonths[$month])) {
                        $allMonths[$month] = date('m.Y', $crdate);
                    }
                    if (!isset($monthData[$month])) {
                        $monthData[$month] = 1;
                    } else {
                        $monthData[$month] += 1;
                    }
                }
            }
        }
        if (!empty($allMonths)) {
            ksort($allMonths);
            $monthKeys = array_keys($allMonths);
            foreach ($monthlyRegistrations as &$monthEventData) {
                $monthData =& $monthEventData['columns'];
                foreach ($monthKeys as $month) {
                    if (!isset($monthData[$month])) {
                        $monthData[$month] = 0;
                    }
                }
                ksort($monthData);
            }
            unset($monthEventData);
        }
        $this->view->assignMultiple(array(
            'headers' => $allMonths,
            'evaluationData' => $monthlyRegistrations,
        ));
    }

    /**
     * action evaluate location
     *
     * @return void
     */
    public function evaluateLocationsAction()
    {
        /** @var Event[] $events */
        $events = $this->eventRepository->findAllForBackend();
        $monthlyRegistrations = [];
        $allMonths = [];
        $minDateTime = date_create('first day of this month -1 year midnight');
        foreach ($events as $event) {
            $eventRegistrationCount = $event->getRegistrations();
            if ($eventRegistrationCount > 0 && $event->getEventStart() >= $minDateTime && null !== $location = $event->getLocation()) {
                $eventRegistrations = $event->getRegistrationList();
                /** @var Registration[] $eventRegistrations */
                if (!isset($monthlyRegistrations[$location->getUid()])) {
                    $monthlyRegistrations[$location->getUid()] = [
                        'title' => $location->getTitle(),
                        'columns' => [],
                    ];
                }
                $monthData =& $monthlyRegistrations[$location->getUid()]['columns'];
                foreach ($eventRegistrations as $registration) {
                    if ($registration->getStatus() === Registration::STATUS_CANCELLED) {
                        continue;
                    }
                    $crdate = $registration->getCrdate();
                    if ($crdate instanceof \DateTime) {
                        $crdate = $crdate->getTimestamp();
                    }
                    $month = date('Ym', $crdate);
                    if (!isset($allMonths[$month])) {
                        $allMonths[$month] = date('m.Y', $crdate);
                    }
                    if (!isset($monthData[$month])) {
                        $monthData[$month] = 1;
                    } else {
                        $monthData[$month] += 1;
                    }
                }
            }
        }
        if (!empty($allMonths)) {
            ksort($allMonths);
            $monthKeys = array_keys($allMonths);
            foreach ($monthlyRegistrations as &$monthEventData) {
                $monthData =& $monthEventData['columns'];
                foreach ($monthKeys as $month) {
                    if (!isset($monthData[$month])) {
                        $monthData[$month] = 0;
                    }
                }
                ksort($monthData);
            }
            unset($monthEventData);
        }
        $this->view->assignMultiple(array(
            'headers' => $allMonths,
            'evaluationData' => $monthlyRegistrations,
        ));
    }

    /**
     * action evaluate cancelled registrations
     *
     * @return void
     */
    public function evaluateCancellationsAction()
    {
        /** @var Event[] $events */
        $events = $this->eventRepository->findAllForBackend();
        $monthlyRegistrations = [];
        $allMonths = [];
        $minDateTime = date_create('first day of this month -1 year midnight');
        foreach ($events as $event) {
            $eventRegistrationCount = $event->getRegistrations();
            if ($eventRegistrationCount > 0 && $event->getEventStart() >= $minDateTime) {
                $eventRegistrations = $event->getRegistrationList();
                /** @var Registration[] $eventRegistrations */
                if (!isset($monthlyRegistrations[$event->getUid()])) {
                    $monthlyRegistrations[$event->getUid()] = [
                        'title' => $event->getTitle(),
                        'columns' => [],
                    ];
                }
                $monthData =& $monthlyRegistrations[$event->getUid()]['columns'];
                foreach ($eventRegistrations as $registration) {
                    if ($registration->getStatus() != Registration::STATUS_CANCELLED) {
                        continue;
                    }
                    $crdate = $registration->getCrdate();
                    if ($crdate instanceof \DateTime) {
                        $crdate = $crdate->getTimestamp();
                    }
                    $month = date('Ym', $crdate);
                    if (!isset($allMonths[$month])) {
                        $allMonths[$month] = date('m.Y', $crdate);
                    }
                    if (!isset($monthData[$month])) {
                        $monthData[$month] = 1;
                    } else {
                        $monthData[$month] += 1;
                    }
                }
            }
        }
        if (!empty($allMonths)) {
            ksort($allMonths);
            $monthKeys = array_keys($allMonths);
            foreach ($monthlyRegistrations as &$monthEventData) {
                $monthData =& $monthEventData['columns'];
                foreach ($monthKeys as $month) {
                    if (!isset($monthData[$month])) {
                        $monthData[$month] = 0;
                    }
                }
                ksort($monthData);
            }
            unset($monthEventData);
        }
        $this->view->assignMultiple(array(
            'headers' => $allMonths,
            'evaluationData' => $monthlyRegistrations,
        ));
    }

}
