<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Controller;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Exception\RegistrationCancellationException;
use KDN\KdnEvents\Exception\RegistrationValidationException;
use KDN\KdnEvents\Registration\RegistrationManager;
use KDN\KdnEvents\Service\DemandService;
use KDN\KdnEvents\Service\EncryptionService;
use KDN\KdnEvents\Service\MarkerService;
use KDN\KdnEvents\Service\ShowActionConfigurationService;
use KDN\KdnEvents\Utility\ObjectUtility;
use TYPO3\CMS\Core\Http\ImmediateResponseException;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\RequestInterface;
use TYPO3\CMS\Extbase\Mvc\ResponseInterface;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Validation\Validator\NotEmptyValidator;
use TYPO3\CMS\Frontend\Controller\ErrorController;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class EventController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * Event repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\EventRepository
     */
    protected $eventRepository;

    /**
     * Registration repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\RegistrationRepository
     */
    protected $registrationRepository;

    /**
     * Demand service
     *
     * @var DemandService
     */
    protected $demandService;

    /**
     * Show action configuration service
     *
     * @var ShowActionConfigurationService
     */
    protected $showActionConfigurationService;

    /**
     * Registration service
     *
     * @var RegistrationManager
     */
    protected $registrationService;

    /**
     * ICalendar Service
     *
     * @var \KDN\KdnEvents\Service\ICalendarService
     */
    protected $icalendarService;

    /**
     * @var \KDN\KdnEvents\Service\FrontendSessionHandlerService
     */
    protected $frontendSessionHandlerService;

    public function injectEventRepository(\KDN\KdnEvents\Domain\Repository\EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function injectRegistrationRepository(\KDN\KdnEvents\Domain\Repository\RegistrationRepository $registrationRepository)
    {
        $this->registrationRepository = $registrationRepository;
    }

    public function injectDemandService(DemandService $demandService)
    {
        $this->demandService = $demandService;
    }

    public function injectShowActionConfigurationService(ShowActionConfigurationService $showActionConfigurationService)
    {
        $this->showActionConfigurationService = $showActionConfigurationService;
    }

    public function injectRegistrationManager(RegistrationManager $registrationService)
    {
        $this->registrationService = $registrationService;
    }

    /**
     * DI for $icalendarService
     *
     * @param \KDN\KdnEvents\Service\ICalendarService $icalendarService
     */
    public function injectIcalendarService(\KDN\KdnEvents\Service\ICalendarService $icalendarService)
    {
        $this->icalendarService = $icalendarService;
    }

    /**
     * DI for $frontendSessionHandlerService
     *
     * @param \KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService
     */
    public function injectFrontendSessionHandlerService(\KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService)
    {
        $this->frontendSessionHandlerService = $frontendSessionHandlerService;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @throws \Exception
     */
    public function processRequest(RequestInterface $request, ResponseInterface $response)
    {
        try {
            parent::processRequest($request, $response);
        } catch (\Exception $exception) {
            $this->handleKnownExceptionsElseThrowAgain($exception);
        }
    }

    /**
     * Handle known exceptions
     *
     * @param \Exception $exception
     * @throws \Exception
     */
    private function handleKnownExceptionsElseThrowAgain(\Exception $exception): void
    {
        $previousException = $exception->getPrevious();
        $actions = ['showAction', 'registerAction', 'submitAction', 'icalDownloadAction'];
        if ($previousException instanceof \TYPO3\CMS\Extbase\Property\Exception
            && in_array($this->actionMethodName, $actions, true)
        ) {
            $this->handleEventNotFoundError($this->settings);
        }
        throw $exception;
    }

    /**
     * Error handling if event is not found
     *
     * @param array $settings
     */
    protected function handleEventNotFoundError($settings)
    {
        if (!empty($settings['errorHandling'])) {
            $configuration = GeneralUtility::trimExplode(',', $settings['errorHandling'], true);
            switch ($configuration[0]) {
                case 'pageNotFoundHandler':
                    /** @var ErrorController $errorController */
                    $errorController = GeneralUtility::makeInstance(ErrorController::class);
                    $response = $errorController->pageNotFoundAction(
                        $GLOBALS['TYPO3_REQUEST'],
                        'Event not found'
                    );
                    throw new ImmediateResponseException($response, 1592852862);
                    break;
                case 'redirectToListView':
                    $listPid = (int)$settings['backPid'] > 0 ? (int)$settings['backPid'] : null;
                    $this->redirect('list', null, null, null, $listPid);
                    break;
            }
        }
    }

    /**
     * action list
     *
     * @param \KDN\KdnEvents\Domain\Model\Dto\EventDemand $demand
     * @param string $order
     * @return void
     */
    public function listAction(\KDN\KdnEvents\Domain\Model\Dto\EventDemand $demand = null, $order = null)
    {
        $initializedDemand = $this->demandService->initDemandSettings($demand, $this->settings, $order);
        $events = $this->eventRepository->findDemanded($initializedDemand);
        if (empty($this->settings['detailPid'])) {
            $this->settings['detailPid'] = $this->settings['backPid'];
        }
        $this->view->assign('events', $events);
    }

    /**
     * action show
     *
     * @param Event $event
     * @param Registration $registration
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("registration")
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("event")
     */
    public function showAction(Event $event = null, Registration $registration = null)
    {
        if (null === $event) {
            $this->handleEventNotFoundError($this->settings);
        }

        if (!$event->getPublished() || !$event->getStatus()->getEventVisible()) {
            $this->redirect('list');
        }
        $this->view->assignMultiple($this->showActionConfigurationService->initializeShowViewParameters(
            $event,
            $registration,
            $this->settings
        ));
    }

    /**
     * Initiates the iCalendar download for the given event
     *
     * @param Event $event The event
     *
     * @return string|false
     */
    public function icalDownloadAction(Event $event = null)
    {
        if (null === $event) {
            $this->handleEventNotFoundError($this->settings);
        }
        $extbaseConfig = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
            'KdnEvents'
        );
        $this->icalendarService->downloadiCalendarFile($event, $extbaseConfig['view']);
        exit;
    }

    /**
     * initialize register action
     *
     * @param void
     */
    public function initializeRegisterAction()
    {
        $this->setPropertyMappingForEnabledFields();
    }

    /**
     * initialize submit action
     *
     * @param void
     */
    public function initializeSubmitAction()
    {
        $this->setPropertyMappingForEnabledFields();
    }

    private function setPropertyMappingForEnabledFields(): void
    {
        $propertyMappingConfiguration = $this->arguments->getArgument('registration')
            ->getPropertyMappingConfiguration();
        $availableFields = $this->registrationService->getAvailableEnableProperties();
        foreach ($availableFields as $propertyName) {
            if ($propertyName === 'birthDate') {
                $dateFormat = 'd.m.Y';
                $propertyMappingConfiguration->allowProperties($propertyName)
                    ->forProperty($propertyName)
                    ->setTypeConverterOption(
                        DateTimeConverter::class,
                        DateTimeConverter::CONFIGURATION_DATE_FORMAT,
                        $dateFormat
                    );
            } elseif ($propertyName === 'workAreas') {
                $propertyMappingConfiguration->allowProperties($propertyName)
                    ->forProperty($propertyName)
                    ->setTypeConverter(new \TYPO3\CMS\Extbase\Property\TypeConverter\ObjectStorageConverter());
            } else {
                $propertyMappingConfiguration->allowProperties($propertyName);
            }
        }
    }

    /**
     * Initialize action method validators
     *
     * @return void
     */
    protected function initializeActionMethodValidators()
    {
        parent::initializeActionMethodValidators();
        if ($this->actionMethodName === 'submitAction' || $this->actionMethodName === 'registerAction') {
            // TODO: check billing required fields
            foreach ($this->arguments as $argument) {
                /** @var \TYPO3\CMS\Extbase\Mvc\Controller\Argument $argument */
                if ($argument->getName() === 'registration') {
                    $genericObjectValidator = $this->getArgumentGenericObjectValidator($argument);
                    $requestArguments = $this->request->getArguments();
                    $eventId = (int)$requestArguments['event'];
                    $event = $this->eventRepository->findByUid($eventId);
                    if ($genericObjectValidator !== null && $event !== null) {
                        /** @var \TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator $genericObjectValidator */
                        /** @var Event $event */
                        $enabledRequiredFieldsCsv = $event->getEnableRequiredFields();
                        if (!empty($enabledRequiredFieldsCsv)) {
                            $enabledRequiredFields = explode(',', $enabledRequiredFieldsCsv);
                            $configurationService = $this->registrationService;
                            foreach ($enabledRequiredFields as $requiredField) {
                                if (!empty($requiredField) && $configurationService->isFieldEnabledForEvent($requiredField, $event)) {
                                    $propertyName = lcfirst(trim(str_replace(' ', '', ucwords(str_replace('_', ' ', $requiredField)))));
                                    /** @var NotEmptyValidator $notEmptyValidator */
                                    $notEmptyValidator = $this->objectManager->get(NotEmptyValidator::class);
                                    $genericObjectValidator->addPropertyValidator($propertyName, $notEmptyValidator);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param \TYPO3\CMS\Extbase\Mvc\Controller\Argument $argument
     * @return \TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator|null
     */
    private function getArgumentGenericObjectValidator(\TYPO3\CMS\Extbase\Mvc\Controller\Argument $argument): ?\TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator
    {
        /* @var \TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator $validator */
        $validator = $argument->getValidator();
        $genericObjectValidator = null;
        $argValidators = $validator->getValidators();
        foreach ($argValidators as $validator) {
            if ($validator instanceof \TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator) {
                $subValidators = $validator->getValidators();
                foreach ($subValidators as $subValidator) {
                    if ($subValidator instanceof \TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator) {
                        $genericObjectValidator = $subValidator;
                        break;
                    }
                }
            }
        }
        return $genericObjectValidator;
    }

    /**
     * Submit registration create
     *
     * @param Event $event
     * @param Registration $registration
     * @param string $notes Used as Honeypot field for spammers
     * @param string $confirmation
     * @return void
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("event")
     */
    public function submitAction(Event $event, Registration $registration = null, $notes = null, $confirmation = null)
    {
        if (null === $registration) {
            $this->redirect('show', NULL, NULL, ['event' => $event]);
        }
        if (!$event->getPublished() || !$event->getStatus()->getEventVisible()) {
            $this->redirect('list');
        }
        try {
            $registration->setEvent($event);
            $parameters = $this->registrationService->initializeSubmitViewParameters(
                $event,
                $registration,
                $notes,
                $confirmation,
                $this->settings
            );
            //$encryptionService = new EncryptionService();
            $this->view->assignMultiple($parameters);
        } catch (RegistrationValidationException $e) {
            $message = LocalizationUtility::translate($e->getMessageLabelKey(), 'kdn_events');
            $this->addFlashMessage($message, '', $e->getMessageLevel());
            $this->forwardShowWithData($event, $registration);
        }
    }

    private function forwardShowWithData(Event $event, Registration $registration)
    {
        // Forwarding the registration object will cause a serialization error
        // Therefore we save the registration data in the session and the redirect the user to the show action
        // where the registration is filled with the session data again
        $this->frontendSessionHandlerService->storeRegistrationData($registration);
        $this->redirect('show', NULL, NULL, array('event' => $event));
        //$this->forward('show', NULL, NULL, array('event' => $event, 'registration' => $registration));
    }

    /**
     * action create
     *
     * @param Event $event
     * @param Registration $registration
     * @param string $notes Used as Honeypot field for spammers
     * @param string $confirmation
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("event")
     */
    public function registerAction(Event $event, Registration $registration, $notes = null, $confirmation = null)
    {
        $isSpam = !empty($notes);
        if (!$isSpam) {
            $encryptionService = new EncryptionService();
            $value = empty($confirmation) ? '' : trim($encryptionService->decrypt($confirmation));
            if (empty($value) || strtotime($value) > time() - 3) {
                //if (empty($value) || strtotime($value) < time() - 3600 || strtotime($value) > time() - 3) {
                $isSpam = true;
            }
        }
        $requestArguments = $this->request->getArguments();
        if (!empty($requestArguments['back'])) {
            $regArguments = $requestArguments['registration'];
            $this->frontendSessionHandlerService->storeRegistrationData($regArguments ?? []);
            $this->redirect('show', NULL, NULL, ['event' => $event]);
            // Spam protection
        } elseif (!$isSpam) {
            try {
                $registration->setEvent($event);
                // assign the registration invitation if set
                $invitation = $this->showActionConfigurationService->initInvitation($registration);
                $this->registrationService->processRegistration(
                    $event,
                    $registration,
                    $this->settings
                );

                $this->registrationService->sendRegistrationNotifications($event, $registration, $this->settings['mail']);
                // Clear the invitation hash
                if (null !== $invitation) {
                    $this->frontendSessionHandlerService->storeInvitationHash(null);
                }
            } catch (RegistrationValidationException $e) {
                $message = LocalizationUtility::translate($e->getMessageLabelKey(), 'kdn_events');
                $this->addFlashMessage($message, '', $e->getMessageLevel());
                $this->forwardShowWithData($event, $registration);
            }

            // Redirect to confirmation page
            $arguments = ['hash' => $registration->getHash()];
            $redirectPageUid = (int)$this->settings['redirectPage'];
            if ($event->getStatus()->isWaitlistOpen()) {
                $tmpRedirectPid = (int)$this->settings['redirectPageWaitlist'];
                if ($tmpRedirectPid > 0) {
                    $redirectPageUid = $tmpRedirectPid;
                }
            }
            $this->redirect('confirm', NULL, NULL, $arguments, $redirectPageUid);

        } else {
            $this->redirect('list');
        }
    }

    /**
     * action confirm
     *
     * @param string $hash
     *
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    public function confirmAction($hash = null)
    {
        $registration = empty($hash) ? null : $this->registrationRepository->findOneByHash($hash);
        /** @var Registration|null $registration */
        if (null !== $registration && null !== $event = $registration->getEvent()) {
            /** @var MarkerService $markerService */
            $markerService = $this->objectManager->get(MarkerService::class);
            $markerService->setMode('FE');
            $markers = $markerService->getRegistrationMarkers($registration);
            $search = array_keys($markers);
            $textRte = $this->settings['confirmationText'];
            $textRte = str_replace($search, $markers, $textRte);
            if (empty($this->settings['detailPid'])) {
                $this->settings['detailPid'] = $this->settings['backPid'];
            }
            $tsFe = ObjectUtility::getTyposcriptFrontendController();
            // Disallow current page as detail page; only confirmation is allowed here
            if ((int) $this->settings['detailPid'] === (int) $tsFe->page['uid']) {
                $this->settings['detailPid'] = (int) $tsFe->page['pid'];
            }
            $this->view->assign('settings', $this->settings);
            /*$contentObject = $this->configurationManager->getContentObject();
            $confirmationText = $contentObject->parseFunc($textRte, array(), '< ' . 'lib.parseFunc_RTE');
            $this->view->assign('registrationContent', $confirmationText);*/
            $this->view->assign('registrationContent', $textRte);
            $this->view->assign('event', $event);
        } else {
            $pageUid = (int)$this->settings['backPid'];
            $this->redirect('list', NULL, NULL, NULL, $pageUid);
        }
    }

    /**
     * action registration cancel
     *
     * @param string $hash
     * @param string $email
     * @param string $notes Used as Honeypot field for spammers
     *
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    public function cancelAction($hash = null, $email = null, $notes = null)
    {
        $redirectToList = false;
        $registration = null;
        $showForm = empty($email);
        $exception = null;
        try {
            $registration = $this->registrationService->processCancelAction($hash, $email, $this->settings);
        } catch (RegistrationCancellationException $e) {
            $exception = $e;
            if ($e->getCode() === RegistrationCancellationException::CODE_CANNOT_BE_CANCELLED) {
                $e->setMessageLabelKey('errors.registration_cannot_be_canceled');
                $redirectToList = true;
            } elseif ($e->getCode() === RegistrationCancellationException::CODE_ALREADY_CANCELLED) {
                $showForm = false;
            }
        } catch (IllegalObjectTypeException $e) {
            $this->addFlashMessage($e->getMessage(), '', FlashMessage::ERROR);
            $redirectToList = true;
        } catch (UnknownObjectException $e) {
            $this->addFlashMessage($e->getMessage(), '', FlashMessage::ERROR);
            $redirectToList = true;
        }
        if ($redirectToList || null === $registration) {
            if (null !== $exception) {
                $message = LocalizationUtility::translate($exception->getMessageLabelKey(), 'kdn_events');
                $this->addFlashMessage($message, '', FlashMessage::ERROR);
            }
            $pageUid = (int)$this->settings['backPid'];
            $this->redirect('list', NULL, NULL, NULL, $pageUid);
        } else {
            if (null !== $exception) {
                $message = LocalizationUtility::translate($exception->getMessageLabelKey(), 'kdn_events');
                $this->addFlashMessage($message, '', FlashMessage::ERROR);
            } elseif (!$showForm) {
                $message = LocalizationUtility::translate('message_registration_cancelled', 'kdn_events');
                $this->addFlashMessage($message, '', FlashMessage::OK);
            }
            /** @var Registration $registration */
            $this->view->assignMultiple(array(
                'event' => $registration->getEvent(),
                'registration' => $registration,
                'notes' => (string)$notes,
                'showForm' => $showForm,
            ));
        }
    }

    /*
     * Deactivate flash message for form errors
     *
     * @see \TYPO3\CMS\Extbase\MVC\Controller\ActionController::getErrorFlashMessage()
     */
    protected function getErrorFlashMessage()
    {
        return FALSE;
    }

}
