<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Controller;

use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\Invitation;
use KDN\KdnEvents\Domain\Model\PersonInterface;
use KDN\KdnEvents\Invitation\InvitationManager;
use KDN\KdnEvents\Invitation\InvitationValidationException;
use KDN\KdnEvents\Seo\EventTitleProvider;
use KDN\KdnEvents\Service\EncryptionService;
use KDN\KdnEvents\Service\MarkerService;
use KDN\KdnEvents\Service\ShowActionConfigurationService;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Http\ImmediateResponseException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\RequestInterface;
use TYPO3\CMS\Extbase\Mvc\ResponseInterface;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\Controller\ErrorController;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class InvitationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * Event repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\EventRepository
     */
    protected $eventRepository;

    /**
     * Show action configuration service
     *
     * @var ShowActionConfigurationService
     */
    protected $showActionConfigurationService;

    /**
     * Invitation service
     *
     * @var InvitationManager
     */
    protected $invitationService;

    /**
     * @var \KDN\KdnEvents\Service\FrontendSessionHandlerService
     */
    protected $frontendSessionHandlerService;

    /**
     * @var MarkerService
     */
    protected $markerService;

    public function injectEventRepository(\KDN\KdnEvents\Domain\Repository\EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function injectShowActionConfigurationService(ShowActionConfigurationService $showActionConfigurationService)
    {
        $this->showActionConfigurationService = $showActionConfigurationService;
    }

    public function injectInvitationManager(InvitationManager $invitationService)
    {
        $this->invitationService = $invitationService;
    }

    /**
     * DI for $frontendSessionHandlerService
     *
     * @param \KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService
     */
    public function injectFrontendSessionHandlerService(\KDN\KdnEvents\Service\FrontendSessionHandlerService $frontendSessionHandlerService)
    {
        $this->frontendSessionHandlerService = $frontendSessionHandlerService;
    }

    /**
     * @param MarkerService $markerService
     */
    public function injectMarkerService(MarkerService $markerService)
    {
        $this->markerService = $markerService;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @throws \Exception
     */
    public function processRequest(RequestInterface $request, ResponseInterface $response)
    {
        try {
            parent::processRequest($request, $response);
        } catch (\Exception $exception) {
            $this->handleKnownExceptionsElseThrowAgain($exception);
        }
    }

    /**
     * Handle known exceptions
     *
     * @param \Exception $exception
     * @throws \Exception
     */
    private function handleKnownExceptionsElseThrowAgain(\Exception $exception): void
    {
        $previousException = $exception->getPrevious();
        $actions = ['showAction', 'inviteAction', 'submitAction',];
        if ($previousException instanceof \TYPO3\CMS\Extbase\Property\Exception
            && in_array($this->actionMethodName, $actions, true)
        ) {
            $this->handleEventNotFoundError($this->settings);
        }
        throw $exception;
    }

    /**
     * Error handling if event is not found
     *
     * @param array $settings
     */
    protected function handleEventNotFoundError($settings)
    {
        if (!empty($settings['errorHandling'])) {
            $configuration = GeneralUtility::trimExplode(',', $settings['errorHandling'], true);
            switch ($configuration[0]) {
                case 'pageNotFoundHandler':
                    /** @var ErrorController $errorController */
                    $errorController = GeneralUtility::makeInstance(ErrorController::class);
                    $response = $errorController->pageNotFoundAction(
                        $GLOBALS['TYPO3_REQUEST'],
                        'Event not found'
                    );
                    throw new ImmediateResponseException($response, 1592852862);
                    break;
                case 'redirectToListView':
                    $listPid = (int)$settings['backPid'] > 0 ? (int)$settings['backPid'] : null;
                    $this->redirect('list', 'Event', null, null, $listPid);
                    break;
            }
        }
    }

    /**
     * action show
     *
     * @param Event $event
     * @param Invitation $invitation
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("invitation")
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("event")
     */
    public function showAction(Event $event = null, Invitation $invitation = null)
    {
        if (null === $event) {
            $this->handleEventNotFoundError($this->settings);
        }
        if (!$this->invitationService->isInvitationEnabledForEvent($event)) {
            $this->redirectToEventListView();
        }

        $context = GeneralUtility::makeInstance(Context::class);
        /** @var \TYPO3\CMS\Core\Context\UserAspect $userAspect */
        $userAspect = $context->getAspect('frontend.user');
        $userGroupIds = $userAspect->getGroupIds();        
        $matchingGroups = array_intersect([
            1,//Member
            2,// Regional speaker
            4,//Board
        ], $userGroupIds);
        if (empty($matchingGroups)) {
            $this->redirectToEventListView();
        }
        if (null === $invitation) {
            $invitation = new Invitation();
            $feSessionHandler = $this->frontendSessionHandlerService;
            $feSessionHandler->prefillSessionInvitationDataIfSet($invitation);
        }

        // TODO: move this to vinissima ext!
        $fixedProperties = ['salutation'];
        $invitation->setSalutation(PersonInterface::SALUTATION_FEMALE);

        $encryptionService = new EncryptionService();
        /** @var EventTitleProvider $provider */
        $provider = GeneralUtility::makeInstance(EventTitleProvider::class);
        $providerConfiguration = [];
        $provider->setTitleByEvent($event, $providerConfiguration);

        $parameters = [
            'event' => $event,
            'invitation' => $invitation,
            'cryptKey' => $encryptionService->encrypt(date('Y-m-d H:i:s')),
            'enabledFields' => $this->invitationService->getAvailableEnableProperties(),
            'requiredFields' => [],
            'notes' => '',
            'fixedProperties' => $fixedProperties,
        ];
        $this->view->assignMultiple($parameters);
    }

    private function redirectToEventShowView(Event $event)
    {
        $detailPid = $this->settings['detailPid'];
        $this->redirect('show', 'Event', null, ['event' => $event], $detailPid);
    }

    private function redirectToEventListView()
    {
        $listPid = (int)$this->settings['backPid'] > 0 ? (int)$this->settings['backPid'] : $this->settings['detailPid'];
        $this->redirect('list', 'Event', null, null, $listPid);
    }

    /**
     * initialize register action
     *
     * @param void
     */
    public function initializeRegisterAction()
    {
        $this->setPropertyMappingForEnabledFields();
    }

    /**
     * initialize submit action
     *
     * @param void
     */
    public function initializeSubmitAction()
    {
        $this->setPropertyMappingForEnabledFields();
    }

    private function setPropertyMappingForEnabledFields(): void
    {
        $propertyMappingConfiguration = $this->arguments->getArgument('invitation')
            ->getPropertyMappingConfiguration();
        $availableProperties = $this->invitationService->getAvailableEnableProperties();
        foreach ($availableProperties as $propertyName) {
            $propertyMappingConfiguration->allowProperties($propertyName);
        }
    }

    /**
     * Submit invitation create
     *
     * @param Event $event
     * @param Invitation $invitation
     * @param string $notes Used as Honeypot field for spammers
     * @param string $confirmation
     * @return void
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("event")
     */
    public function submitAction(Event $event, Invitation $invitation = null, $notes = null, $confirmation = null)
    {
        if (null === $invitation) {
            $this->redirectToEventShowView($event);
        }
        if (!$this->invitationService->isInvitationEnabledForEvent($event)) {
            $this->redirectToEventListView();
        }
        try {
            $parameters = $this->invitationService->initializeSubmitViewParameters(
                $event,
                $invitation,
                $notes,
                $confirmation,
                $this->settings
            );
            $this->view->assignMultiple($parameters);
        } catch (InvitationValidationException $e) {
            $labelKey = $e->getMessageLabelKey();
            $message = (string)LocalizationUtility::translate($labelKey, 'kdn_events');
            if (empty($message)) {
                $message = $labelKey;
            }
            $this->addFlashMessage($message, '', $e->getMessageLevel());
            $this->forwardShowWithData($event, $invitation);
        }
    }

    private function forwardShowWithData(Event $event, Invitation $invitation)
    {
        // Forwarding the invitation object will cause a serialization error
        // Therefore we save the invitation data in the session and the redirect the user to the show action
        // where the invitation is filled with the session data again
        $this->frontendSessionHandlerService->storeInvitationData($invitation);
        $this->redirect('show', NULL, NULL, ['event' => $event]);
        //$this->forward('show', NULL, NULL, array('event' => $event, 'invitation' => $invitation));
    }

    /**
     * action create
     *
     * @param Event $event
     * @param Invitation $invitation
     * @param string $notes Used as Honeypot field for spammers
     * @param string $confirmation
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("event")
     */
    public function inviteAction(Event $event, Invitation $invitation, $notes = null, $confirmation = null)
    {
        $isSpam = !empty($notes);
        if (!$isSpam) {
            $encryptionService = new EncryptionService();
            $value = empty($confirmation) ? '' : trim($encryptionService->decrypt($confirmation));
            if (empty($value) || strtotime($value) > time() - 3) {
                //if (empty($value) || strtotime($value) < time() - 3600 || strtotime($value) > time() - 3) {
                $isSpam = true;
            }
        }
        $requestArguments = $this->request->getArguments();
        if (!empty($requestArguments['back'])) {
            $regArguments = $requestArguments['invitation'];
            $this->frontendSessionHandlerService->storeInvitationData($regArguments ?? []);
            $this->redirect('show', 'Invitation', NULL, ['event' => $event]);
            // Spam protection
        } elseif (!$isSpam) {
            try {
                $this->invitationService->processInvitation(
                    $event,
                    $invitation,
                    $this->settings
                );

                $this->invitationService->sendInvitationNotifications($event, $invitation, $this->settings['mail']);
            } catch (InvitationValidationException $e) {
                $message = LocalizationUtility::translate($e->getMessageLabelKey(), 'kdn_events');
                $this->addFlashMessage($message, '', $e->getMessageLevel());
                $this->forwardShowWithData($event, $invitation);
            }

            // Redirect to confirmation page
            $arguments = ['hash' => $invitation->getHash()];
            $this->redirect('confirm', NULL, NULL, $arguments);

        } else {
            $this->redirectToEventListView();
        }
    }

    /**
     * action accept
     *
     * @param string $hash
     *
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    public function acceptAction($hash = null)
    {
        $invitation = $this->invitationService->getInvitationByHash((string)$hash);
        /** @var Invitation|null $invitation */
        if (null !== $invitation && null !== $event = $invitation->getEvent()) {
            $feSessionHandler = $this->frontendSessionHandlerService;
            $feSessionHandler->storeInvitationHash($hash);
            $this->redirectToEventShowView($event);
        } else {
            $this->redirectToEventListView();
        }
    }


    /**
     * action confirm
     *
     * @param string $hash
     *
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    public function confirmAction($hash = null)
    {
        $invitation = $this->invitationService->getInvitationByHash((string)$hash);
        /** @var Invitation|null $invitation */
        if (null !== $invitation && null !== $event = $invitation->getEvent()) {
            $markerService = $this->markerService;
            $markerService->setMode('FE');
            $markers = $markerService->getInvitationMarkers($invitation);
            $search = array_keys($markers);
            $textRte = $this->settings['confirmationText'];
            $textRte = str_replace($search, $markers, $textRte);
            /*$contentObject = $this->configurationManager->getContentObject();
            $confirmationText = $contentObject->parseFunc($textRte, array(), '< ' . 'lib.parseFunc_RTE');
            $this->view->assign('invitationContent', $confirmationText);*/
            $this->view->assign('invitationContent', $textRte);
            $this->view->assign('event', $event);
        } else {
            $this->redirectToEventListView();
        }
    }

    /*
     * Deactivate flash message for form errors
     *
     * @see \TYPO3\CMS\Extbase\MVC\Controller\ActionController::getErrorFlashMessage()
     */
    protected function getErrorFlashMessage()
    {
        return FALSE;
    }

}
