<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Controller;

use KDN\KdnEvents\Domain\Model\DemandInterface;
use KDN\KdnEvents\Domain\Model\Dto\AbstractDemand;
use KDN\KdnEvents\Domain\Model\Dto\EventDemand;
use KDN\KdnEvents\Domain\Model\Dto\RegistrationDemand;
use KDN\KdnEvents\Domain\Model\Event;
use KDN\KdnEvents\Domain\Model\HasEventInterface;
use KDN\KdnEvents\Domain\Model\HasStatusInterface;
use KDN\KdnEvents\Domain\Model\Mailing;
use KDN\KdnEvents\Domain\Model\Registration;
use KDN\KdnEvents\Domain\Model\Status;
use KDN\KdnEvents\Domain\Repository\EventRepository;
use KDN\KdnEvents\Domain\Repository\RegistrationRepository;
use KDN\KdnEvents\Domain\Repository\StatusRepository;
use KDN\KdnEvents\Service\AbstractService;
use KDN\KdnEvents\Service\BackendSessionHandlerService;
use KDN\KdnEvents\Service\EventService;
use KDN\KdnEvents\Service\ExcelExportService;
use KDN\KdnEvents\Service\MailService;
use KDN\KdnEvents\Service\MarkerService;
use KDN\KdnEvents\Service\PdfCreator;
use KDN\KdnEvents\Service\SpreadsheetExportService;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\File\BasicFileUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class BackendController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * TypoScript settings
     *
     * @var array
     */
    protected $settings = array();

    /**
     * id of selected page
     *
     * @var int
     */
    protected $id;

    /**
     * info of selected page
     *
     * @var array
     */
    protected $pageinfo;

    /**
     * Event repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\EventRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $eventRepository;

    /**
     * Event status repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\StatusRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $statusRepository;

    /**
     * Registration repository
     *
     * @var \KDN\KdnEvents\Domain\Repository\RegistrationRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $registrationRepository;

    protected function initializeAction()
    {
        /** @var EventService $eventService */
        $eventService = $this->objectManager->get(EventService::class);
        $pagesWithRegistrations = $eventService->getRegistrationPagesInfo();
        $pageIds = array_keys($pagesWithRegistrations);

        /** @var \TYPO3\CMS\Core\Authentication\BackendUserAuthentication $beUser */
        $beUser = $GLOBALS['BE_USER'];

        // Only load events and registrations from pages the current user has access to
        $allowedPageIds = array();
        $selectedPageId = 0;
        foreach ($pageIds as $pageId) {
            $pageInfo = BackendUtility::readPageAccess($pageId, $beUser->getPagePermsClause(1));
            if (!empty($pageInfo)) {
                if (empty($selectedPageId)) {
                    $selectedPageId = $pageId;
                    $this->pageinfo = $pageInfo;
                }
                $allowedPageIds[] = $pageId;
            }
        }
        $this->id = $selectedPageId > 0 ? $selectedPageId : (int)GeneralUtility::_GP('id');
        if (empty($allowedPageIds)) {
            $allowedPageIds[] = $this->id;
        }
        /** @var Typo3QuerySettings $defaultQuerySettings */
        $defaultQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $defaultQuerySettings->setStoragePageIds($allowedPageIds);
        $defaultQuerySettings->setRespectStoragePage(true);
        // don't add fields from enable fields constraint
        //$defaultQuerySettings->setRespectEnableFields(FALSE);
        // don't add sys_language_uid constraint
        //$defaultQuerySettings->setRespectSysLanguage(FALSE);
        if (null === $this->registrationRepository) {
            $this->registrationRepository = $this->objectManager->get(RegistrationRepository::class);
        }
        $this->registrationRepository->setDefaultQuerySettings($defaultQuerySettings);
        if (null === $this->eventRepository) {
            $this->eventRepository = $this->objectManager->get(EventRepository::class);
        }
        $this->eventRepository->setDefaultQuerySettings($defaultQuerySettings);

        $this->settings = $this->configurationManager->getConfiguration(
            ConfigurationManager::CONFIGURATION_TYPE_SETTINGS
        );
        if (empty($this->settings)) {
            $this->settings = $this->initSettingsForPageId($this->id);
        }
        parent::initializeAction();
    }

    /**
     * Fallback function to load TypoScript settings for module if page outside of main page tree (containing
     * the event registration pages) is selected;
     * Instead of loading the TypoScript from the current page, the event storage page is given
     *
     * @param int $pageId
     * @return array|null
     */
    private function initSettingsForPageId($pageId): ?array
    {
        $settings = null;
        if ($pageId > 0) {
            $this->id = $pageId;
            $settings = AbstractService::loadEventsTS($pageId, true, 'module.');
        }
        return $settings;
    }

    /**
     * action events
     *
     * @param \KDN\KdnEvents\Domain\Model\Dto\EventDemand $demand
     * @param string $order
     *
     * @return void
     */
    public function eventsAction(\KDN\KdnEvents\Domain\Model\Dto\EventDemand $demand = null, $order = null)
    {
        $statusChoices = [];
        $defaultStatus = null;
        if (null === $this->statusRepository) {
            $this->statusRepository = $this->objectManager->get(StatusRepository::class);
        }
        $statusList = $this->statusRepository->findAll();
        foreach ($statusList as $status) {
            /** @var Status $status */
            $statusChoices[$status->getUid()] = $status->getName();
            if (null === $defaultStatus && $status->isEventVisible()) {
                $defaultStatus = $status->getUid();
            }
        }
        //$events = $this->eventRepository->findAllForBackend();
        if (null === $demand) {
            $demand = new EventDemand();
            $demand->setStatus($defaultStatus);
        } else {
            $demand->setSubmitted(true);
        }
        $this->initDemandSettings($demand, $order);
        $demand->setPublishedState(EventDemand::RESTRICTION_IGNORE);
        $events = $this->eventRepository->findDemanded($demand);
        $this->view->assignMultiple([
            'events' => $events,
            'demand' => $demand,
            'newItemPid' => $this->settings['newItemPid'],
            'statusChoices' => $statusChoices,
        ]);
    }

    /**
     * action list
     *
     * @param \KDN\KdnEvents\Domain\Model\Dto\RegistrationDemand $demand
     * @param string $order
     *
     * @return void
     */
    public function listAction(\KDN\KdnEvents\Domain\Model\Dto\RegistrationDemand $demand = null, $order = null)
    {
        $events = $this->eventRepository->findAllForBackend();
        // #21968: update registration count; TODO: create command controller for registration count update
        $demand = $this->initRegistrationDemand($demand, true, $order);
        $statusChoices = [
            Registration::STATUS_REGISTERED,
            Registration::STATUS_PARTICIPATED,
            Registration::STATUS_NOT_PRESENT,
            Registration::STATUS_WAITLIST,
            Registration::STATUS_CANCELLED,
        ];
        $registrations = $this->registrationRepository->findDemanded($demand);
        /** @var Mailing $mailing */
        $mailing = $this->objectManager->get(Mailing::class);
        $this->view->assignMultiple([
            'events' => $events,
            'registrations' => $registrations,
            'demand' => $demand,
            'mailing' => $mailing,
            'statusChoices' => $statusChoices,
        ]);
    }

    /**
     * action export
     *
     * @param \KDN\KdnEvents\Domain\Model\Dto\RegistrationDemand $demand
     * @param string $order
     *
     * @return void
     */
    public function exportAction(\KDN\KdnEvents\Domain\Model\Dto\RegistrationDemand $demand = null, $order = null)
    {
        $demand = $this->initRegistrationDemand($demand, false, $order);
        $registrations = $this->registrationRepository->findDemanded($demand);
        if (class_exists('\PhpOffice\PhpSpreadsheet\Spreadsheet')) {
            $exportService = $this->objectManager->get(SpreadsheetExportService::class);
        } else {
            $exportService = $this->objectManager->get(ExcelExportService::class);
        }
        /** @var \KDN\KdnEvents\Service\AbstractExportService $exportService */


        $event = $demand->getEvent();
        $sheetName = LocalizationUtility::translate('heading.registrations', 'kdn_events');
        $fileBaseName = $sheetName . '_' . ($event !== null ? $event->getTitle() : 'KDN_Event');
        $exportService->create($sheetName, $registrations);
        $exportService->sendFile($fileBaseName, 'xlsx');
    }

    /**
     * @param RegistrationDemand|null $demand
     * @param bool $updateEventRegistrationCounts
     * @param string|null $order
     * @return RegistrationDemand
     */
    protected function initRegistrationDemand(
        ?RegistrationDemand $demand = null,
        bool $updateEventRegistrationCounts = false,
        $order = null
    ): RegistrationDemand
    {
        if (null === $demand) {
            $demand = new RegistrationDemand();
        } else {
            $demand->setSubmitted(true);
        }
        /** @var EventService $eventService */
        $eventService = $this->objectManager->get(EventService::class);
        $this->initDemandSettings($demand, $order);
        if (null === $demand->getEvent()) {
            $events = $this->eventRepository->findAllForBackend();
            $lastEvent = null;
            foreach ($events as $event) {
                if ($updateEventRegistrationCounts) {
                    $eventRegistrationCount = $eventService->updateEventRegistrationCount($event);
                } else {
                    $eventRegistrationCount = $event->getRegistrations();
                }
                /** @var Event $event */
                if ($eventRegistrationCount > 0
                    && (null === $lastEvent || $event->getEventStart() > $lastEvent->getEventStart())) {
                    $lastEvent = $event;
                }
            }
            $demand->setEvent($lastEvent);
        }
        return $demand;
    }

    /**
     * action export
     *
     * @param \KDN\KdnEvents\Domain\Model\Dto\RegistrationDemand $demand
     * @param string $order
     *
     * @return void
     */
    public function exportPdfAction(\KDN\KdnEvents\Domain\Model\Dto\RegistrationDemand $demand = null, $order = null)
    {
        $demand = $this->initRegistrationDemand($demand, false, $order);
        $registrations = $this->registrationRepository->findDemanded($demand);
        /** @var PdfCreator $exportService */
        $exportService = $this->objectManager->get(PdfCreator::class);

        $groupedList = [];
        foreach ($registrations as $registration) {
            /** @var Event $event */
            $event = $registration->getEvent();
            if (null !== $event) {
                if (!isset($groupedList[$event->getUid()])) {
                    $eventStart = $event->getEventStart();
                    if (null !== $eventStart) {
                        $title = date('Y-m-d', $eventStart->getTimestamp()) . '_' . $event->getTitle();
                    } else {
                        $title = $event->getTitle();
                    }
                    $groupedList[$event->getUid()] = [
                        'event' => $event,
                        'title' => $title,
                        'registrations' => [],
                    ];
                }
                $groupedList[$event->getUid()]['registrations'][] = $registration;
            }
        }
        if (count($groupedList) > 1) {
            $title = date('Y-m-d') . '_Veranstaltungen';
        } else {
            $title = '';
            if (!empty($groupedList)) {
                $groupData = current($groupedList);
                $title = $groupData['title'];
                if (strlen($title) > 40) {
                    $title = trim(substr($title, 0, 40));
                }
            }
        }
        foreach ($groupedList as $group) {
            $event = $group['event'];
            /** @var Event $event */
            $registrations = $group['registrations'];
            /** @var Registration[] $registrations */
            $exportService->setEvent($event);
            $exportService->create('export.pdf.participant_list', $registrations);
        }

        $fileBaseName = 'Teilnehmerliste_' . $title;
        $exportService->sendFile($fileBaseName, 'pdf');
    }

    /**
     *
     * @param DemandInterface|AbstractDemand $demand
     * @param string $reqOrder
     *
     * @return void
     */
    protected function initDemandSettings(DemandInterface $demand, $reqOrder = null): void
    {
        $sessionHandler = new BackendSessionHandlerService();
        $sessionKey = $demand->getSessionKey();
        if (!$demand->isSubmitted()) {
            $eventId = (int)$sessionHandler->get($sessionKey . '.eventId');
            $searchTerm = $sessionHandler->get($sessionKey . '.searchTerm');
            $order = $sessionHandler->get($sessionKey . '.order');
            if ($searchTerm !== null) {
                $demand->setSearchTerm($searchTerm);
            }
            if ($order !== null && in_array(explode(' ', $order)[0], $demand->getValidSortProperties(), false)) {
                $demand->setOrder($order);
            } else {
                $demand->setDefaultOrdering();
            }
            if ($demand instanceof HasEventInterface) {
                /** @var Event $event */
                $event = null;
                if ($eventId > 0) {
                    $event = $this->eventRepository->findByUid($eventId);
                }
                /*if ($event === null && $events->count() > 0) {
                    $event = $events->getFirst();
                }*/
                if (null !== $event) {
                    $demand->setEvent($event);
                }
            }
            if ($demand instanceof HasStatusInterface) {
                $status = (int)$sessionHandler->get($sessionKey . '.status');
                if (!empty($status)) {
                    $demand->setStatus($status);
                }
            }
        } else {
            if ($demand instanceof HasEventInterface) {
                $eventId = $demand->getEvent() === null ? null : $demand->getEvent()->getUid();
                $sessionHandler->store($sessionKey . '.eventId', $eventId);
            }
            if ($demand instanceof HasStatusInterface) {
                $sessionHandler->store($sessionKey . '.status', $demand->getStatus());
            }
            $sessionHandler->store($sessionKey . '.searchTerm', $demand->getSearchTerm());
            //$sessionHandler->store($sessionKey . '.order', $demand->getOrder());
        }
        $demand->setAllowedStatusList([]);
        $demand->setRespectStoragePid(false);
        if ($demand instanceof EventDemand) {
            $demand->setIgnoreHideInListFlag(true);
            $demand->setMinStartTimestamp(null);
            if (!empty($reqOrder)) {
                $orderParts = explode(':', trim(strip_tags($reqOrder)));
                if (!empty($orderParts[0]) && in_array($orderParts[0], $demand->getValidSortProperties(), false)) {
                    $order = $orderParts[0] . ' ' . ((int)$orderParts[1] === 1 ? 'DESC' : 'ASC');
                    $sessionHandler->store($sessionKey . '.order', $order);
                    $demand->setOrder($order);
                }
            } else {
                $demand->setOrder('eventStart DESC');
            }
        } else {
            $demand->setOrder('crdate ASC');
        }
        if ($demand instanceof RegistrationDemand && !empty($this->settings['searchFields'])) {
            $searchFields = array();
            $tmpSearchFields = explode(',', $this->settings['searchFields']);
            foreach ($tmpSearchFields as $tmpField) {
                $searchFields[] = trim($tmpField);
            }
            $demand->setSearchFields($searchFields);
        }
    }

    /**
     * initialize notify action
     *
     * @return void
     */
    public function initializeNotifyAction()
    {
        if ($this->arguments->hasArgument('mailing')) {
            $propertyMapping = $this->arguments->getArgument('mailing')->getPropertyMappingConfiguration();
            for ($i = 1; $i < 7; $i++) {
                $propertyMapping->allowProperties('attachment' . $i);
                $propertyMapping->setTargetTypeForSubProperty('attachment' . $i, 'array');
            }
        }
    }

    /**
     * action notify
     *
     * @param \KDN\KdnEvents\Domain\Model\Mailing $mailing
     *
     * @return void
     */
    public function notifyAction(\KDN\KdnEvents\Domain\Model\Mailing $mailing)
    {
        $recipientIds = $mailing->getRecipients();
        $registrations = array();
        if (!empty($recipientIds)) {
            $registrations = $this->registrationRepository->findAllInIdList($recipientIds);
        }
        if (count($registrations) > 0) {
            /** @var \KDN\KdnEvents\Domain\Model\Registration $firstRegistration */
            $firstRegistration = $registrations->getFirst();
            $subject = trim(strip_tags($mailing->getSubject()));
            $body = trim($mailing->getBody());
            $isHtml = $mailing->isHtmlMode();
            if (!$isHtml) {
                $body = trim(strip_tags($body));
            }
            $pid = $firstRegistration->getEvent()->getPid();
            /** @var MailService $mailService */
            $mailService = $this->objectManager->get(MailService::class);
            $mailService->setMode('BE');
            $emailTemplate = $mailService->updateMailSettingsFromEmailTemplate(
                $this->settings['mail'],
                MailService::TEMPLATE_KEY_REGISTRATION_EMAIL_CUSTOMER,
                $pid
            );
            if (!empty($subject) && !empty($body)) {
                $attachments = [];
                $fileKey = 'tx_kdnevents_web_kdneventskdnevents';
                if (!empty($_FILES[$fileKey])) {
                    /** @var BasicFileUtility $basicFileFunctions */
                    $basicFileFunctions = GeneralUtility::makeInstance(BasicFileUtility::class);

                    for ($i = 1; $i < 10; $i++) {
                        if (!empty($_FILES[$fileKey]['name']['mailing']['attachment' . $i])) {
                            $fileName = $basicFileFunctions->getUniqueName(
                                $_FILES[$fileKey]['name']['mailing']['attachment' . $i],
                                GeneralUtility::getFileAbsFileName('typo3temp/')
                            );

                            GeneralUtility::upload_copy_move(
                                $_FILES[$fileKey]['tmp_name']['mailing']['attachment' . $i],
                                $fileName
                            );
                            $attachments[] = $fileName;
                        }
                    }
                }
                $mailService->addAttachmentsFromTemplate($emailTemplate, $attachments);
                $message = LocalizationUtility::translate('form-mailing.send.success', 'kdn_events');
                $this->addFlashMessage($message);
                //$contentObject = $this->configurationManager->getContentObject();
                foreach ($registrations as $registration) {
                    $mailService->sendRegistrationMail(
                        $registration,
                        $registration->getEmail(),
                        $this->settings['mail'],
                        null,
                        $subject,
                        $body,
                        $isHtml,
                        $attachments
                    );
                }
                foreach ($attachments as $absPath) {
                    unlink($absPath);
                }
                $this->forward('list');
            }
            /** @var MarkerService $markerService */
            $markerService = $this->objectManager->get(MarkerService::class);
            $markerService->setMode('BE');
            $markers = $markerService->getRegistrationMarkers($firstRegistration);
            $this->view->assignMultiple(array(
                'event' => $firstRegistration->getEvent(),
                'registrations' => $registrations,
                'mailing' => $mailing,
                'markers' => $markers,
                'senderFromEmail' => $this->settings['mail']['fromEmail'],
                'senderFromName' => $this->settings['mail']['fromName'],
            ));
        } else {
            $this->forward('list');
        }
    }
}
