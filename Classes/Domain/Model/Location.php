<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Location extends AbstractEntity
{

    /**
     * Name
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $name;

    /**
     * Location title
     *
     * @var string
     */
    protected $title;

    /**
     * street
     *
     * @var string
     */
    protected $street;

    /**
     * zip
     *
     * @var string
     */
    protected $zip;

    /**
     * room
     *
     * @var string
     */
    protected $room;

    /**
     * Description
     *
     * @var string|null
     */
    protected $description;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Location
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return Location
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     * @return Location
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param string $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription(): string
    {
        return (string) $this->description;
    }

    /**
     * Sets the description
     *
     * @param string|null $description
     * @return void
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
    }

    public function getMapsUrl()
    {
        $address = trim($this->getZip() . ' ' . $this->getName());
        if (!empty($this->getStreet())) {
            $address = $this->getStreet() . (empty($address)?'':',') . $address;
        }
        return 'https://www.google.com/maps/place/' . urlencode($address);
    }

    /**
     * Returns the full name of this location with the complete address and the title
     *
     * @param string $separator Separator for field values
     * @return string
     */
    public function getFullName($separator = "\n")
    {
        $fullName = '';
        if (!empty($this->getTitle())) {
            $fullName .= $this->getTitle();
        }
        if (!empty($this->getStreet())) {
            if (!empty($fullName)) {
                $fullName .= $separator;
            }
            $fullName .= $this->getStreet();
        }
        $zipTown = trim($this->getZip() . ' ' . $this->getName());
        if (!empty($zipTown)) {
            if (!empty($fullName)) {
                $fullName .= $separator;
            }
            $fullName .= $zipTown;
        }
        if ($room = $this->getRoom()) {
            if (!empty($fullName)) {
                $fullName .= $separator;
            }
            $fullName .= $room;
        }
        return $fullName;
    }
}
