<?php

namespace KDN\KdnEvents\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Facility
 */
class Time extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * Start time
     *
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $timeStart;

    /**
     * End time
     *
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $timeEnd;

    /**
     * Max. participants
     *
     * @var integer
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $maxParticipants;

    /**
     * Registrations
     *
     * @var integer
     */
    protected $registrations;

    /**
     * Event
     *
     * @var \KDN\KdnEvents\Domain\Model\Event
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $event;

    /**
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * @param \DateTime $timeStart
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;
    }

    /**
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * @param \DateTime $timeEnd
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;
    }

    /**
     * Returns the maxParticipants
     *
     * @return integer $maxParticipants
     */
    public function getMaxParticipants()
    {
        return $this->maxParticipants;
    }

    /**
     * Sets the maxParticipants
     *
     * @param integer $maxParticipants
     * @return void
     */
    public function setMaxParticipants($maxParticipants)
    {
        $this->maxParticipants = $maxParticipants;
    }

    /**
     * Returns the registrations
     *
     * @return integer $registrations
     */
    public function getRegistrations()
    {
        return $this->registrations;
    }

    /**
     * Sets the registrations
     *
     * @param integer $registrations
     * @return void
     */
    public function setRegistrations($registrations)
    {
        $this->registrations = $registrations;
    }

    /**
     * @return \KDN\KdnEvents\Domain\Model\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param \KDN\KdnEvents\Domain\Model\Event $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    public function belongsToEvent(Event $event)
    {
        return null !== $this->event && $this->event->getUid() === $event->getUid();
    }

    public function getTitle()
    {
        $eventStart = $this->getTimeStart();
        $eventEnd = $this->getTimeEnd();
        $startDate = date('d.m.Y', $eventStart->getTimestamp());
        $endDate = date('d.m.Y', $eventEnd->getTimestamp());
        $startTime = date('H:i', $eventStart->getTimestamp());
        $endTime = date('H:i', $eventEnd->getTimestamp());
        $timePeriod = $startDate . ' ' . $startTime;
        if ($endDate !== $startDate) {
            $timePeriod .= ' - ' . $endDate . ' ' . $endTime;

        } elseif ($startTime !== $endTime) {
            $timePeriod .= ' - ' . $endTime;
        }
        return $timePeriod;
    }

}