<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model;

/**
 * Demanded repository interface
 */
interface DemandInterface
{


    /**
     * Get search fields
     *
     * @return array
     */
    public function getSearchFields(): array;

    /**
     * Returns the search term
     *
     * @return string|null
     */
    public function getSearchTerm(): ?string;

    /**
     * Returns the search words
     *
     * @return array
     */
    public function getSearchWords(): array;

    /**
     * Get order
     *
     * @return string|null
     */
    public function getOrder(): ?string;

    /**
     * Get limit
     *
     * @return int|null
     */
    public function getLimit(): ?int;

    /**
     * Get offset
     *
     * @return int|null
     */
    public function getOffset(): ?int;

    /**
     * Returns the session key for this demand
     * @return string
     */
    public function getSessionKey(): string;

    /**
     * Returns true if the demand has been submitted; false otherwise
     *
     * @return bool
     */
    public function isSubmitted(): bool;

    /**
     * Sets the default sort field and sort order
     */
    public function setDefaultOrdering(): void;

    /**
     * Returns the valid sort properties
     *
     * @return array
     */
    public function getValidSortProperties(): array;

    /**
     * @return bool
     */
    public function isRespectStoragePid(): bool;
}
