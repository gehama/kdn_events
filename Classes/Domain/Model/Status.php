<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Status extends AbstractEntity {

	/**
	 * Name
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $name;

	/**
	 * Registration possible
	 *
	 * @var boolean
	 */
	protected $registrationPossible = FALSE;

	/**
	 * Registration possible
	 *
	 * @var boolean
	 */
	protected $waitlistOpen = FALSE;

	/**
	 * Event visible
	 *
	 * @var boolean
	 */
	protected $eventVisible = FALSE;

    /**
     * Event visible
     *
     * @var boolean
     */
    protected $showRegistrationInfo = FALSE;

	/**
	 * Registration text
	 *
	 * @var string
	 */
	protected $registrationText;

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the registrationPossible
	 *
	 * @return boolean $registrationPossible
	 */
	public function getRegistrationPossible() {
		return $this->registrationPossible;
	}

	/**
	 * Sets the registrationPossible
	 *
	 * @param boolean $registrationPossible
	 * @return void
	 */
	public function setRegistrationPossible($registrationPossible) {
		$this->registrationPossible = $registrationPossible;
	}

	/**
	 * Returns the boolean state of registrationPossible
	 *
	 * @return boolean
	 */
	public function isRegistrationPossible() {
		return $this->getRegistrationPossible();
	}

	/**
	 * Returns the waitlistOpen
	 *
	 * @return boolean
	 */
	public function getWaitlistOpen()
	{
		return (bool) $this->waitlistOpen;
	}

	/**
	 * Sets the waitlistOpen
	 *
	 * @param boolean $waitlistOpen
	 */
	public function setWaitlistOpen($waitlistOpen)
	{
		$this->waitlistOpen = (bool) $waitlistOpen;
	}

	/**
	 * Returns the boolean state of waitlistOpen
	 *
	 * @return boolean
	 */
	public function isWaitlistOpen(): bool
    {
		return (bool) $this->getWaitlistOpen();
	}

	/**
	 * Returns the eventVisible
	 *
	 * @return boolean $eventVisible
	 */
	public function getEventVisible() {
		return $this->eventVisible;
	}

	/**
	 * Sets the eventVisible
	 *
	 * @param boolean $eventVisible
	 * @return void
	 */
	public function setEventVisible($eventVisible) {
		$this->eventVisible = $eventVisible;
	}

	/**
	 * Returns the boolean state of eventVisible
	 *
	 * @return boolean
	 */
	public function isEventVisible(): bool
    {
		return $this->getEventVisible();
	}

    /**
     * @return bool
     */
    public function getShowRegistrationInfo()
    {
        return $this->showRegistrationInfo;
    }

    /**
     * @param bool $showRegistrationInfo
     */
    public function setShowRegistrationInfo($showRegistrationInfo)
    {
        $this->showRegistrationInfo = $showRegistrationInfo;
    }

    /**
     * @return bool
     */
    public function isShowRegistrationInfo()
    {
        return $this->getShowRegistrationInfo();
    }

	/**
	 * Returns the registrationText
	 *
	 * @return string $registrationText
	 */
	public function getRegistrationText() {
		return $this->registrationText;
	}

	/**
	 * Sets the registrationText
	 *
	 * @param string $registrationText
	 * @return void
	 */
	public function setRegistrationText($registrationText) {
		$this->registrationText = $registrationText;
	}

}
