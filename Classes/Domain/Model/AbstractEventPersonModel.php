<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model;

use DateTime;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AbstractEventPersonModel extends AbstractPersonModel
{
    public const STATUS_CREATED = 1;
    public const STATUS_CANCELLED = 9;
    public const STATUS_COMPLETED = 10;

    /**
     * Event
     *
     * @var \KDN\KdnEvents\Domain\Model\Event
     */
    protected $event;

    /**
     * Event time
     *
     * @var \KDN\KdnEvents\Domain\Model\Time
     */
    protected $eventTime;

    /**
     * Status
     *
     * @var integer
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $status = self::STATUS_CREATED;

    /**
     * Registration hash
     *
     * @var string
     */
    protected $hash;

    /**
     * internal remarks
     *
     * @var string|null
     */
    protected $internalRemarks;

    /**
     * Returns the event
     *
     * @return \KDN\KdnEvents\Domain\Model\Event $event
     */
    public function getEvent(): ?Event
    {
        return $this->event;
    }

    /**
     * Sets the event
     *
     * @param \KDN\KdnEvents\Domain\Model\Event $event
     * @return void
     */
    public function setEvent(\KDN\KdnEvents\Domain\Model\Event $event): void
    {
        $this->event = $event;
    }

    /**
     * @return \KDN\KdnEvents\Domain\Model\Time
     */
    public function getEventTime()
    {
        return $this->eventTime;
    }

    /**
     * @param \KDN\KdnEvents\Domain\Model\Time $eventTime
     */
    public function setEventTime(\KDN\KdnEvents\Domain\Model\Time $eventTime)
    {
        $this->eventTime = $eventTime;
    }

    /**
     * @return DateTime
     */
    public function getEventStart(): ?DateTime
    {
        $event = $this->getEvent();
        if (null !== $event) {
            $isSeriesEvent = $event->isSeriesEvent();
            if ($isSeriesEvent && null !== $this->eventTime && $this->eventTime->belongsToEvent($event)) {
                return $this->eventTime->getTimeStart();
            }
            return $event->getEventStart();
        }
        return null;
    }

    /**
     * @return DateTime|null
     */
    public function getEventEnd(): ?DateTime
    {
        $event = $this->getEvent();
        if (null !== $event) {
            $isSeriesEvent = $event->isSeriesEvent();
            if ($isSeriesEvent && null !== $this->eventTime && $this->eventTime->belongsToEvent($event)) {
                return $this->eventTime->getTimeEnd();
            }
            return $event->getEventEnd();
        }
        return null;
    }

    /**
     * Returns the status
     *
     * @return integer $status
     */
    public function getStatus()
    {
        return $this->status ?? self::STATUS_CREATED;
    }

    /**
     * Sets the status
     *
     * @param integer $status
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Returns the hash
     *
     * @return string $hash
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Sets the hash
     *
     * @param string $hash
     * @return void
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string|null
     */
    public function getInternalRemarks()
    {
        return $this->internalRemarks;
    }

    /**
     * @param string|null $internalRemarks
     * @return self
     */
    public function setInternalRemarks($internalRemarks)
    {
        $this->internalRemarks = $internalRemarks;
        return $this;
    }
}
