<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Invitation extends AbstractEventPersonModel
{
    public const STATUS_REGISTERED = 2;
    public const STATUS_PARTICIPATED = 3;

    /**
     * @var FrontendUser|null
     */
    protected $invitedBy;

    /**
     * @var Registration|null
     */
    protected $registration;

    /**
     * message to prospect
     *
     * @var string|null
     */
    protected $message;

    /**
     * @return FrontendUser|null
     */
    public function getInvitedBy(): ?FrontendUser
    {
        return $this->invitedBy;
    }

    /**
     * @param FrontendUser|null $invitedBy
     */
    public function setInvitedBy(?FrontendUser $invitedBy): void
    {
        $this->invitedBy = $invitedBy;
    }

    /**
     * @return Registration|null
     */
    public function getRegistration(): ?Registration
    {
        return $this->registration;
    }

    /**
     * @param Registration|null $registration
     */
    public function setRegistration(?Registration $registration): void
    {
        $this->registration = $registration;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

}
