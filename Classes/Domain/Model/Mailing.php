<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Mailing extends AbstractEntity
{

    /**
     * @var array
     */
    protected $recipients;

    /**
     * @var bool
     */
    protected $htmlMode;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var string
     */
    protected $body;

    /**
     * @var string
     */
    protected $attachment1;

    /**
     * @var string
     */
    protected $attachment2;

    /**
     * @var string
     */
    protected $attachment3;

    /**
     * @var string
     */
    protected $attachment4;

    /**
     * @var string
     */
    protected $attachment5;

    /**
     * @var string
     */
    protected $attachment6;


    public function __construct()
    {
        $this->recipients = array();
    }

    /**
     * @return array
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * @param array $recipients
     */
    public function setRecipients($recipients)
    {
        $this->recipients = $recipients;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return boolean
     */
    public function getHtmlMode()
    {
        return (bool)$this->htmlMode;
    }

    /**
     * @return boolean
     */
    public function isHtmlMode()
    {
        return (bool)$this->htmlMode;
    }

    /**
     * @param boolean $htmlMode
     */
    public function setHtmlMode($htmlMode)
    {
        $this->htmlMode = $htmlMode;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getAttachment1()
    {
        return $this->attachment1;
    }

    /**
     * @param string $attachment1
     * @return Mailing
     */
    public function setAttachment1($attachment1)
    {
        $this->attachment1 = (string)$attachment1;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttachment2()
    {
        return $this->attachment2;
    }

    /**
     * @param string $attachment2
     * @return Mailing
     */
    public function setAttachment2($attachment2)
    {
        $this->attachment2 = $attachment2;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttachment3()
    {
        return $this->attachment3;
    }

    /**
     * @param string $attachment3
     * @return Mailing
     */
    public function setAttachment3($attachment3)
    {
        $this->attachment3 = $attachment3;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttachment4()
    {
        return $this->attachment4;
    }

    /**
     * @param string $attachment4
     * @return Mailing
     */
    public function setAttachment4($attachment4)
    {
        $this->attachment4 = $attachment4;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttachment5()
    {
        return $this->attachment5;
    }

    /**
     * @param string $attachment5
     * @return Mailing
     */
    public function setAttachment5($attachment5)
    {
        $this->attachment5 = $attachment5;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttachment6()
    {
        return $this->attachment6;
    }

    /**
     * @param string $attachment6
     * @return Mailing
     */
    public function setAttachment6($attachment6)
    {
        $this->attachment6 = $attachment6;
        return $this;
    }

}
