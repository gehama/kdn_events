<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model;

use DateTime;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Registration extends AbstractEventPersonModel
{
    public const STATUS_REGISTERED = 1;
    public const STATUS_PARTICIPATED = 2;
    public const STATUS_NOT_PRESENT = 3;
    public const STATUS_WAITLIST = 4;
    public const STATUS_CANCELLED = 9;

    public const REMINDER_STATUS_NOT_SENT = 0;
    public const REMINDER_STATUS_SENT = 1;
    public const REMINDER_STATUS_DISABLED = 2;

    /**
     * @var FrontendUser|null
     */
    protected $user;

    /**
     * Birth date
     *
     * @var DateTime
     */
    protected $birthDate;

    /**
     * Street
     *
     * @var string
     */
    protected $street;

    /**
     * Zip code
     *
     * @var string
     */
    protected $zipcode;

    /**
     * Town
     *
     * @var string
     */
    protected $town;

    /**
     * occupation
     *
     * @var string
     */
    protected $occupation;

    /**
     * work experience
     *
     * @var string
     */
    protected $workExperience;

    /**
     * organisation
     *
     * @var string
     */
    protected $organisation;

    /**
     * organisation street
     *
     * @var string
     */
    protected $organisationStreet;

    /**
     * organisation zip code
     *
     * @var string
     */
    protected $organisationZipcode;

    /**
     * organisation town
     *
     * @var string
     */
    protected $organisationTown;

    /**
     * organisation phone
     *
     * @var string
     */
    protected $organisationPhone;

    /**
     * organisation email
     *
     * @var string
     */
    protected $organisationEmail;

    /**
     * customer message
     *
     * @var string
     */
    protected $customerMessage;

    /**
     * terms and conditions
     *
     * @var boolean
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     * @TYPO3\CMS\Extbase\Annotation\Validate("Boolean", options={"is": true})
     */
    protected $agreeToTerms = false;

    /**
     * Subscribe to newsletter
     *
     * @var boolean
     */
    protected $newsletter = false;

    /**
     * privacy terms and conditions
     *
     * @var boolean
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     * @TYPO3\CMS\Extbase\Annotation\Validate("Boolean", options={"is": true})
     */
    protected $privacyTerms = false;

    /**
     * Facility
     *
     * @var \KDN\KdnEvents\Domain\Model\Facility
     */
    protected $facility;

    /**
     * Reminder status
     *
     * @var int
     */
    protected $reminderStatus = self::REMINDER_STATUS_NOT_SENT;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KDN\KdnEvents\Domain\Model\WorkArea>
     */
    protected $workAreas;

    /**
     * Registration price
     *
     * @var string|null
     */
    protected $price;

    /**
     * Bill id (currently not used internally, but can be used by extensions)
     *
     * @var int|null
     */
    protected $billId;

    /**
     * Member type (can be used for price calculation)
     *
     * @var int|null
     */
    protected $memberType;

    /**
     * @var Invitation|null
     */
    protected $invitation;

    /**
     * Initialize relations
     */
    public function __construct()
    {
        $this->workAreas = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @return FrontendUser|null
     */
    public function getUser(): ?FrontendUser
    {
        return $this->user;
    }

    /**
     * @param FrontendUser|null $user
     */
    public function setUser(?FrontendUser $user): void
    {
        $this->user = $user;
    }

    /**
     * Returns the agreeToTerms
     *
     * @return boolean $agreeToTerms
     */
    public function getAgreeToTerms()
    {
        return (bool)$this->agreeToTerms;
    }

    /**
     * Sets the agreeToTerms
     *
     * @param boolean $agreeToTerms
     * @return void
     */
    public function setAgreeToTerms($agreeToTerms)
    {
        $this->agreeToTerms = (bool)$agreeToTerms;
    }

    /**
     * Returns the boolean state of agreeToTerms
     *
     * @return boolean
     */
    public function isAgreeToTerms()
    {
        return (bool)$this->getAgreeToTerms();
    }

    /**
     * Returns the newsletter subscription flag
     * @return bool
     */
    public function getNewsletter(): bool
    {
        return (bool) $this->newsletter;
    }

    /**
     * Returns the newsletter subscription flag
     * @return bool
     */
    public function isNewsletter(): bool
    {
        return (bool) $this->newsletter;
    }

    /**
     * Sets the newsletter subscription flag
     * @param bool $newsletter
     */
    public function setNewsletter($newsletter): void
    {
        $this->newsletter = (bool) $newsletter;
    }

    /**
     * Returns the privacy terms
     *
     * @return boolean $privacyTerms
     */
    public function getPrivacyTerms()
    {
        return (bool)$this->privacyTerms;
    }

    /**
     * Returns the privacy terms
     *
     * @return bool
     */
    public function isPrivacyTerms()
    {
        return $this->privacyTerms;
    }

    /**
     * @param bool $privacyTerms
     * @return Registration
     */
    public function setPrivacyTerms(bool $privacyTerms)
    {
        $this->privacyTerms = $privacyTerms;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param DateTime $birthDate
     * @return Registration
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return Registration
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     * @return Registration
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @param string $town
     * @return Registration
     */
    public function setTown($town)
    {
        $this->town = $town;
        return $this;
    }

    /**
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param string $occupation
     * @return Registration
     */
    public function setOccupation($occupation)
    {
        $this->occupation = $occupation;
        return $this;
    }

    /**
     * @return string
     */
    public function getWorkExperience()
    {
        return $this->workExperience;
    }

    /**
     * @param string $workExperience
     * @return Registration
     */
    public function setWorkExperience($workExperience)
    {
        $this->workExperience = $workExperience;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * @param string $organisation
     * @return Registration
     */
    public function setOrganisation($organisation)
    {
        $this->organisation = $organisation;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisationStreet()
    {
        return $this->organisationStreet;
    }

    /**
     * @param string $organisationStreet
     * @return Registration
     */
    public function setOrganisationStreet($organisationStreet)
    {
        $this->organisationStreet = $organisationStreet;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisationZipcode()
    {
        return $this->organisationZipcode;
    }

    /**
     * @param string $organisationZipcode
     * @return Registration
     */
    public function setOrganisationZipcode($organisationZipcode)
    {
        $this->organisationZipcode = $organisationZipcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisationTown()
    {
        return $this->organisationTown;
    }

    /**
     * @param string $organisationTown
     * @return Registration
     */
    public function setOrganisationTown($organisationTown)
    {
        $this->organisationTown = $organisationTown;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisationPhone()
    {
        return $this->organisationPhone;
    }

    /**
     * @param string $organisationPhone
     * @return Registration
     */
    public function setOrganisationPhone($organisationPhone)
    {
        $this->organisationPhone = $organisationPhone;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisationEmail()
    {
        return $this->organisationEmail;
    }

    /**
     * @param string $organisationEmail
     * @return Registration
     */
    public function setOrganisationEmail($organisationEmail)
    {
        $this->organisationEmail = $organisationEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerMessage()
    {
        return $this->customerMessage;
    }

    /**
     * @param string $customerMessage
     * @return Registration
     */
    public function setCustomerMessage($customerMessage)
    {
        $this->customerMessage = $customerMessage;
        return $this;
    }

    /**
     * Returns the event
     *
     * @return \KDN\KdnEvents\Domain\Model\Facility|null $facility
     */
    public function getFacility()
    {
        return $this->facility;
    }

    /**
     * Sets the facility
     *
     * @param \KDN\KdnEvents\Domain\Model\Facility|null $facility
     * @return void
     */
    public function setFacility(?\KDN\KdnEvents\Domain\Model\Facility $facility)
    {
        $this->facility = $facility;
    }

    /**
     * @return int
     */
    public function getReminderStatus()
    {
        return (int)$this->reminderStatus;
    }

    /**
     * @param int $reminderStatus
     */
    public function setReminderStatus($reminderStatus)
    {
        $this->reminderStatus = (int)$reminderStatus;
    }

    /**
     * @return bool true if reminder has not been sent and is not disabled
     */
    public function isReminderEnabled()
    {
        return (int)$this->reminderStatus === self::REMINDER_STATUS_NOT_SENT;
    }

    /**
     * Get work areas
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KDN\KdnEvents\Domain\Model\WorkArea>
     */
    public function getWorkAreas()
    {
        return $this->workAreas;
    }

    /**
     * Set work areas
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $workAreas
     * @return void
     */
    public function setWorkAreas($workAreas)
    {
        $this->workAreas = $workAreas;
    }

    /**
     * Adds a work area to the work area list.
     *
     * @param WorkArea $workArea
     * @return void
     */
    public function addWorkArea(WorkArea $workArea)
    {
        $this->workAreas->attach($workArea);
    }

    /**
     * Removes a work area
     *
     * @param WorkArea $workArea
     * @return void
     */
    public function removeWorkArea(WorkArea $workArea)
    {
        $this->workAreas->detach($workArea);
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @param string|null $price
     */
    public function setPrice(?string $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int|null
     */
    public function getBillId(): ?int
    {
        return $this->billId;
    }

    /**
     * @param int|null $billId
     */
    public function setBillId(?int $billId): void
    {
        $this->billId = $billId;
    }

    /**
     * @return int|null
     */
    public function getMemberType(): ?int
    {
        return $this->memberType;
    }

    /**
     * @param int|null $memberType
     */
    public function setMemberType(?int $memberType): void
    {
        $this->memberType = $memberType;
    }

    /**
     * @return Invitation|null
     */
    public function getInvitation(): ?Invitation
    {
        return $this->invitation;
    }

    /**
     * @param Invitation|null $invitation
     */
    public function setInvitation(?Invitation $invitation): void
    {
        $this->invitation = $invitation;
        // Always override the person data with the invitation data
        if (null !== $invitation) {
            $this->setFirstName($invitation->getFirstName());
            $this->setEmail($invitation->getEmail());
            $this->setLastName($invitation->getLastName());
            $this->setSalutation($invitation->getSalutation());
        }
    }

    /**
     * @return bool
     */
    public function hasActiveStatus(): bool
    {
        return in_array($this->getStatus(), [
            self::STATUS_REGISTERED,
            self::STATUS_PARTICIPATED,
            self::STATUS_WAITLIST,
            self::STATUS_NOT_PRESENT,
        ], true);
    }

    /**
     * Check if valid address is given (required for billing)
     * @return bool
     */
    public function hasValidAddress(): bool
    {
        return !empty($this->street) && !empty($this->zipcode) && !empty($this->town);
    }

    public function getCanBeCancelled()
    {
        if ($this->getHash() && $this->hasActiveStatus() && (null !== $event = $this->getEvent())) {
            $regEndDate = $event->getRegistrationEnd();
            if (null === $regEndDate) {
                $regEndDate = $event->getEventStart();
            }
            $limitDays = $event->getRegistrationCancelLimit();
            $maxTstamp = 0;
            // Check end of cancel limit has been reached; if not the registration can be cancelled
            if (null !== $regEndDate) {
                $maxTstamp = $regEndDate->getTimestamp();
                if ($limitDays > 0) {
                    $maxTstamp = strtotime('-' . $limitDays . 'days ', $regEndDate->getTimestamp());
                }
            }
            return $maxTstamp > time();
        }
        return false;
    }
}
