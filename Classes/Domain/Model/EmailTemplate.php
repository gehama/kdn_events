<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *  
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class EmailTemplate extends AbstractEntity {

	/**
	 * Template key
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $templateKey;

	/**
	 * Description
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $description;

	/**
	 * Sender email
	 *
	 * @var string
	 */
	protected $senderEmail;

	/**
	 * Sender name
	 *
	 * @var string
	 */
	protected $senderName;

	/**
	 * Administrator email
	 *
	 * @var string
	 */
	protected $adminEmail;

	/**
	 * ReplyTo email
	 *
	 * @var string
	 */
	protected $replyToEmail;

	/**
	 * Email subject
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $subject;

	/**
	 * Email body
	 *
	 * @var string
	 * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
	 */
	protected $body;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $attachments;

    /**
     * Initialize attachments
     */
    public function __construct()
    {
        $this->attachments = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

	/**
	 * Returns the templateKey
	 *
	 * @return string $templateKey
	 */
	public function getTemplateKey() {
		return $this->templateKey;
	}

	/**
	 * Sets the templateKey
	 *
	 * @param string $templateKey
	 * @return void
	 */
	public function setTemplateKey($templateKey) {
		$this->templateKey = $templateKey;
	}

	/**
	 * Returns the description
	 *
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 *
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getSenderEmail()
	{
		return $this->senderEmail;
	}

	/**
	 * @param string $senderEmail
	 * @return EmailTemplate
	 */
	public function setSenderEmail($senderEmail)
	{
		$this->senderEmail = $senderEmail;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSenderName()
	{
		return $this->senderName;
	}

	/**
	 * @param string $senderName
	 * @return EmailTemplate
	 */
	public function setSenderName($senderName)
	{
		$this->senderName = $senderName;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAdminEmail()
	{
		return $this->adminEmail;
	}

	/**
	 * @param string $adminEmail
	 * @return EmailTemplate
	 */
	public function setAdminEmail($adminEmail)
	{
		$this->adminEmail = $adminEmail;
		return $this;
	}

	/**
	 * Returns the subject
	 *
	 * @return string $subject
	 */
	public function getSubject() {
		return $this->subject;
	}

	/**
	 * Sets the subject
	 *
	 * @param string $subject
	 * @return void
	 */
	public function setSubject($subject) {
		$this->subject = $subject;
	}

	/**
	 * Returns the body
	 *
	 * @return string $body
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * Sets the body
	 *
	 * @param string $body
	 * @return void
	 */
	public function setBody($body) {
		$this->body = $body;
	}

	/**
	 * @return string
	 */
	public function getReplyToEmail()
	{
		return $this->replyToEmail;
	}

	/**
	 * @param string $replyToEmail
	 * @return EmailTemplate
	 */
	public function setReplyToEmail($replyToEmail)
	{
		$this->replyToEmail = $replyToEmail;
		return $this;
	}

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $attachments
     */
    public function setAttachments($attachments)
    {
        $this->attachments = $attachments;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Add attachment
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $attachment
     */
    public function addAttachment(\TYPO3\CMS\Extbase\Domain\Model\FileReference $attachment)
    {
        $this->attachments->attach($attachment);
    }

    /**
     * Remove attachment
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $attachment
     */
    public function removeAttachment(\TYPO3\CMS\Extbase\Domain\Model\FileReference $attachment)
    {
        $this->attachments->detach($attachment);
    }

}
