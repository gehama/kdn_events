<?php

namespace KDN\KdnEvents\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * WorkArea
 */
class WorkArea extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * Description
     *
     * @var string|null
     */
    protected $description;

    /**
     * Event
     *
     * @var \KDN\KdnEvents\Domain\Model\Event|null
     */
    protected $event;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription(): string
    {
        return (string) $this->description;
    }

    /**
     * Sets the description
     *
     * @param string|null $description
     * @return void
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
    }

    /**
     * Returns the event
     *
     * @return \KDN\KdnEvents\Domain\Model\Event|null $event
     */
    public function getEvent(): ?Event
    {
        return $this->event;
    }

    /**
     * Sets the event
     *
     * @param \KDN\KdnEvents\Domain\Model\Event|null $event
     * @return void
     */
    public function setEvent(?\KDN\KdnEvents\Domain\Model\Event $event): void
    {
        $this->event = $event;
    }

}