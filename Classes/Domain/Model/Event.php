<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\FileReference;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Event extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var \DateTime
     */
    protected $tstamp;

    /**
     * Event type
     *
     * @var int|null
     */
    protected $eventType;

    /**
     * Title
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $title;

    /**
     * Slug
     *
     * @var string
     */
    protected $slug;

    /**
     * Subtitle
     *
     * @var string
     */
    protected $subtitle;

    /**
     * Description
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $description;

    /**
     * Start date
     *
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $eventStart;

    /**
     * End date
     *
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $eventEnd;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KDN\KdnEvents\Domain\Model\Time>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $eventTimes;

    /**
     * Is series event
     *
     * @var boolean
     */
    protected $isSeriesEvent = false;

    /**
     * Is registration disabled
     *
     * @var boolean
     */
    protected $registrationDisabled = false;

    /**
     * Registration start
     *
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $registrationStart;

    /**
     * Registration end
     *
     * @var \DateTime
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $registrationEnd;

    /**
     * Number of days before event until which registrations can be cancelled
     *
     * @var int
     */
    protected $registrationCancelLimit = 0;

    /**
     * Max. participants
     *
     * @var int
     * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
     */
    protected $maxParticipants;

    /**
     * Registrations
     *
     * @var int
     */
    protected $registrations;

    /**
     * Waitlist available?
     *
     * @var boolean
     */
    protected $waitlistAvailable = false;

    /**
     * Published
     *
     * @var boolean
     */
    protected $published = false;

    /**
     * Hide in list view
     *
     * @var boolean
     */
    protected $hideInList = false;

    /**
     * is top event
     *
     * @var boolean
     */
    protected $isTopEvent = false;

    /**
     * is online event
     *
     * @var boolean
     */
    protected $isOnlineEvent = false;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KDN\KdnEvents\Domain\Model\Registration>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $registrationList;

    /**
     * Status
     *
     * @var \KDN\KdnEvents\Domain\Model\Status
     */
    protected $status;

    /**
     * Organizer
     *
     * @var \KDN\KdnEvents\Domain\Model\Organizer
     */
    protected $organizer;

    /**
     * Location
     *
     * @var \KDN\KdnEvents\Domain\Model\Location
     */
    protected $location;

    /**
     * Custom location
     *
     * @var string
     */
    protected $customLocation;

    /**
     * internal remarks
     *
     * @var string
     */
    protected $internalRemarks;

    /**
     * video conference url
     *
     * @var string
     */
    protected $videoConferenceUrl;

    /**
     * video conference test url
     *
     * @var string
     */
    protected $videoConferenceTestUrl;

    /**
     * video conference comment
     *
     * @var string
     */
    protected $videoConferenceComment;

    /**
     * string
     *
     * @var string
     */
    protected $enableOptionalFields;

    /**
     * string
     *
     * @var string
     */
    protected $enableRequiredFields;

    /**
     * @var string|null
     */
    protected $enabledWorkAreas;

    /**
     * terms
     *
     * @var string|null
     */
    protected $terms;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KDN\KdnEvents\Domain\Model\Category>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $eventCategories;

    /**
     * Fal media items
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $media;

    /**
     * Fal related files
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $relatedFiles;

    /**
     * Contact email
     *
     * @var string|null
     */
    protected $contact;

    /**
     * Tutor
     *
     * @var string|null
     */
    protected $tutor;

    /**
     * Default price
     *
     * @var string
     */
    protected $price;

    /**
     * price member type 2
     *
     * @var string
     */
    protected $priceMemberType2;

    /**
     * price member type 3
     *
     * @var string
     */
    protected $priceMemberType3;

    /**
     * price member type 4
     *
     * @var string
     */
    protected $priceMemberType4;

    /**
     * price member type 5
     *
     * @var string
     */
    protected $priceMemberType5;

    /**
     * tax type
     *
     * @var int
     */
    protected $taxType;

    /**
     * bill title
     *
     * @var string
     */
    protected $billTitle;

    /**
     * bill payment info
     *
     * @var string
     */
    protected $billPaymentInfo;

    /**
     * Initialize relations
     */
    public function __construct()
    {
        $this->eventCategories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->eventTimes = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->media = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->relatedFiles = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->registrationList = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @return int
     */
    public function getEventType(): int
    {
        return (int) $this->eventType;
    }

    /**
     * @param int $eventType
     */
    public function setEventType(int $eventType): void
    {
        $this->eventType = $eventType;
    }

    /**
     * Setter for uid. (Only used for testing)
     *
     * @param int $uid
     * @internal
     */
    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Set time stamp
     *
     * @param \DateTime $tstamp time stamp
     */
    public function setTstamp($tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     *
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the eventStart
     *
     * @return \DateTime $eventStart
     */
    public function getEventStart()
    {
        if (null === $this->eventStart) {
            $eventTimes = $this->getEventTimes();
            $dateTime = null;
            /** @var Time $time */
            foreach ($eventTimes as $time) {
                if (null === $dateTime || $dateTime > $time->getTimeStart()) {
                    $dateTime = $time->getTimeStart();
                }
            }
            $this->eventStart = $dateTime;
        }
        return $this->eventStart;
    }

    /**
     * Sets the eventStart
     *
     * @param \DateTime $eventStart
     * @return void
     */
    public function setEventStart($eventStart)
    {
        $this->eventStart = $eventStart;
    }

    /**
     * Returns the eventEnd
     *
     * @return \DateTime $eventEnd
     */
    public function getEventEnd()
    {
        if (null === $this->eventEnd) {
            $eventTimes = $this->getEventTimes();
            $dateTime = null;
            /** @var Time $time */
            foreach ($eventTimes as $time) {
                if (null === $dateTime || $dateTime < $time->getTimeEnd()) {
                    $dateTime = $time->getTimeEnd();
                }
            }
            $this->eventEnd = $dateTime;
        }
        return $this->eventEnd;
    }

    /**
     * Sets the eventEnd
     *
     * @param \DateTime $eventEnd
     * @return void
     */
    public function setEventEnd($eventEnd)
    {
        $this->eventEnd = $eventEnd;
    }

    /**
     * Returns the event times
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getEventTimes()
    {
        return $this->eventTimes;
    }

    /**
     * Returns the first event time that is not over yet
     *
     * @return Time|null
     */
    public function getFirstActiveEventTime()
    {
        $time = null;
        $now = new \DateTime();
        foreach ($this->eventTimes as $eventTime) {
            /** @var Time $eventTime */
            if ($eventTime->getTimeStart() > $now) {
                $time = $eventTime;
                break;
            }
        }
        return $time;
    }

    /**
     * Returns the first event time that is not over yet
     *
     * @return Time[]
     */
    public function getFutureEventTimes()
    {
        $times = [];
        $now = new \DateTime();
        foreach ($this->eventTimes as $eventTime) {
            /** @var Time $eventTime */
            if ($eventTime->getTimeStart() > $now) {
                $times[] = $eventTime;
            }
        }
        return $times;
    }

    /**
     * Sets the event times
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $eventTimes
     */
    public function setEventTimes($eventTimes)
    {
        $this->eventTimes = $eventTimes;
    }

    /**
     * @return bool
     */
    public function isSeriesEvent()
    {
        return $this->isSeriesEvent;
    }

    /**
     * @return bool
     */
    public function getIsSeriesEvent()
    {
        return $this->isSeriesEvent;
    }

    /**
     * @param bool $isSeriesEvent
     */
    public function setIsSeriesEvent($isSeriesEvent)
    {
        $this->isSeriesEvent = $isSeriesEvent;
    }

    /**
     * @return bool
     */
    public function isRegistrationDisabled(): bool
    {
        return (bool)$this->registrationDisabled;
    }

    /**
     * @param bool $registrationDisabled
     */
    public function setRegistrationDisabled(bool $registrationDisabled): void
    {
        $this->registrationDisabled = $registrationDisabled;
    }

    /**
     * Returns the registrationStart
     *
     * @return \DateTime $registrationStart
     */
    public function getRegistrationStart()
    {
        return $this->registrationStart;
    }

    /**
     * Returns true if the registration period hast started
     *
     * @return bool
     */
    public function getHasRegistrationStarted()
    {
        return null !== $this->registrationStart && $this->registrationStart->getTimestamp() < time();
    }

    /**
     * Sets the registrationStart
     *
     * @param \DateTime $registrationStart
     * @return void
     */
    public function setRegistrationStart($registrationStart)
    {
        $this->registrationStart = $registrationStart;
    }

    /**
     * Returns the registrationEnd
     *
     * @return \DateTime $registrationEnd
     */
    public function getRegistrationEnd()
    {
        return $this->registrationEnd;
    }

    /**
     * Sets the registrationEnd
     *
     * @param \DateTime $registrationEnd
     * @return void
     */
    public function setRegistrationEnd($registrationEnd)
    {
        $this->registrationEnd = $registrationEnd;
    }

    /**
     * @return int
     */
    public function getRegistrationCancelLimit(): int
    {
        return $this->registrationCancelLimit;
    }

    /**
     * @param int $registrationCancelLimit
     */
    public function setRegistrationCancelLimit(int $registrationCancelLimit): void
    {
        $this->registrationCancelLimit = $registrationCancelLimit;
    }

    /**
     * Returns the maxParticipants
     *
     * @return integer $maxParticipants
     */
    public function getMaxParticipants()
    {
        return $this->maxParticipants;
    }

    /**
     * Sets the maxParticipants
     *
     * @param integer $maxParticipants
     * @return void
     */
    public function setMaxParticipants($maxParticipants)
    {
        $this->maxParticipants = $maxParticipants;
    }

    /**
     * Returns the registrations
     *
     * @return integer $registrations
     */
    public function getRegistrations()
    {
        return $this->registrations;
    }

    /**
     * Sets the registrations
     *
     * @param integer $registrations
     * @return void
     */
    public function setRegistrations($registrations)
    {
        $this->registrations = $registrations;
    }

    /**
     * Returns whether the wait list is available
     *
     * @return boolean $waitlistAvailable
     */
    public function getWaitlistAvailable()
    {
        return $this->waitlistAvailable;
    }

    /**
     * Sets whether the wait list is available
     *
     * @param boolean $waitlistAvailable
     * @return Event
     */
    public function setWaitlistAvailable($waitlistAvailable)
    {
        $this->waitlistAvailable = $waitlistAvailable;
        return $this;
    }

    /**
     * Returns whether the wait list is available
     *
     * @return boolean $waitlistAvailable
     */
    public function isWaitlistAvailable()
    {
        return $this->waitlistAvailable;
    }

    /**
     * Returns the published flag
     *
     * @return boolean $published
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Sets the published flag
     *
     * @param boolean $published
     * @return void
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * Returns the boolean state of published
     *
     * @return boolean
     */
    public function isPublished()
    {
        return $this->getPublished();
    }

    /**
     * Returns the boolean state of hideInList
     *
     * @return boolean
     */
    public function getHideInList()
    {
        return $this->isHideInList();
    }

    /**
     * @return bool
     */
    public function isHideInList(): bool
    {
        return $this->hideInList;
    }

    /**
     * @param bool $hideInList
     */
    public function setHideInList(bool $hideInList): void
    {
        $this->hideInList = $hideInList;
    }

    /**
     * Returns the boolean state of isTopEvent
     *
     * @return boolean
     */
    public function getIsTopEvent()
    {
        return $this->isTopEvent();
    }

    /**
     * @return bool
     */
    public function isTopEvent(): bool
    {
        return $this->isTopEvent;
    }

    /**
     * @param bool $isTopEvent
     */
    public function setIsTopEvent(bool $isTopEvent): void
    {
        $this->isTopEvent = $isTopEvent;
    }

    /**
     * Returns the boolean state of isOnlineEvent
     *
     * @return boolean
     */
    public function getIsOnlineEvent()
    {
        return $this->isOnlineEvent();
    }

    /**
     * @return bool
     */
    public function isOnlineEvent(): bool
    {
        return $this->isOnlineEvent;
    }

    /**
     * @param bool $isOnlineEvent
     */
    public function setIsOnlineEvent(bool $isOnlineEvent): void
    {
        $this->isOnlineEvent = $isOnlineEvent;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getRegistrationList()
    {
        return $this->registrationList;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $registrationList
     */
    public function setRegistrationList($registrationList)
    {
        $this->registrationList = $registrationList;
    }

    /**
     * Returns the status
     *
     * @return \KDN\KdnEvents\Domain\Model\Status $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status
     *
     * @param \KDN\KdnEvents\Domain\Model\Status $status
     * @return void
     */
    public function setStatus(\KDN\KdnEvents\Domain\Model\Status $status)
    {
        $this->status = $status;
    }

    /**
     * Returns the organizer
     *
     * @return \KDN\KdnEvents\Domain\Model\Organizer $organizer
     */
    public function getOrganizer()
    {
        return $this->organizer;
    }

    /**
     * Sets the organizer
     *
     * @param \KDN\KdnEvents\Domain\Model\Organizer $organizer
     * @return void
     */
    public function setOrganizer(\KDN\KdnEvents\Domain\Model\Organizer $organizer)
    {
        $this->organizer = $organizer;
    }

    /**
     * Returns the location
     *
     * @return \KDN\KdnEvents\Domain\Model\Location $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Returns the full location name
     *
     * @return string
     */
    public function getLocationFull()
    {
        return null !== $this->location ? $this->location->getFullName(', ') : ($this->customLocation ?? '');
    }

    /**
     * Sets the location
     *
     * @param \KDN\KdnEvents\Domain\Model\Location $location
     * @return void
     */
    public function setLocation(\KDN\KdnEvents\Domain\Model\Location $location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getCustomLocation(): string
    {
        return $this->customLocation;
    }

    /**
     * @param string $customLocation
     */
    public function setCustomLocation(string $customLocation): void
    {
        $this->customLocation = $customLocation;
    }

    /**
     * check if event registration should be displayed
     *
     * @return bool
     */
    public function getRegistrationIsOpen()
    {
        $status = $this->getStatus();
        $isInRegistrationPeriod = (null === $this->getRegistrationStart() || $this->getRegistrationStart()->getTimestamp() < time())
            && (null === $this->getRegistrationEnd() || $this->getRegistrationEnd()->getTimestamp() > time());
        $registrationPossible = $isInRegistrationPeriod
            && $status->getEventVisible() && $status->getRegistrationPossible();
        if ($registrationPossible) {
            $maxParticipantsReached = $this->getMaxParticipantsReached();
            $registrationPossible = !$maxParticipantsReached || $this->getStatus()->isWaitlistOpen();
        }
        return $registrationPossible;
    }

    /**
     * check if the maximum number of participants for this event has been reached
     *
     * @return bool
     */
    public function getMaxParticipantsReached()
    {
        $openPlaces = $this->getOpenPlaces();
        return $openPlaces === 0;
    }

    /**
     * returns the number of open places or -1 if no max participants are set
     *
     * @return int
     */
    public function getOpenPlaces()
    {
        $maxParticipants = (int)$this->getMaxParticipants();
        if ($maxParticipants > 0) {
            $registrations = (int)$this->getRegistrations();
            return max(0, $maxParticipants - $registrations);
        }
        return -1;
    }

    /**
     * Returns the event title appended with the number of registrations
     *
     * @return string
     */
    public function getRegistrationTitle()
    {
        return $this->getTitle() . ' (' . (int)$this->getRegistrations() . ')';
    }

    /**
     * Returns the event title and date appended with the number of registrations
     *
     * @return string
     */
    public function getTitleWithDateAndRegistrationCount()
    {
        return $this->getTitle() . ' - ' . $this->getEventStart()->format('d.m.Y') . ' (' . (int)$this->getRegistrations() . ')';
    }

    /**
     * @return string
     */
    public function getInternalRemarks()
    {
        return $this->internalRemarks;
    }

    /**
     * @param string $internalRemarks
     * @return Event
     */
    public function setInternalRemarks($internalRemarks)
    {
        $this->internalRemarks = $internalRemarks;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVideoConferenceUrl()
    {
        return $this->videoConferenceUrl;
    }

    /**
     * @param string|null $videoConferenceUrl
     */
    public function setVideoConferenceUrl($videoConferenceUrl)
    {
        $this->videoConferenceUrl = $videoConferenceUrl;
    }

    /**
     * @return string|null
     */
    public function getVideoConferenceTestUrl()
    {
        return $this->videoConferenceTestUrl;
    }

    /**
     * @param string $videoConferenceTestUrl
     */
    public function setVideoConferenceTestUrl($videoConferenceTestUrl)
    {
        $this->videoConferenceTestUrl = $videoConferenceTestUrl;
    }

    /**
     * @return string|null
     */
    public function getVideoConferenceComment()
    {
        return $this->videoConferenceComment;
    }

    /**
     * @param string|null $videoConferenceComment
     */
    public function setVideoConferenceComment($videoConferenceComment)
    {
        $this->videoConferenceComment = $videoConferenceComment;
    }

    /**
     * @return string
     */
    public function getEnableOptionalFields()
    {
        return $this->enableOptionalFields;
    }

    /**
     * @param string $enableOptionalFields
     * @return Event
     */
    public function setEnableOptionalFields($enableOptionalFields)
    {
        $this->enableOptionalFields = $enableOptionalFields;
        return $this;
    }


    /**
     * @param string $field
     * @return bool
     */
    public function isOptionalFieldEnabled($field)
    {
        if (!empty($this->enableOptionalFields)) {
            $enabledFields = explode(',', $this->enableOptionalFields);
            return in_array($field, $enabledFields, false);
        }
        return false;
    }

    /**
     * @return string
     */
    public function getEnableRequiredFields()
    {
        return $this->enableRequiredFields;
    }

    /**
     * @param string $enableRequiredFields
     * @return Event
     */
    public function setEnableRequiredFields($enableRequiredFields)
    {
        $this->enableRequiredFields = $enableRequiredFields;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEnabledWorkAreas(): ?string
    {
        return $this->enabledWorkAreas;
    }

    /**
     * @param string|null $enabledWorkAreas
     */
    public function setEnabledWorkAreas(?string $enabledWorkAreas): void
    {
        $this->enabledWorkAreas = $enabledWorkAreas;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function isRequiredFieldEnabled($field)
    {
        if (!empty($this->enableRequiredFields)) {
            $enabledFields = explode(',', $this->enableRequiredFields);
            return in_array($field, $enabledFields, false);
        }
        return false;
    }

    /**
     * @param $field
     * @return bool
     */
    public function getRequiredField($field)
    {
        return $this->isRequiredFieldEnabled($field);
    }

    /**
     * Returns the list of all additional fields
     *
     * @return array
     */
    public function getAdditionalFields()
    {
        if (!empty($this->enableRequiredFields)) {
            $enabledFields = explode(',', $this->enableRequiredFields);
        } else {
            $enabledFields = array();
        }
        if (!empty($this->enableOptionalFields)) {
            $enabledFields = array_merge($enabledFields, explode(',', $this->enableOptionalFields));
            $enabledFields = array_unique($enabledFields);
        }
        return $enabledFields;
    }

    /**
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }

    /**
     * Returns if terms are available
     *
     * @return bool
     */
    public function getTermsAvailable()
    {
        $termsPlainText = trim(strip_tags((string)$this->terms));
        return !empty($termsPlainText);
    }

    /**
     * @param string $terms
     * @return Event
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;
        return $this;
    }

    /**
     * Get categories
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\KDN\KdnEvents\Domain\Model\Category>
     */
    public function getEventCategories()
    {
        return $this->eventCategories;
    }

    /**
     * Get first category
     *
     * @return Category|null
     */
    public function getFirstCategory(): ?Category
    {
        $categories = $this->getEventCategories();
        if (!is_null($categories)) {
            $categories->rewind();
            $category = $categories->current();
            /** @var Category $category */
            return $category;
        }
        return null;
    }

    /**
     * Set categories
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $eventCategories
     * @return void
     */
    public function setEventCategories($eventCategories)
    {
        $this->eventCategories = $eventCategories;
    }

    /**
     * Adds a category to this categories.
     *
     * @param Category $category
     * @return void
     */
    public function addCategory(Category $category)
    {
        $this->getEventCategories()->attach($category);
    }

    /**
     * Get the media items
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set media relation
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $media
     */
    public function setMedia(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $media)
    {
        $this->media = $media;
    }

    /**
     * Add a media file reference
     *
     * @param FileReference $falMedia
     */
    public function addMedia(FileReference $falMedia)
    {
        if ($this->getMedia() === null) {
            $this->media = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        }
        $this->media->attach($falMedia);
    }

    /**
     * Get FAL related files
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     */
    public function getRelatedFiles()
    {
        return $this->relatedFiles;
    }

    /**
     * Set FAL related files
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $relatedFiles FAL related files
     */
    public function setRelatedFiles($relatedFiles)
    {
        $this->relatedFiles = $relatedFiles;
    }

    /**
     * Adds a file to this files.
     *
     * @param FileReference $file
     */
    public function addRelatedFile(FileReference $file)
    {
        if ($this->getRelatedFiles() === null) {
            $this->relatedFiles = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        }
        $this->getRelatedFiles()->attach($file);
    }

    /**
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getTutor()
    {
        return $this->tutor;
    }

    /**
     * @param string $tutor
     */
    public function setTutor($tutor)
    {
        $this->tutor = $tutor;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice(string $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getPriceMemberType2(): string
    {
        return $this->priceMemberType2;
    }

    /**
     * @param string $priceMemberType2
     */
    public function setPriceMemberType2(string $priceMemberType2): void
    {
        $this->priceMemberType2 = $priceMemberType2;
    }

    /**
     * @return string
     */
    public function getPriceMemberType3(): string
    {
        return $this->priceMemberType3;
    }

    /**
     * @param string $priceMemberType3
     */
    public function setPriceMemberType3(string $priceMemberType3): void
    {
        $this->priceMemberType3 = $priceMemberType3;
    }

    /**
     * @return string
     */
    public function getPriceMemberType4(): string
    {
        return $this->priceMemberType4;
    }

    /**
     * @param string $priceMemberType4
     */
    public function setPriceMemberType4(string $priceMemberType4): void
    {
        $this->priceMemberType4 = $priceMemberType4;
    }

    /**
     * @return string
     */
    public function getPriceMemberType5(): string
    {
        return $this->priceMemberType5;
    }

    /**
     * @param string $priceMemberType5
     */
    public function setPriceMemberType5(string $priceMemberType5): void
    {
        $this->priceMemberType5 = $priceMemberType5;
    }

    /**
     * @return int
     */
    public function getTaxType(): int
    {
        return (int) $this->taxType;
    }

    /**
     * @param int $taxType
     */
    public function setTaxType(int $taxType): void
    {
        $this->taxType = $taxType;
    }

    /**
     * @return string
     */
    public function getBillTitle(): string
    {
        return $this->billTitle;
    }

    /**
     * @param string $billTitle
     */
    public function setBillTitle(string $billTitle): void
    {
        $this->billTitle = $billTitle;
    }

    /**
     * @return string
     */
    public function getBillPaymentInfo(): string
    {
        return $this->billPaymentInfo;
    }

    /**
     * @param string $billPaymentInfo
     */
    public function setBillPaymentInfo(string $billPaymentInfo): void
    {
        $this->billPaymentInfo = $billPaymentInfo;
    }
    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

}
