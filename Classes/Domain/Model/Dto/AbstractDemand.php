<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model\Dto;

use KDN\KdnEvents\Domain\Model\DemandInterface;
use KDN\KdnEvents\Domain\Model\HasStatusInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
abstract class AbstractDemand extends AbstractEntity implements DemandInterface, HasStatusInterface
{

    /**
     * @var string
     */
    protected $order;

    /**
     * @var array
     */
    protected $searchFields;

    /**
     * @var string|null
     */
    protected $searchTerm;

    /**
     * @var integer|null
     */
    protected $limit;

    /**
     * @var integer|null
     */
    protected $offset;

    /**
     * List of allowed status values
     *
     * @var array|null
     */
    protected $allowedStatusList;

    /**
     * @var int|null
     */
    protected $status;

    /**
     * @var bool
     */
    protected $respectStoragePid = true;

    /**
     * @var bool
     */
    protected $submitted = false;

    /**
     * List of categories ids
     *
     * @var array|null
     */
    protected $categories;

    /**
     * Set search fields
     *
     * @param array $searchFields search fields
     * @return void
     */
    public function setSearchFields(array $searchFields): void
    {
        $this->searchFields = $searchFields;
    }

    /**
     * Get search fields
     *
     * @return array
     */
    public function getSearchFields(): array
    {
        if (null === $this->searchFields) {
            $this->searchFields = [];
            $reflect = new \ReflectionClass($this->getModelClass());
            $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);
            foreach ($props as $prop) {
                if (strpos($prop->getName(), '_') !== 0) {
                    $docComment = $prop->getDocComment();
                    preg_match('/@var ([\w\[\]\|]+)/', $docComment, $matches);
                    $dataTypes = empty($matches[1]) ? [] : explode('|', $matches[1]);
                    if (!empty($dataTypes) && in_array('string', $dataTypes, false)) {
                        $this->searchFields[] = $prop->getName();
                    }
                }
            }
        }
        return $this->searchFields;
    }

    /**
     * Returns the search term
     *
     * @return string|null
     */
    public function getSearchTerm(): ?string
    {
        return $this->searchTerm;
    }

    /**
     * @param string|null $searchTerm
     */
    public function setSearchTerm($searchTerm): void
    {
        $this->searchTerm = trim(strip_tags($searchTerm));
    }

    /**
     * Returns the search words
     *
     * @return array
     */
    public function getSearchWords(): array
    {
        $searchWords = [];
        $tempSearchWords = explode(' ', $this->searchTerm);
        foreach ($tempSearchWords as $word) {
            $word = trim($word);
            if (strlen($word) > 2) {
                $searchWords[$word] = $word;
            }
        }
        return $searchWords;
    }

    /**
     * Set order
     *
     * @param string|null $order order
     * @return void
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    /**
     * Get order
     *
     * @return string|null
     */
    public function getOrder(): ?string
    {
        return $this->order;
    }

    /**
     * Set limit
     *
     * @param int|null $limit limit
     * @return void
     */
    public function setLimit($limit): void
    {
        $this->limit = (int)$limit;
    }

    /**
     * Get limit
     *
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * Set offset
     *
     * @param int|null $offset offset
     * @return void
     */
    public function setOffset($offset): void
    {
        $this->offset = (int)$offset;
    }

    /**
     * Get offset
     *
     * @return int|null
     */
    public function getOffset(): ?int
    {
        return $this->offset;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    public function isSubmitted(): bool
    {
        return $this->submitted;
    }

    /**
     * @param bool $submitted
     */
    public function setSubmitted(bool $submitted): void
    {
        $this->submitted = $submitted;
    }

    public function setDefaultOrdering(): void
    {
        $this->setOrder('crdate DESC');
    }

    /**
     * Returns the session key for this demand
     * @return string
     */
    abstract public function getSessionKey(): string;

    /**
     * Returns the model class name
     *
     * @return string
     */
    abstract protected function getModelClass(): string;

    /**
     * Returns the valid sort properties for the given model class
     *
     * @return array
     * @throws \ReflectionException
     */
    public function getValidSortProperties(): array
    {
        $reflect = new \ReflectionClass($this->getModelClass());
        $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);
        $propertyList = [];
        foreach ($props as $prop) {
            if (strpos($prop->getName(), '_') !== 0) {
                $propertyList[] = $prop->getName();
            }
        }
        return $propertyList;
    }

    /**
     * @return array|int[]
     */
    public function getAllowedStatusList()
    {
        return $this->allowedStatusList ?? [];
    }

    /**
     * @param array|int[] $allowedStatusList
     */
    public function setAllowedStatusList($allowedStatusList): void
    {
        $this->allowedStatusList = $allowedStatusList;
    }

    /**
     * @return bool
     */
    public function isRespectStoragePid(): bool
    {
        return $this->respectStoragePid;
    }

    /**
     * @param bool $respectStoragePid
     */
    public function setRespectStoragePid(bool $respectStoragePid): void
    {
        $this->respectStoragePid = $respectStoragePid;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories ?? [];
    }

    /**
     * @param string|array|null $categories
     */
    public function setCategories($categories): void
    {
        if (null !== $categories && !is_array($categories)) {
            if (!empty($categories)) {
                $this->categories = GeneralUtility::intExplode(',', $categories);
            } else {
                $this->categories = [];
            }
        } else {
            $this->categories = $categories;
        }
    }

}
