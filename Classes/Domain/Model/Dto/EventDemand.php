<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2020 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model\Dto;

use KDN\KdnEvents\Domain\Model\Event;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class EventDemand extends AbstractDemand
{
    public const PUBLISHED_ONLY = 1;
    public const PUBLISHED_EXCLUDE = 2;

    public const RESTRICTION_IGNORE = 0;
    public const TOP_EVENTS_ONLY = 1;
    public const TOP_EVENTS_EXCLUDE = 2;

    /**
     * @var int|null
     */
    protected $publishedState = self::RESTRICTION_IGNORE;

    /**
     * @var bool
     */
    protected $ignoreHideInListFlag = false;

    /**
     * @var int
     */
    protected $topEventsRestriction = self::RESTRICTION_IGNORE;

    /**
     * @var int|null
     */
    protected $minStartTimestamp;

    /**
     * List of organizer ids
     *
     * @var array|null
     */
    protected $organizers;

    /**
     * List of allowed event types
     *
     * @var array|null
     */
    protected $eventTypeRestriction;

    /**
     * @return int
     */
    public function getPublishedState(): int
    {
        return $this->publishedState;
    }

    /**
     * @param int $publishedState
     */
    public function setPublishedState(int $publishedState): void
    {
        $this->publishedState = $publishedState;
    }

    public function setDefaultOrdering(): void
    {
        $this->setOrder('eventStart DESC');
    }

    public function getSessionKey(): string
    {
        return 'kdnevents.event';
    }

    /**
     * Returns the model class name
     *
     * @return string
     */
    protected function getModelClass(): string
    {
        return Event::class;
    }

    /**
     * @return bool
     */
    public function isIgnoreHideInListFlag(): bool
    {
        return $this->ignoreHideInListFlag;
    }

    /**
     * @param bool $ignoreHideInListFlag
     */
    public function setIgnoreHideInListFlag(bool $ignoreHideInListFlag): void
    {
        $this->ignoreHideInListFlag = $ignoreHideInListFlag;
    }

    /**
     * @return int
     */
    public function getTopEventsRestriction(): int
    {
        return $this->topEventsRestriction;
    }

    /**
     * @param int $topEventsRestriction
     */
    public function setTopEventsRestriction(int $topEventsRestriction): void
    {
        $this->topEventsRestriction = $topEventsRestriction;
    }


    /**
     * @return int|null
     */
    public function getMinStartTimestamp(): ?int
    {
        return $this->minStartTimestamp;
    }

    /**
     * @param int|null $minStartTimestamp
     */
    public function setMinStartTimestamp(?int $minStartTimestamp): void
    {
        $this->minStartTimestamp = $minStartTimestamp;
    }

    /**
     * @return array|null
     */
    public function getOrganizers(): ?array
    {
        return $this->organizers;
    }

    /**
     * @param string|array|null $organizers
     */
    public function setOrganizers($organizers): void
    {
        if (null !== $organizers && !is_array($organizers)) {
            if (!empty($organizers)) {
                $this->organizers = GeneralUtility::intExplode(',', $organizers);
            } else {
                $this->organizers = [];
            }
        } else {
            $this->organizers = $organizers;
        }
        if (!empty($this->organizers) && in_array('_all', GeneralUtility::trimExplode(',', $organizers), true)) {
            $this->organizers = [];
        }
    }

    /**
     * @return array|null
     */
    public function getEventTypeRestriction(): ?array
    {
        return $this->eventTypeRestriction;
    }

    /**
     * @param string|array|null $eventTypeRestriction
     */
    public function setEventTypeRestriction($eventTypeRestriction): void
    {
        if (null !== $eventTypeRestriction && !is_array($eventTypeRestriction)) {
            if (!empty($eventTypeRestriction)) {
                $this->eventTypeRestriction = GeneralUtility::intExplode(',', $eventTypeRestriction);
            } else {
                $this->eventTypeRestriction = [];
            }
        } else {
            $this->eventTypeRestriction = $eventTypeRestriction;
        }
    }

}
