<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Model\Dto;

use KDN\KdnEvents\Domain\Model\HasEventInterface;
use KDN\KdnEvents\Domain\Model\Registration;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class RegistrationDemand extends AbstractDemand implements HasEventInterface
{

    /**
     * @var \KDN\KdnEvents\Domain\Model\Event|null
     */
    protected $event;

    /**
     * @return \KDN\KdnEvents\Domain\Model\Event|null
     */
    public function getEvent(): ?\KDN\KdnEvents\Domain\Model\Event
    {
        return $this->event;
    }

    /**
     * @param \KDN\KdnEvents\Domain\Model\Event|null $event
     */
    public function setEvent(?\KDN\KdnEvents\Domain\Model\Event $event): void
    {
        $this->event = $event;
    }

    public function getSessionKey(): string
    {
        return 'kdnevents.registration';
    }

    /**
     * Returns the model class name
     *
     * @return string
     */
    protected function getModelClass(): string
    {
        return Registration::class;
    }

}
