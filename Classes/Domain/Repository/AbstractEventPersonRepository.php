<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Repository;

use KDN\KdnEvents\Domain\Model\AbstractEventPersonModel;
use KDN\KdnEvents\Domain\Model\Event;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AbstractEventPersonRepository extends AbstractDemandRepository
{
    /**
     * @param AbstractEventPersonModel $model
     *
     * @param bool $checkEvent
     * @param bool $checkUid
     * @return AbstractEventPersonModel|null The matching object if found, otherwise NULL
     */
    public function findDuplicate(AbstractEventPersonModel $model, bool $checkEvent = true, bool $checkUid = true)
    {
        $email = $model->getEmail();

        $query = $this->createQuery();
        //$query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);

        $constraints = array();
        $constraints[] = $query->equals('email', $email);
        if ($checkEvent && null !== $event = $model->getEvent()) {
            $constraints[] = $query->equals('event', $event);
        }
        if ($checkUid && $modelId = $model->getUid()) {
            $constraints[] = $query->logicalNot($query->equals('uid', $modelId));
        }
        $statusList = [AbstractEventPersonModel::STATUS_CANCELLED, AbstractEventPersonModel::STATUS_COMPLETED];
        $constraints[] = $query->logicalNot($query->in('status', $statusList));
        $query->matching($query->logicalAnd($constraints));
        $duplicateObject = $query->execute()->getFirst();
        /** @var \KDN\KdnEvents\Domain\Model\Registration|null $duplicateObject */
        return $duplicateObject;
    }
    /**
     * Find all records that are not marked as either cancelled or completed
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findActive()
    {
        $query = $this->createQuery();
        //$query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);

        $constraints = array();
        $statusList = [AbstractEventPersonModel::STATUS_CANCELLED, AbstractEventPersonModel::STATUS_COMPLETED];
        $constraints[] = $query->logicalNot($query->in('status', $statusList));
        $query->matching($query->logicalAnd($constraints));
        return $query->execute();
    }

    /**
     * @param Event $event
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findEventRowsForAnonymization(Event $event)
    {
        $query = $this->createQuery();
        //$query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE)
            ->setIgnoreEnableFields(true)
            ->setIncludeDeleted(true);

        $constraints = array();
        $constraints[] = $query->equals('event', $event);
        $query->matching($query->logicalAnd($constraints));

        return $query->execute();
    }

    /**
     * @param array $idList
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllInIdList(array $idList)
    {
        $query = $this->createQuery();
        //$query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);

        $constraints = array();
        $constraints[] = $query->in('uid', $idList);
        $query->matching($query->logicalAnd($constraints));

        return $query->execute();
    }

    /**
     * Generates a hash
     *
     * @param AbstractEventPersonModel $model
     *
     * @return void
     */
    public function generateUniqueHash(AbstractEventPersonModel $model)
    {
        $counter = 0;
        do {
            $hash = md5(time() . random_int(1, 99999999));
            ++$counter;

        } while ($this->findOneByHash($hash) !== null && $counter < 100);

        $model->setHash($hash);
    }

    /**
     * @param string $hash
     *
     * @return AbstractEventPersonModel|null The matching object if found, otherwise NULL
     */
    public function findOneByHash($hash)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);

        $constraints = array();
        $constraints[] = $query->equals('hash', $hash);
        $query->matching($query->logicalAnd($constraints));
        $object = $query->execute()->getFirst();
        /** @var AbstractEventPersonModel|null $object */
        return $object;
    }
}
