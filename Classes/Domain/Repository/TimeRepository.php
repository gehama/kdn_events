<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Repository;

use KDN\KdnEvents\Domain\Model\Time;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class TimeRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * Returns the event times for reminders
     *
     * @param int $period Number of days until event time start
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface|Time[]|array The query result
     */
    public function findForReminders($period) {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $constraints = array();
        $min = strtotime('midnight + ' . ($period) . ' days');
        $max = strtotime('+1 day', $min);
        $constraints[] = $query->greaterThanOrEqual('timeStart', new \DateTime('@'.$min));
        $constraints[] = $query->lessThan('timeStart', new \DateTime('@'.$max));
        $query->matching($query->logicalAnd($constraints));

        return $query->execute();
    }

}
