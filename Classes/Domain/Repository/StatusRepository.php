<?php

namespace KDN\KdnEvents\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for status
 */
class StatusRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * Initializes the repository.
     *
     * @return void
     */
    public function initializeObject()
    {
        /** @var \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings $querySettings */
        $querySettings = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(FALSE);
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * Returns the next status for the given status
     *
     * @param \KDN\KdnEvents\Domain\Model\Status $prevStatus
     * @param bool $eventIsFull
     * @return \KDN\KdnEvents\Domain\Model\Status|null
     */
    public function findNextStatus(\KDN\KdnEvents\Domain\Model\Status $prevStatus, $eventIsFull)
    {
        $object = null;
        $query = $this->createQuery();
        //$query->getQuerySettings()->setRespectStoragePage(FALSE);
        $constraints = array();
        $constraints[] = $query->equals('pid', $prevStatus->getPid());

        if ($prevStatus->getRegistrationPossible()) {
            $constraints[] = $query->equals('registrationPossible', true);
            if ($eventIsFull && !$prevStatus->getWaitlistOpen()) {
                $constraints[] = $query->equals('waitlistOpen', true);
            } elseif (!$eventIsFull && $prevStatus->getWaitlistOpen()) {
                $constraints[] = $query->equals('waitlistOpen', false);
            } else {
                return null;
            }

            $query->matching($query->logicalAnd($constraints));
            $result = $query->execute();
            if ($result->count() > 0) {
                $object = $result->getFirst();
            }
        }
        /** @var \KDN\KdnEvents\Domain\Model\Status|null $object */
        return $object;
    }

}