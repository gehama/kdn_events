<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Repository;

use KDN\KdnEvents\Domain\Model\DemandInterface;
use KDN\KdnEvents\Domain\Model\Dto\EventDemand;
use KDN\KdnEvents\Utility\TcaUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class EventRepository extends AbstractDemandRepository
{

    protected $defaultOrderings = array(
        'eventStart' => QueryInterface::ORDER_ASCENDING,
    );

    /**
     * Returns the events for anonymization
     *
     * @param int $period Number of days since the event is over
     *
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface|array
     */
    public function findForAnonymization($period)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false)
            ->setIgnoreEnableFields(true);
        $constraints = array();
        $constraints[] = $query->lessThanOrEqual('eventEnd', strtotime('- ' . $period . ' days'));
        // Only load events that have been over for twice the period of days
        // => fallback for missed anonymization, but don't load all past events
        $constraints[] = $query->greaterThanOrEqual('eventEnd', strtotime('- ' . (2 * $period) . ' days'));
        // Don't add status condition here, because the status records are stored on the global storage pid,
        // the events are not => the status condition would always return an empty result
        //$constraints[] = $query->equals('status.eventVisible', 1);
        $query->matching($query->logicalAnd($constraints));

        return $query->execute();
    }

    /**
     * Returns all events visible in backend module
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllForBackend()
    {
        $query = $this->createQuery();
        //$query->getQuerySettings()->setRespectStoragePage(FALSE);
        /*$constraints = array();
        $constraints[] = $query->greaterThanOrEqual('eventStart', time());

        $query->matching($query->logicalAnd($constraints));*/

        $orderings = array(
            'eventStart' => QueryInterface::ORDER_DESCENDING,
        );
        $query->setOrderings($orderings);


        return $query->execute();
    }

    /**
     * Returns an array of constraints created from a given demand object.
     *
     * @param QueryInterface $query
     * @param DemandInterface|EventDemand $demand
     *
     * @return array
     */
    protected function createConstraintsFromDemand(QueryInterface $query, DemandInterface $demand): array
    {
        $constraints = parent::createConstraintsFromDemand($query, $demand);
        if (null !== $demand->getMinStartTimestamp()) {
            $dateField = 'eventStart';
            $minTstamp = $demand->getMinStartTimestamp();
            if (TcaUtility::getExtConf('hideEventsAfterEnd')) {
                $dateField = 'eventEnd';
                $minTstamp = strtotime('midnight', $minTstamp);
            }
            $constraints[$dateField] = $query->greaterThanOrEqual($dateField, $minTstamp);
        }
        /** @var EventDemand $demand */
        $publishedState = $demand->getPublishedState();
        if ($publishedState === EventDemand::PUBLISHED_ONLY) {
            $constraints['published'] = $query->equals('published', 1);
        } elseif ($publishedState === EventDemand::PUBLISHED_EXCLUDE) {
            $constraints['published'] = $query->equals('published', 0);
        }
        if (!$demand->isIgnoreHideInListFlag()) {
            $constraints['hideInList'] = $query->equals('hideInList', 0);
        }
        if ($demand->getTopEventsRestriction() === EventDemand::TOP_EVENTS_ONLY) {
            $constraints['isTopEvent'] = $query->equals('isTopEvent', 1);
        } elseif ($demand->getTopEventsRestriction() === EventDemand::TOP_EVENTS_EXCLUDE) {
            $constraints['isTopEvent'] = $query->equals('isTopEvent', 0);
        }
        $eventTypeRestriction = $demand->getEventTypeRestriction();
        if (!empty($eventTypeRestriction)) {
            $constraints['eventType'] = $query->in('eventType', $eventTypeRestriction);
        }
        $organizers = $demand->getOrganizers();
        if (!empty($organizers)) {
            $constraints['organizer'] = $query->in('organizer', $organizers);
        }
        $categories = $demand->getCategories();
        if (!empty($categories)) {
            $constraints['categories'] = $this->createCategoryConstraint(
                $query,
                $categories,
                'and'
            );
        }
        return $constraints;
    }

    /**
     * Returns a category constraint created by
     * a given list of categories and a junction string
     *
     * @param QueryInterface $query
     * @param  string|array $categories
     * @param  string $conjunction
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\Qom\ConstraintInterface|null
     */
    protected function createCategoryConstraint(
        QueryInterface $query,
        $categories,
        $conjunction
    ) {
        $constraint = null;
        $categoryConstraints = [];

        // If "ignore category selection" is used, nothing needs to be done
        if (empty($conjunction)) {
            return null;
        }

        if (!is_array($categories)) {
            $categories = GeneralUtility::intExplode(',', $categories, true);
        }
        foreach ($categories as $category) {
            $categoryConstraints[] = $query->contains('eventCategories', $category);
        }

        if ($categoryConstraints) {
            switch (strtolower($conjunction)) {
                case 'or':
                    $constraint = $query->logicalOr($categoryConstraints);
                    break;
                case 'notor':
                    $constraint = $query->logicalNot($query->logicalOr($categoryConstraints));
                    break;
                case 'notand':
                    $constraint = $query->logicalNot($query->logicalAnd($categoryConstraints));
                    break;
                case 'and':
                default:
                    $constraint = $query->logicalAnd($categoryConstraints);
            }
        }

        return $constraint;
    }
}
