<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes <info@gerthammes.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace KDN\KdnEvents\Domain\Repository;

use KDN\KdnEvents\Domain\Model\DemandInterface;
use KDN\KdnEvents\Domain\Model\HasEventInterface;
use KDN\KdnEvents\Domain\Model\HasStatusInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
abstract class AbstractDemandRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @param DemandInterface $demand
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findDemanded(DemandInterface $demand)
    {
        $query = $this->createDemandQuery($demand);

        return $query->execute();
    }

    /**
     * Returns a query for objects of this repository
     *
     * @param DemandInterface $demand
     * @return \TYPO3\CMS\Extbase\Persistence\QueryInterface
     */
    protected function createDemandQuery(DemandInterface $demand)
    {
        $query = $this->createQuery();
        //$query->getQuerySettings()->setRespectStoragePage(FALSE);


        $constraints = $this->createConstraintsFromDemand($query, $demand);

        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd($constraints)
            );
        }

        $this->addDemandOrderings($query, $demand);

        if ((int)$demand->getLimit() > 0) {
            $query->setLimit((int)$demand->getLimit());
        }

        if ((int)$demand->getOffset() > 0) {
            $query->setOffset((int)$demand->getOffset());
        }
        return $query;
    }

    /**
     * @param QueryInterface $query
     * @param DemandInterface $demand
     */
    private function addDemandOrderings(QueryInterface $query, DemandInterface $demand)
    {
        $orderList = GeneralUtility::trimExplode(',', $demand->getOrder(), TRUE);
        if (!empty($orderList)) {
            $orderings = array();
            // go through every order statement
            foreach ($orderList as $orderItem) {
                [$orderField, $ascDesc] = GeneralUtility::trimExplode(' ', $orderItem, TRUE);
                // count == 1 means that no direction is given
                if ($ascDesc) {
                    $orderings[$orderField] = ((strtolower($ascDesc) === 'desc') ?
                        QueryInterface::ORDER_DESCENDING :
                        QueryInterface::ORDER_ASCENDING);
                } else {
                    $orderings[$orderField] = QueryInterface::ORDER_ASCENDING;
                }
            }

            if (!empty($orderings)) {
                $query->setOrderings($orderings);
            }
        }
    }

    /**
     * Returns an array of constraints created from a given demand object.
     *
     * @param QueryInterface $query
     * @param DemandInterface $demand
     *
     * @return array
     */
    protected function createConstraintsFromDemand(QueryInterface $query, DemandInterface $demand): array
    {
        $constraints = [];

        if (!$demand->isRespectStoragePid()) {
            $query->getQuerySettings()->setRespectStoragePage(false);
        }

        if ($demand instanceof HasEventInterface && null !== $event = $demand->getEvent()) {
            $constraints[] = $query->equals('event', $event);
        }

        $searchFields = $demand->getSearchFields();
        $searchWords = $demand->getSearchWords();
        if (!empty($searchFields) && !empty($searchWords)) {

            $searchConstraints = array();
            foreach ($searchFields as $field) {
                foreach ($searchWords as $word) {
                    if (!empty($word)) {
                        $searchConstraints[] = $query->like($field, '%' . $word . '%');
                    }
                }
            }

            if (count($searchConstraints)) {
                $constraints[] = $query->logicalOr($searchConstraints);
            }
        }

        if ($demand instanceof HasStatusInterface) {
            if (0 < (int)$status = $demand->getStatus()) {
                $constraints[] = $query->equals('status', (int)$status);
            }
            // Limit generally allowed status values
            // (additionally to selected status, in case an invalid status is selected!)
            $allowedStatusList = $demand->getAllowedStatusList();
            if (!empty($allowedStatusList)) {
                $constraints[] = $query->in('status', $allowedStatusList);
            }
        }

        return $constraints;
    }
}
