# customcategory=storage=Storage PIDs
# customcategory=categories=Category IDs
# customcategory=misc=Miscellaneous

# cat=storage//40; type=string; label= Page ID for events list and detail view
eventsListPid = 0

# cat=storage//50; type=string; label= Page ID for registration cancellation view
registrationCancelPid = 0

# cat=storage//60; type=string; label= ID of parent category for event categories
eventCategoryParentId = 0

# cat=storage//70; type=string; label= ID of parent category for event group
eventGroupParentId = 0

# cat=storage//80; type=string; label= Page ID for events invitations
invitationPid = 0

# cat=features//80; type=boolean; label=Enable chargeable events
enableChargeableEvents = 0

# cat=features//90; type=boolean; label=Hide events in list after event end (default event start)
hideEventsAfterEnd = 0

# cat=features//100; type=boolean; label=Disable agree to privacy terms checkbox for authenticated users
disablePrivacyTermsForAuthenticatedUsers = 0

# cat=features//110; type=boolean; label=Disable change of personal data for authenticated users
disableAuthenticatedUserDataChange = 0

# cat=features//120; type=boolean; label=Enable email to organizer for new registrations
enableEmailToOrganizer = 0

# cat=features//130; type=boolean; label=Enable invitations to interested prospects
enableInvitations = 0

# cat=misc//10; type=string; label= Domain name (without protocol) used for console commands
domain =

# cat=misc//12; type=string; label= Force email domain for email template records
baEmailDomain =

# cat=misc//20; type=string; label= Make fields (from optional field list) always visible in frontend registration form (CSV list of table field names)
baAddFieldsAlwaysVisibleFields =

# cat=misc//30; type=string; label= Make fields always required in frontend registration form (CSV list of table field names)
baAddFieldsAlwaysRequiredFields =

# cat=misc//40; type=string; label= Always exclude fields in frontend registration form (CSV list of table field names)
baAddFieldsAlwaysExcluded =

# cat=misc//83; type=string; label= User property used for price calculation
userTypeField = usergroup

# cat=misc//84; type=string; label= The enabled price fields
enabledCustomPriceFields = price_member_type_2,price_member_type_3,price_member_type_4,price_member_type_5