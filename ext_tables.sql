#
# Table structure for table 'tx_kdnevents_domain_model_event'
#
CREATE TABLE tx_kdnevents_domain_model_event (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

    event_type tinyint(1) unsigned DEFAULT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	subtitle varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,
	event_start int(11) DEFAULT '0' NOT NULL,
	event_end int(11) DEFAULT '0' NOT NULL,
	registration_start int(11) DEFAULT '0' NOT NULL,
	registration_end int(11) DEFAULT '0' NOT NULL,
    registration_cancel_limit tinyint(3) unsigned NOT NULL DEFAULT '0',
	max_participants int(11) DEFAULT '0' NOT NULL,
    is_top_event tinyint(1) unsigned NOT NULL DEFAULT '0',
    is_online_event tinyint(1) unsigned NOT NULL DEFAULT '0',
	published tinyint(1) unsigned DEFAULT '0' NOT NULL,
	hide_in_list tinyint(1) unsigned DEFAULT '0' NOT NULL,
	waitlist_available tinyint(1) unsigned DEFAULT '0' NOT NULL,
	registration_disabled tinyint(1) unsigned DEFAULT '0' NOT NULL,
	registrations int(11) DEFAULT '0' NOT NULL,
	status int(11) unsigned DEFAULT '0',
	organizer int(11) unsigned DEFAULT '0',
	location int(11) unsigned DEFAULT '0',
	custom_location varchar(255) DEFAULT '' NOT NULL,
	internal_remarks text NOT NULL,
	terms text NOT NULL,
	enable_optional_fields varchar(1024) DEFAULT '' NOT NULL,
	enable_required_fields varchar(1024) DEFAULT '' NOT NULL,
    enabled_work_areas varchar(255) NOT NULL DEFAULT '',
	media int(11) unsigned DEFAULT '0',
	related_files int(11) unsigned DEFAULT '0',
	event_times int(11) unsigned DEFAULT '0',
	registration_list int(11) unsigned DEFAULT '0',
	is_series_event tinyint(1) unsigned DEFAULT '0' NOT NULL,
	contact varchar(255) DEFAULT '' NOT NULL,
	tutor varchar(255) DEFAULT '' NOT NULL,
	video_conference_url text NULL,
	video_conference_test_url text NULL,
	video_conference_comment text NULL,
	slug varchar(2048),
    price decimal(6,2) unsigned NOT NULL DEFAULT '0.00',
    price_member_type_2 decimal(6,2) unsigned NOT NULL DEFAULT '0.00',
    price_member_type_3 decimal(6,2) unsigned NOT NULL DEFAULT '0.00',
    price_member_type_4 decimal(6,2) unsigned NOT NULL DEFAULT '0.00',
    price_member_type_5 decimal(6,2) unsigned NOT NULL DEFAULT '0.00',
    tax_type tinyint(4) default NULL,
    bill_title varchar(255) default NULL,
    bill_payment_info varchar(1000) default NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
    KEY deleted (deleted),
    KEY hidden (hidden),
    KEY published (published),

	KEY language (l10n_parent,sys_language_uid),
    KEY is_top_event (is_top_event)

);

#
# Table structure for table 'tx_kdnevents_domain_model_status'
#
CREATE TABLE tx_kdnevents_domain_model_status (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	name varchar(255) DEFAULT '' NOT NULL,
	registration_possible tinyint(1) unsigned DEFAULT '0' NOT NULL,
	event_visible tinyint(1) unsigned DEFAULT '0' NOT NULL,
    show_registration_info tinyint(1) unsigned DEFAULT '0' NOT NULL,
	waitlist_open tinyint(1) unsigned DEFAULT '0' NOT NULL,
	registration_text text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_kdnevents_domain_model_organizer'
#
CREATE TABLE tx_kdnevents_domain_model_organizer (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	name varchar(255) DEFAULT '' NOT NULL,
    email varchar(255) DEFAULT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_kdnevents_domain_model_location'
#
CREATE TABLE tx_kdnevents_domain_model_location (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	name varchar(255) DEFAULT '' NOT NULL,
	street varchar(255) DEFAULT '' NOT NULL,
	zip varchar(20) DEFAULT '' NOT NULL,
    room varchar(20) DEFAULT '' NOT NULL,
    description text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),

	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_kdnevents_domain_model_registration'
#
CREATE TABLE tx_kdnevents_domain_model_registration (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	salutation int(11) DEFAULT '0' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	first_name varchar(255) DEFAULT '' NOT NULL,
	last_name varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	birth_date int(11) DEFAULT '0' NOT NULL,
	street varchar(255) DEFAULT '' NOT NULL,
	zipcode varchar(255) DEFAULT '' NOT NULL,
	town varchar(255) DEFAULT '' NOT NULL,
	phone varchar(255) DEFAULT '' NOT NULL,
    mobile varchar(255) DEFAULT '' NOT NULL,
	occupation varchar(255) DEFAULT '' NOT NULL,
	work_experience varchar(255) DEFAULT '' NOT NULL,
	organisation varchar(255) DEFAULT '' NOT NULL,
	organisation_street varchar(255) DEFAULT '' NOT NULL,
	organisation_zipcode varchar(255) DEFAULT '' NOT NULL,
	organisation_town varchar(255) DEFAULT '' NOT NULL,
	organisation_email varchar(255) DEFAULT '' NOT NULL,
	organisation_phone varchar(255) DEFAULT '' NOT NULL,
	customer_message TEXT DEFAULT NULL,
	agree_to_terms tinyint(1) unsigned DEFAULT '0' NOT NULL,
    newsletter tinyint(1) unsigned DEFAULT '0' NOT NULL,
	hash varchar(255) DEFAULT '' NOT NULL,
	status int(11) DEFAULT '0' NOT NULL,
	event int(11) unsigned DEFAULT '0',
    event_time int(11) unsigned DEFAULT '0',
	facility int(11) unsigned DEFAULT '0',
	internal_remarks TEXT DEFAULT NULL,
    reminder_status tinyint(4) unsigned DEFAULT '0' NOT NULL,
	work_areas int(11) unsigned DEFAULT '0' NOT NULL,
    user int(11) unsigned DEFAULT '0',
    price decimal(6,2) unsigned NOT NULL DEFAULT '0.00',
    bill_id int(10) unsigned NOT NULL DEFAULT '0',
    member_type tinyint(1) unsigned NOT NULL DEFAULT '0',
    invitation int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
    KEY event (event),
    KEY event_time (event_time),
    KEY facility (facility),
    KEY user (user),

	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_kdnevents_domain_model_emailtemplate'
#
CREATE TABLE tx_kdnevents_domain_model_emailtemplate (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	template_key varchar(255) DEFAULT '' NOT NULL,
	description varchar(255) DEFAULT '' NOT NULL,
	subject varchar(255) DEFAULT '' NOT NULL,
	sender_email varchar(255) DEFAULT '' NOT NULL,
	sender_name varchar(255) DEFAULT '' NOT NULL,
	admin_email varchar(255) DEFAULT '' NOT NULL,
	reply_to_email varchar(255) DEFAULT '' NOT NULL,
	body text NOT NULL,
    attachments int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
    KEY template_key (template_key),

	KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_kdnevents_domain_model_facility'
#
CREATE TABLE tx_kdnevents_domain_model_facility (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	name varchar(255) DEFAULT '' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
    KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'tx_kdnevents_domain_model_workarea'
#
CREATE TABLE tx_kdnevents_domain_model_workarea (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	name varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,
	event int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
    KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'tx_kdnevents_domain_model_registration_workarea_mm'
#
CREATE TABLE tx_kdnevents_domain_model_registration_workarea_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_kdnevents_domain_model_time'
#
CREATE TABLE tx_kdnevents_domain_model_time (

  uid int(11) NOT NULL auto_increment,
  pid int(11) DEFAULT '0' NOT NULL,

  time_start int(11) unsigned DEFAULT '0' NOT NULL,
  time_end int(11) unsigned DEFAULT '0' NOT NULL,
  event int(11) unsigned DEFAULT '0' NOT NULL,
  max_participants int(11) DEFAULT '0' NOT NULL,
  registrations int(11) DEFAULT '0' NOT NULL,

  tstamp int(11) unsigned DEFAULT '0' NOT NULL,
  crdate int(11) unsigned DEFAULT '0' NOT NULL,
  cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
  hidden tinyint(1) unsigned DEFAULT '0' NOT NULL,

  PRIMARY KEY (uid),
  KEY parent (pid),
  KEY event (event)
);

#
# Add fields to table 'fe_groups'
#
CREATE TABLE fe_groups (
	tx_kdnevents_price_type int(11) unsigned DEFAULT '0' NOT NULL
);

#
# Table structure for table 'tx_kdnevents_domain_model_invitation'
#
CREATE TABLE tx_kdnevents_domain_model_invitation (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	salutation int(11) DEFAULT '0' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	first_name varchar(255) DEFAULT '' NOT NULL,
	last_name varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	phone varchar(255) DEFAULT '' NOT NULL,
    mobile varchar(255) DEFAULT '' NOT NULL,
	message TEXT DEFAULT NULL,
	hash varchar(255) DEFAULT '' NOT NULL,
    status int(11) DEFAULT '0' NOT NULL,
	event int(11) unsigned DEFAULT '0',
    event_time int(11) unsigned DEFAULT '0',
	internal_remarks TEXT DEFAULT NULL,
    invited_by int(11) unsigned DEFAULT '0',
    registration int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
    KEY event (event),
    KEY event_time (event_time),
    KEY invited_by (invited_by),

	KEY language (l10n_parent,sys_language_uid)
);