<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Gert Hammes
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use KDN\KdnEvents\Utility\TcaUtility;

/**
 *
 *
 * @package kdn_events
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class tx_kdnevents_emaildomaineval
{
    private $domain;

    private function getDomain()
    {
        if (null === $this->domain) {
            $this->domain = (string)TcaUtility::getExtConf('baEmailDomain');
        }
        return $this->domain;
    }

    public function returnFieldJS()
    {
        $domain = $this->getDomain();
        if (empty($domain)) {
            return '
			return value;
		  ';
        }

        return '
			if (value.trim().length > 0 && value.indexOf(\'' . $domain . '\') > 0) {
				var valParts = value.trim().split("@");
				var emailLocalName = valParts[0];
				if (emailLocalName.length > 0) {
					return emailLocalName + \'@' . $domain . '\';
				}
			}
			return value;
		  ';
    }

    public function evaluateFieldValue($value, $is_in, &$set)
    {
        $trimVal = trim($value);
        $domain = $this->getDomain();
        if (!empty($trimVal) && !empty($domain) && strpos($value, $domain) === false) {
            $valParts = explode('@', trim($value));
            $emailLocalName = trim($valParts[0]);
            if (!empty($emailLocalName)) {
                return $emailLocalName . '@' . $this->getDomain();
            }
        }
        return $trimVal;
    }
}
