
var kdnEvents = {

    init: function () {
        if(jQuery().fancybox) {
            jQuery(".kdn-events-fancybox").fancybox();
        }
        var form = document.querySelector('.form-validate');
        if (null !== form) {
            form.addEventListener("submit", function(e){
                if (!kdnEvents.validateForm(form)) {
                    e.preventDefault();
                }
            });
        }
    },

    validateForm: function(form) {
        var self = this;
        var isValid = true;
        var errorElts = form.querySelectorAll('.has-error');
        for (var i = 0, n = errorElts.length; i < n; i++) {
            errorElts[i].classList.remove('has-error');
        }

        var isValidRequired = this.validateRequired(form, 'form-group', 'has-error');
        var isValidCb = self.validateCheckboxes(form, 'form-group', 'has-error');
        var isValidEmail = this.validateEmail(form, 'form-group', 'has-error');
        return isValid && isValidRequired && isValidEmail && isValidCb;
    },
    validateCheckboxes: function(form, parentClass, errorClass)
    {
        var self = this;
        var isValid = true;
        var elements = form.querySelectorAll('.validate-checkbox');
        for (var i = 0, vcb = elements.length; i < vcb; i++) {
            var isValidField = elements[i].checked;
            self.toggleFormGroupError(elements[i], parentClass, errorClass, isValidField);
            if (!isValidField) {
                isValid = false;
            }
        }
        return isValid;
    },
    validateEmail: function(form, parentClass, errorClass)
    {
        var self = this;
        var isValid = true;
        var elements = form.querySelectorAll('.validate-email');
        for (var i = 0, vel = elements.length; i < vel; i++) {
            var fieldVal = elements[i].value.trim();

            var isValidEmail = fieldVal.length > 0;
            if (isValidEmail) {
                isValidEmail = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(fieldVal);
            } else {
                isValidEmail = !elements[i].classList.contains('validate-required');
            }
            self.toggleFormGroupError(elements[i], parentClass, errorClass, isValidEmail);
            if (!isValidEmail) {
                isValid = false;
            }
        }
        return isValid;
    },
    validateRequired: function(form, parentClass, errorClass)
    {
        var self = this;
        var isValid = true;
        var elements = form.querySelectorAll('.validate-required');
        for (var i = 0, vel = elements.length; i < vel; i++) {
            var type = elements[i].nodeName.toLowerCase() === 'select' ? 'select' : elements[i].getAttribute('type');
            var fieldIsEmpty = false;
            if (type === 'select') {
                var fieldVal = elements[i].value.trim();
                fieldIsEmpty = fieldVal.length === 0 || parseInt(fieldVal) === 0;
            } else if (type === 'radio' || type === 'checkbox') {
                var parent = self.getClosestWithClass(elements[i], parentClass);
                fieldIsEmpty = self.getCheckedCountForParent(parent, type) === 0;
            } else {
                fieldIsEmpty = elements[i].value.trim().length === 0;
            }
            self.toggleFormGroupError(elements[i], parentClass, errorClass, !fieldIsEmpty);
            if (fieldIsEmpty) {
                isValid = false;
            }
        }
        return isValid;
    },
    getCheckedCountForParent: function (parent, type) {
        var count = 0;
        var elements = parent.querySelectorAll('input[type="'+type+'"]');
        for (var i = 0, vel = elements.length; i < vel; i++) {
            if (elements[i].offsetParent !== null && elements[i].checked) {
                ++count;
            }
        }
        return count;
    },
    getClosestWithClass: function (elem, matchClass) {
        for ( ; elem && elem !== document; elem = elem.parentNode ) {
            if ( elem.classList.contains(matchClass) ) return elem;
        }
        return null;
    },
    toggleFormGroupError: function(element, parentClass, errorClass, isValidField)
    {
        var parent = this.getClosestWithClass(element, parentClass);
        if (null !== parent) {
            if (isValidField) {
                parent.classList.remove(errorClass);
            } else {
                parent.classList.add(errorClass);
            }
        }
    }

};

if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {
    kdnEvents.init();
} else {
    document.addEventListener("DOMContentLoaded", function(){
        kdnEvents.init();
    });
}