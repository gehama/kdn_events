
var kdnEvents = {
    waitCount: 0,
    init: function() {
        var self = this;
        var checkboxes = document.querySelectorAll("input.kdn-event-cb");
        var cbControl = document.getElementById("kdn-events-check-all");
        var submitRow = document.getElementById("row-mailing-submit");
        if (null === submitRow && self.waitCount < 5) {
            ++self.waitCount;
            window.setTimeout(function(){self.init();}, 250);
        }
        if (null === submitRow) {
            return;
        }
        var totalCheckboxCount = checkboxes.length;
        var initCheckCount = 0;
        for (var i = 0; i < totalCheckboxCount; i++) {
            if (checkboxes[i].checked) {
                ++initCheckCount;
            }
        }
        if (initCheckCount === 0) {
            submitRow.style.display = "none";
        }
        cbControl.addEventListener('click', function() {
            for (var i = 0; i < totalCheckboxCount; i++) {
                checkboxes[i].checked = cbControl.checked;
            }
            if (cbControl.checked) {
                submitRow.style.display = "block";
            } else {
                submitRow.style.display = "none";
            }
        }, false);

        for (var k = 0; k < totalCheckboxCount; k++) {
            checkboxes[k].addEventListener('click', function() {
                var checkCount = 0;
                for (var j = 0; j < totalCheckboxCount; j++) {
                    if (checkboxes[j].checked) {
                        ++checkCount;
                    }
                }
                if (checkCount === 0) {
                    submitRow.style.display = "none";
                    cbControl.checked = false;
                } else {
                    submitRow.style.display = "block";
                    cbControl.checked = checkCount === totalCheckboxCount;
                }
            }, false);
        }

        self.initForm();
    },

    initForm: function() {
        var self = this;
        var mailingForm = document.getElementById("form-mailing");
        mailingForm.addEventListener('submit', function(evt) {
            var isValid = self.validateForm(mailingForm);
            if (!isValid) {
                evt.preventDefault();
            }
            return isValid;
        }, false);
    },
    validateForm: function(form) {
        var self = this;
        var isValid = true;
        var requiredFields = form.querySelectorAll(".validate-required");
        for (var j = 0, n = requiredFields.length; j < n; j++) {
            var vlElt = requiredFields[j];
            var fieldVal = vlElt.tagName.toLowerCase() === 'select' ? vlElt.options[vlElt.selectedIndex].value : vlElt.value;
            var parent = self.parents(vlElt, '.form-group', true);
            if (fieldVal.length === 0) {
                isValid = false;
                parent.classList.add('error');
            } else {
                parent.classList.remove('error');
            }
        }
        return isValid;
    },
    parents: function(elem, selector, onlyFirst) {
        var elements = [];
        var ishaveselector = selector !== undefined;

        while ((elem = elem.parentElement) !== null) {
            if (elem.nodeType !== Node.ELEMENT_NODE) {
                continue;
            }

            if (!ishaveselector || elem.matches(selector)) {
                if (onlyFirst) {
                    return elem;
                }
                elements.push(elem);
            }
        }

        return elements;
    }
};
if (
    document.readyState === "complete" ||
    (document.readyState !== "loading" && !document.documentElement.doScroll)
) {

    kdnEvents.init();
} else {
    document.addEventListener("DOMContentLoaded", function(){
        kdnEvents.init()
    });
}