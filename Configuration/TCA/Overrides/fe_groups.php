<?php

use KDN\KdnEvents\Utility\TcaUtility;

defined('TYPO3_MODE') or die();

// Add price type field in user group TCA depending on extension configuration
$enableChargeableEvents = (bool) \KDN\KdnEvents\Utility\TcaUtility::getExtConf('enableChargeableEvents');
$field = \KDN\KdnEvents\Service\OrderItemPriceCalculator::USER_GROUP_PRICE_TYPE_PROPERTY;
if ($enableChargeableEvents && 'usergroup' === $userTypeField = TcaUtility::getExtConf('userTypeField')) {
    $addGroupColumns = [
        $field => [
            'exclude' => true,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:fe_groups.tx_kdnevents_price_type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:fe_groups.tx_kdnevents_price_type.0', 0],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:fe_groups.tx_kdnevents_price_type.2', 2],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:fe_groups.tx_kdnevents_price_type.3', 3],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:fe_groups.tx_kdnevents_price_type.4', 4],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:fe_groups.tx_kdnevents_price_type.4', 5],
                ],
                'size' => 1,
                'maxitems' => 1,
                'default' => 0
            ]
        ]
    ];
} else {
    $addGroupColumns = [
        $field => [
            'config' => [
                'type' => 'passthrough',
            ],
        ]
    ];
}
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_groups', $addGroupColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_groups', $field, '', 'after:subgroup');
