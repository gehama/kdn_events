<?php
$eventCategoryParentId = (int) \KDN\KdnEvents\Utility\TcaUtility::getExtConf('eventCategoryParentId');
$foreignTableWhere = ' AND sys_category.sys_language_uid IN (-1, 0) '
    . ($eventCategoryParentId > 0 ? ' AND (sys_category.uid = '.$eventCategoryParentId
        . ' OR sys_category.parent = '.$eventCategoryParentId
        . ' OR sys_category.parent IN (SELECT b.uid FROM sys_category b WHERE b.parent = '.$eventCategoryParentId.'))' : '')
    .' ORDER BY sys_category.title ASC';
//https://docs.typo3.org/typo3cms/CoreApiReference/ApiOverview/Categories/Index.html
// Add an extra categories selection field to the pages table
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
	'kdn_events',
	'tx_kdnevents_domain_model_event',
	// Do not use the default field name ("categories"), which is already used
	'event_categories',
	[
		// Set a custom label
		//'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang.xlf:additional_categories',
		// This field should not be an exclude-field
		'exclude' => false,
		// Override generic configuration, e.g. sort by title rather than by sorting
		'fieldConfiguration' => [
			'foreign_table_where' => $foreignTableWhere,
        ],
		// string (keyword), see TCA reference for details
		'l10n_mode' => 'exclude',
		// list of keywords, see TCA reference for details
		'l10n_display' => 'hideDiff',
    ]
);