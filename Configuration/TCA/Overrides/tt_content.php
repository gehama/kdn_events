<?php
defined('TYPO3_MODE') || die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'kdn_events',
    'Items',
    'KDN Event database'
);

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase('kdn_events'));
$pluginName = 'items';
$pluginSignature = $extensionName.'_'.$pluginName;

/**
 * Disable not needed fields in tt_content
 */
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    $pluginSignature,
    'FILE:EXT:kdn_events/Configuration/FlexForms/flexform_kdnevents.xml'
);

if (\KDN\KdnEvents\Utility\TcaUtility::getExtConf('enableInvitations')) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'kdn_events',
        'Invitation',
        'KDN Event invitations'
    );
    $pluginName = 'invitation';
    $pluginSignature = $extensionName.'_'.$pluginName;
    /**
     * Disable not needed fields in tt_content
     */
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key';
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
        $pluginSignature,
        'FILE:EXT:kdn_events/Configuration/FlexForms/flexform_invitations.xml'
    );
}