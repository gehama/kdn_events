<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_time',
        'label_userFunc' => \KDN\KdnEvents\Hook\Labels::class . '->getEventTimeLabel',
        'label' => 'time_start',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'hideTable' => true,
        'default_sortby' => 'ORDER BY time_start ASC',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'typeicon_classes' => [
            'default' => 'kdn-events-time',
        ],
    ],
    'types' => [
        '1' => ['showitem' => '--palette--;;date'],
    ],
    'palettes' => [
        'date' => [
            'showitem' => 'event, hidden, time_start, time_end',
        ],
    ],
    'columns' => [
        'hidden' => [
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'time_start' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_time.time_start',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int,required',
                //'default' => time()
            ],
        ],
        'time_end' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_time.time_end',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int,required',
                //'default' => time()
            ],
        ],
        'event' => [
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_kdnevents_domain_model_event',
            ]
        ],
        'max_participants' => [
            /*'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_time.max_participants',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int,required'
            ],*/
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'registrations' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_time.registrations',
            'config' => [
                'type' => 'none',
                'size' => 4,
                'format' => 'int'
            ],
        ],
    ]
];