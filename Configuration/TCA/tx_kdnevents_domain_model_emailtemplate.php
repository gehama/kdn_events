<?php
return [
	'ctrl' => [
		'title'	=> 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate',
		'label' => 'template_key',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',

		'origUid' => 't3_origuid',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
        ],
		'searchFields' => 'template_key,description,subject,body,',
        'typeicon_classes' => [
            'default' => 'kdn-events-email-template',
        ],
    ],
	'types' => [
		'1' => ['showitem' => '--palette--;;paletteCore, template_key, description, sender_email, sender_name, admin_email, 
		reply_to_email, subject, body, attachments, marker_legend,
		--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime, endtime,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;paletteLanguage,'],
    ],
	'palettes' => [
		'1' => ['showitem' => ''],
        'paletteCore' => [
            'showitem' => 'hidden,',
        ],
        'paletteLanguage' => [
            'showitem' => '
                sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l10n_parent, l10n_diffsource,
            ',
        ],
    ],
	'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ],
                ],
                'default' => 0,
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0]
                ],
                'foreign_table' => 'tx_kdnevents_domain_model_emailtemplate',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_emailtemplate.pid=###CURRENT_PID### AND tx_kdnevents_domain_model_emailtemplate.sys_language_uid IN (-1,0)',
                'default' => 0
            ]
        ],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ],
                ],
            ]
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
		'template_key' => [
			'exclude' => 1,
			'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.template_key',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
            ],
        ],
		'description' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.description',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
            ],
        ],
		'sender_email' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.sender_email',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'tx_kdnevents_emaildomaineval,trim,required'
            ],
        ],
		'sender_name' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.sender_name',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
            ],
        ],
		'admin_email' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.admin_email',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'tx_kdnevents_emaildomaineval,trim,required'
            ],
        ],
		'reply_to_email' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.reply_to_email',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'tx_kdnevents_emaildomaineval,trim'
            ],
        ],
		'subject' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.subject',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
            ],
        ],
		'body' => [
			'exclude' => 0,
			'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.body',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim,required'
            ],
        ],
        'attachments' => [
            'exclude' => true,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.attachments',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'attachments',
                [
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.attachments.add',
                        'showPossibleLocalizationRecords' => true,
                        'showRemovedLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'inline' => [
                        'inlineOnlineMediaAddButtonStyle' => 'display:none'
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'attachments',
                        'tablenames' => 'tx_kdnevents_domain_model_emailtemplate',
                        'table_local' => 'sys_file',
                    ],
                ]
            )
        ],
		'marker_legend' => [
				'exclude' => 0,
				'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_emailtemplate.marker_legend',
				'config' => [
						'type' => 'none',
						'size' => 30,
                        'renderType' => 'eventMarkers',
                ]
        ],
    ],
];
