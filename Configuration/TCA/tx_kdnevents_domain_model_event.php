<?php
$enableChargeableEvents = (bool) \KDN\KdnEvents\Utility\TcaUtility::getExtConf('enableChargeableEvents');
$tcaCostsFields = '';
$palettes = [
    'visibility' => ['showitem' => 'status, published, hidden'],
    'settings' => ['showitem' => 'is_top_event, is_online_event, hide_in_list'],
    'headline' => ['showitem' => 'title, --linebreak--, slug, --linebreak--, subtitle'],
    'registration_time' => ['showitem' => 'registration_start, registration_end'],
    'location_data' => ['showitem' => 'location, custom_location'],
    'registration_participants' => ['showitem' => 'max_participants, waitlist_available, registrations, registration_cancel_limit'],
    'paletteLanguage' => [
        'showitem' => '
                sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l10n_parent, l10n_diffsource,
            ',
    ],
];
if ($enableChargeableEvents) {
    $palettes['costs'] = ['showitem' => 'price, price_member_type_2, price_member_type_3, price_member_type_4, price_member_type_5, --linebreak--, tax_type'];
    $palettes['bill'] = ['showitem' => 'bill_title, bill_payment_info,'];
    $tcaCostsFields = '
    --div--;LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.tabs.costs, 
         --palette--;;costs,
         --palette--;;bill,';
}
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event',
        'label_userFunc' => \KDN\KdnEvents\Hook\Labels::class . '->getEventLabel',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',

        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'default_sortby' => 'ORDER BY event_start DESC, uid DESC',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,subtitle,description,registration_cancel_limit,registration_start,registration_end,max_participants,published,hide_in_list,is_top_event,registrations,status,organizer,location,custom_location',
        'typeicon_classes' => [
            'default' => 'kdn-events',
        ],
    ],
    'types' => [
        '1' => ['showitem' => '--palette--;;visibility,
        --palette--;;settings,
        --palette--;;headline,
        description, tutor,
		organizer,
        --palette--;;location_data,
         contact,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.media,
            media,related_files,
        --div--;LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.event_times,
        event_times, is_series_event,
        --div--;LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.registration,
        registration_disabled,
        --palette--;;registration_time,
        --palette--;;registration_participants, registration_list,'.$tcaCostsFields.'
        --div--;LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.fields,
        enable_optional_fields, enable_required_fields, enabled_work_areas, terms,
        --div--;LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.internal,
        internal_remarks,
        --div--;LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.video_conference,
	    video_conference_url, video_conference_test_url, video_conference_comment,
		--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime, endtime,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;paletteLanguage,'],
    ],
    'palettes' => $palettes,
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ],
                ],
                'default' => 0,
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0]
                ],
                'foreign_table' => 'tx_kdnevents_domain_model_event',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_event.pid=###CURRENT_PID### AND tx_kdnevents_domain_model_event.sys_language_uid IN (-1,0)',
                'default' => 0
            ]
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ],
                ],
            ]
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'title' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.title',
            'config' => [
                'type' => 'input',
                'size' => 60,
                'eval' => 'trim,required'
            ],
        ],
        'slug' => [
            'exclude' => true,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.slug',
            'config' => [
                'type' => 'slug',
                'size' => 50,
                'generatorOptions' => [
                    'fields' => ['title'],
                    'fieldSeparator' => '-',
                    'replacements' => [
                        '/' => '-'
                    ],
                ],
                'fallbackCharacter' => '-',
                'eval' => 'uniqueInPid',
                'default' => ''
            ]
        ],
        'subtitle' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.subtitle',
            'config' => [
                'type' => 'input',
                'size' => 60,
                'eval' => 'trim'
            ],
        ],
        'description' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim,required',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
            ],
        ],
        'contact' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.contact',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,email',
            ],
        ],
        'tutor' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.tutor',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
            ],
        ],
        'event_times' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.event_times',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_kdnevents_domain_model_time',
                'foreign_field' => 'event',
                'minitems' => 1,
                'maxitems' => 9999,
                'levelLinksPosition' => 'bottom',
                'appearance' => [
                    'collapseAll' => 0,
                    'expandSingle' => 0,
                ],
            ],
        ],
        'is_series_event' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.is_series_event',
            'config' => [
                'type' => 'check',
                'default' => 0
            ],
        ],
        'event_start' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'event_end' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'registration_disabled' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.registration_disabled',
            'onChange' => 'reload',
            'config' => [
                'type' => 'check',
                'default' => 0
            ],
        ],
        'registration_start' => [
            'displayCond' => 'FIELD:registration_disabled:!=:1',
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.registration_start',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime,int,required',
                'checkbox' => 1,
                    //'default' => time()
            ],
        ],
        'registration_end' => [
            'displayCond' => 'FIELD:registration_disabled:!=:1',
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.registration_end',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime,int,required',
                'checkbox' => 1,
                //'default' => time()
            ],
        ],
        'registration_cancel_limit' => [
            //'displayCond' => 'FIELD:registration_disabled:!=:1',
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.registration_cancel_limit',
            'config' => [
                'type' => 'input',
                'size' => 5,
                'eval' => 'trim,int',
                'max' => 3,
            ],
        ],
        'max_participants' => [
            'displayCond' => 'FIELD:registration_disabled:!=:1',
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.max_participants',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int,required'
            ],
        ],
        'registrations' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.registrations',
            'config' => [
                'type' => 'none',
                'size' => 4,
                'format' => 'int'
            ],
        ],
        'waitlist_available' => [
            'displayCond' => 'FIELD:registration_disabled:!=:1',
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.waitlist_available',
            'config' => [
                'type' => 'check',
                'default' => 1
            ],
        ],
        'published' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.published',
            'config' => [
                'type' => 'check',
                'default' => 1
            ],
        ],
        'hide_in_list' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.hide_in_list',
            'config' => [
                'type' => 'check',
                'default' => 1
            ],
        ],
        'is_top_event' => [
            'exclude' =>1,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.is_top_event',
            'config' => [
                'type' => 'check'
            ]
        ],
        'is_online_event' => [
            'exclude' =>1,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.is_online_event',
            'config' => [
                'type' => 'check'
            ]
        ],
        'status' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.status',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_kdnevents_domain_model_status',
                //'foreign_table_where' => 'AND tx_kdnevents_domain_model_status.pid=###CURRENT_PID### AND tx_kdnevents_domain_model_status.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_kdnevents_domain_model_status.name ASC',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_status.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_kdnevents_domain_model_status.name ASC',
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 5,
            ],
        ],
        'organizer' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.organizer',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kdnevents_domain_model_organizer',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_organizer.pid=###CURRENT_PID### AND tx_kdnevents_domain_model_organizer.sys_language_uid=###REC_FIELD_sys_language_uid###',
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 0,
            ],
        ],
        'location' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.location',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_kdnevents_domain_model_location',
                'foreign_table' => 'tx_kdnevents_domain_model_location',
                'hideSuggest' => true,
                'hideMoveIcons' => true,
                'hideDeleteIcon' => false,
                'size' => 1,
                'autoSizeMax' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => true,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                    'elementBrowser' => [
                        'disabled' => false,
                    ],
                ],
                'fieldWizard' => [
                    'tableList' => [
                        'disabled' => true,
                    ],
                    'recordsOverview' => [
                        'disabled' => false,
                    ],
                ],
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 0
            ],
        ],
        'custom_location' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.custom_location',
            'config' => [
                'type' => 'input',
                'size' => 60,
                'eval' => 'trim'
            ],
        ],
        'internal_remarks' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.internal_remarks',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
            ],
        ],
        'video_conference_url' => [
            //'displayCond' => 'FIELD:is_online_event:=:1',
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.video_conference_url',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'eval' => 'trim',
                'enableRichtext' => false,
            ],
        ],
        'video_conference_test_url' => [
            //'displayCond' => 'FIELD:is_online_event:=:1',
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.video_conference_test_url',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 3,
                'eval' => 'trim',
                'enableRichtext' => false,
            ],
        ],
        'video_conference_comment' => [
            //'displayCond' => 'FIELD:is_online_event:=:1',
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.video_conference_comment',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 8,
                'eval' => 'trim',
                'enableRichtext' => false,
            ],
        ],
        'enable_optional_fields' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.enable_optional_fields',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectCheckBox',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.street', 'street'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.zipcode', 'zipcode'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.town', 'town'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.title', 'title'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.birth_date', 'birth_date'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.occupation', 'occupation'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.work_experience', 'work_experience'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation', 'organisation'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_facility_choice', 'facility'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_street', 'organisation_street'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_zipcode', 'organisation_zipcode'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_town', 'organisation_town'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_email', 'organisation_email'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_phone', 'organisation_phone'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.customer_message', 'customer_message'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.work_areas', 'work_areas'],
                ],
                'default' => 'zipcode,town,organisation',
                'multiple' => true,
                'size' => 5,
                'autosizemax' => 10,
                'maxitems' => 99,
                //'eval' => 'required'
            ],
        ],
        'enable_required_fields' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.enable_required_fields',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectCheckBox',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.street', 'street'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.zipcode', 'zipcode'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.town', 'town'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.title', 'title'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.birth_date', 'birth_date'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.occupation', 'occupation'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.work_experience', 'work_experience'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation', 'organisation'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_facility_choice', 'facility'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_street', 'organisation_street'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_zipcode', 'organisation_zipcode'],
                    //['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_town', 'organisation_town'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_email', 'organisation_email'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_phone', 'organisation_phone'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.customer_message', 'customer_message'],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.work_areas', 'work_areas'],
                ],
                'multiple' => true,
                'size' => 5,
                'autosizemax' => 10,
                'maxitems' => 99,
                //'eval' => 'required'
            ],
        ],
        'enabled_work_areas' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.enabled_work_areas',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectCheckBox',
                'foreign_table' => 'tx_kdnevents_domain_model_workarea',
                'foreign_table_where' => ' ORDER BY name ASC',
                'maxitems ' => 9999,
            ],
        ],
        'media' => [
            'exclude' => true,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.media',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'media',
                [
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.media.add',
                        'showPossibleLocalizationRecords' => true,
                        'showRemovedLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'media',
                        'tablenames' => 'tx_kdnevents_domain_model_event',
                        'table_local' => 'sys_file',
                    ],
                    // custom configuration for displaying fields in the overlay/reference table
                    // to use the newsPalette and imageoverlayPalette instead of the basicoverlayPalette
                    'overrideChildTca' => [
                        'types' => [
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_UNKNOWN => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
                                    --palette--;;audioOverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
                                    --palette--;;videoOverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                                    --palette--;LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;newsPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ]
                        ],
                    ],
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
            )
        ],
        'related_files' => [
            'exclude' => true,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.related_files',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'related_files',
                [
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.related_files.add',
                        'showPossibleLocalizationRecords' => true,
                        'showRemovedLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'inline' => [
                        'inlineOnlineMediaAddButtonStyle' => 'display:none'
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'related_files',
                        'tablenames' => 'tx_kdnevents_domain_model_event',
                        'table_local' => 'sys_file',
                    ],
                ]
            )
        ],
        'terms' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.terms',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
            ],
        ],
        'registration_list' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.registration_list',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_kdnevents_domain_model_registration',
                'foreign_field' => 'event',
                //'minitems' => 1,
                //'maxitems' => 15,
                'appearance' => [
                    'collapseAll' => 1,
                    'expandSingle' => 0,
                ],
            ],
        ],
        'price' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.price',
            'config' => [
                'type' => 'input',
                'size' => 6,
                'eval' => 'trim,double2',
                'max' => 9,
            ],
        ],
        'price_member_type_2' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.price_member_type_2',
            'config' => [
                'type' => 'input',
                'size' => 6,
                'eval' => 'trim,double2',
                'max' => 9,
            ],
        ],
        'price_member_type_3' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.price_member_type_3',
            'config' => [
                'type' => 'input',
                'size' => 6,
                'eval' => 'trim,double2',
                'max' => 9,
            ],
        ],
        'price_member_type_4' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.price_member_type_4',
            'config' => [
                'type' => 'input',
                'size' => 6,
                'eval' => 'trim,double2',
                'max' => 9,
            ],
        ],
        'price_member_type_5' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.price_member_type_5',
            'config' => [
                'type' => 'input',
                'size' => 6,
                'eval' => 'trim,double2',
                'max' => 9,
            ],
        ],
        'tax_type' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.tax_type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.tax_type.0', 0],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.tax_type.7', 7],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.tax_type.19', 19],
                ],
                'default' => 19,
            ]
        ],
        'bill_title' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.bill_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'max' => 150,
            ],
        ],
        'bill_payment_info' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.bill_payment_info',
            'config' => [
                'type' => 'text',
                'cols' => 20,
                'rows' => 2,
                'eval' => 'trim',
            ],
        ],
        'tstamp' => [
            'label' => 'tstamp',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
    ],
];
