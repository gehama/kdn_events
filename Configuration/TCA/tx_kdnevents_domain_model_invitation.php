<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation',
        'label_userFunc' => \KDN\KdnEvents\Hook\Labels::class . '->getEventInvitationLabel',
        'label' => 'email',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',

        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'email,first_name,last_name,hash,event',
        'typeicon_classes' => [
            'default' => 'kdn-events-invitation',
        ],
    ],
    'types' => [
        '1' => ['showitem' => '--palette--;;visibility, --palette--;;event,
        --palette--;;greeting, --palette--;;name, --palette--;;contact,
          message, status, hash,
         --palette--;;invitation,
        --div--;LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.internal, 
          internal_remarks,
          --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime, endtime,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;paletteLanguage,'],
    ],
    'palettes' => [
        'visibility' => ['showitem' => 'status, hidden'],
        'event' => ['showitem' => 'event, event_time'],
        'greeting' => ['showitem' => 'salutation, title'],
        'name' => ['showitem' => 'first_name, last_name'],
        'contact' => ['showitem' => 'email, phone, mobile',],
        'invitation' => ['showitem' => 'invited_by, registration',],
        'paletteLanguage' => [
            'showitem' => '
                sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l10n_parent, l10n_diffsource,
            ',
        ],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ],
                ],
                'default' => 0,
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0]
                ],
                'foreign_table' => 'tx_kdnevents_domain_model_invitation',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_invitation.pid=###CURRENT_PID### AND tx_kdnevents_domain_model_invitation.sys_language_uid IN (-1,0)',
                'default' => 0
            ]
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ],
                ],
            ]
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'salutation' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.salutation',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:please_choose', 0],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.salutation.1', 1],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.salutation.2', 2],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.salutation.3', 3],
                ],
                'default' => 0,
                //'size' => 1,
                //'maxitems' => 1,
                //'eval' => 'required'
            ],
        ],
        'title' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'first_name' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.first_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//required
            ],
        ],
        'last_name' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.last_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'email' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'email,required'
            ],
        ],
        'phone' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.phone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'mobile' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.mobile',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'message' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.message',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ],
        ],
        'hash' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'status' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.status',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:please_choose', 0],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.status.1', \KDN\KdnEvents\Domain\Model\AbstractEventPersonModel::STATUS_CREATED],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.status.2', \KDN\KdnEvents\Domain\Model\Invitation::STATUS_REGISTERED],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.status.3', \KDN\KdnEvents\Domain\Model\Invitation::STATUS_PARTICIPATED],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.status.9', \KDN\KdnEvents\Domain\Model\AbstractEventPersonModel::STATUS_CANCELLED],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.status.10', \KDN\KdnEvents\Domain\Model\AbstractEventPersonModel::STATUS_COMPLETED],
                ],
                'size' => 1,
                'maxitems' => 1,
                'default' => 0,
                'eval' => 'required'
            ],
        ],
        'event' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.event',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_kdnevents_domain_model_event',
                'foreign_table' => 'tx_kdnevents_domain_model_event',
                'hideSuggest' => true,
                'hideMoveIcons' => true,
                'hideDeleteIcon' => true,
                'size' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                    'elementBrowser' => [
                        'disabled' => false,
                    ],
                ],
                'fieldWizard' => [
                    'tableList' => [
                        'disabled' => true,
                    ],
                    'recordsOverview' => [
                        'disabled' => true,
                    ],
                ],
                'minitems' => 1,
                'maxitems' => 1,
                'default' => 0
            ],
        ],
        'event_time' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.event_time',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kdnevents_domain_model_time',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_time.event=(SELECT event FROM tx_kdnevents_domain_model_invitation WHERE uid=###THIS_UID###)',
                'minitems' => 1,
                'maxitems' => 1,
                'default' => 0,
            ],
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'internal_remarks' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.internal_remarks',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
            ],
        ],
        'invited_by' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.invited_by',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'fe_users',
                'foreign_table' => 'fe_users',
                'foreign_table_where' => 'ORDER BY last_name DESC',
                'hideSuggest' => true,
                'hideMoveIcons' => true,
                'hideDeleteIcon' => true,
                'size' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                    'elementBrowser' => [
                        'disabled' => false,
                    ],
                ],
                'fieldWizard' => [
                    'tableList' => [
                        'disabled' => true,
                    ],
                    'recordsOverview' => [
                        'disabled' => true,
                    ],
                ],
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 0
            ],
        ],
        'registration' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_invitation.registration',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_kdnevents_domain_model_registration',
                'foreign_table' => 'tx_kdnevents_domain_model_registration',
                'hideSuggest' => true,
                'hideMoveIcons' => true,
                'hideDeleteIcon' => true,
                'size' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                    'elementBrowser' => [
                        'disabled' => false,
                    ],
                ],
                'fieldWizard' => [
                    'tableList' => [
                        'disabled' => true,
                    ],
                    'recordsOverview' => [
                        'disabled' => true,
                    ],
                ],
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 0
            ],
        ],
    ],
];
