<?php
$enableInvitations = \KDN\KdnEvents\Utility\TcaUtility::getExtConf('enableInvitations');

return [
    'ctrl' => [
        'title' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration',
        'label_userFunc' => \KDN\KdnEvents\Hook\Labels::class . '->getEventRegistrationLabel',
        'label' => 'email',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',

        'default_sortby' => 'crdate DESC',
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'default_sortby' => 'ORDER BY uid DESC',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'email,first_name,last_name,organisation,town,agree_to_terms,newsletter,hash,status,event',
        'typeicon_classes' => [
            'default' => 'kdn-events-registration',
        ],
    ],
    'types' => [
        '1' => ['showitem' => '--palette--;;visibility, --palette--;;event,
        --palette--;;greeting, --palette--;;name, --palette--;;contact, street, --palette--;;zip_city,
          customer_message, birth_date, agree_to_terms, newsletter, hash,
        --div--;LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.work, 
         --palette--;;organisation, organisation_street, 
         --palette--;;org_zip_city, --palette--;;org_contact, --palette--;;org_job_info, --palette--;;org_work_areas,
        --div--;LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_event.internal, 
          internal_remarks, reminder_status, ' . ($enableInvitations ? 'invitation, ' : '') . '
          --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime, endtime,
            --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                --palette--;;paletteLanguage,'],
    ],
    'palettes' => [
        'visibility' => ['showitem' => 'status, hidden'],
        'event' => ['showitem' => 'event, event_time'],
        'greeting' => ['showitem' => 'salutation, title'],
        'name' => ['showitem' => 'first_name, last_name, user'],
        'contact' => ['showitem' => 'email, phone, mobile',],
        'zip_city' => ['showitem' => 'zipcode, town',],
        'organisation' => ['showitem' => 'organisation, facility',],
        'org_zip_city' => ['showitem' => 'organisation_zipcode, organisation_town',],
        'org_job_info' => ['showitem' => 'occupation, work_experience',],
        'org_work_areas' => ['showitem' => 'work_areas',],
        'org_contact' => ['showitem' => 'organisation_email, organisation_phone',],
        'paletteLanguage' => [
            'showitem' => '
                sys_language_uid;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:sys_language_uid_formlabel,l10n_parent, l10n_diffsource,
            ',
        ],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ],
                ],
                'default' => 0,
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0]
                ],
                'foreign_table' => 'tx_kdnevents_domain_model_registration',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_registration.pid=###CURRENT_PID### AND tx_kdnevents_domain_model_registration.sys_language_uid IN (-1,0)',
                'default' => 0
            ]
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],
        'hidden' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ],
                ],
            ]
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 16,
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ]
        ],
        'salutation' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.salutation',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:please_choose', 0],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.salutation.1', 1],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.salutation.2', 2],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.salutation.3', 3],
                ],
                'default' => 0,
                //'size' => 1,
                //'maxitems' => 1,
                //'eval' => 'required'
            ],
        ],
        'title' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'first_name' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.first_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//required
            ],
        ],
        'last_name' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.last_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'email' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'email,required'
            ],
        ],
        'street' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//required
            ],
        ],
        'zipcode' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.zipcode',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//required
            ],
        ],
        'town' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.town',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//,required
            ],
        ],
        'phone' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.phone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'mobile' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.mobile',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'birth_date' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.birth_date',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 7,
                'eval' => 'date',//,required
                'checkbox' => 1,
            ],
        ],
        'occupation' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.occupation',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//required
            ],
        ],
        'work_experience' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.work_experience',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//required
            ],
        ],
        'organisation' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation',
            'config' => [
                'type' => 'text',
                'cols' => 30,
                'rows' => 3,
                'eval' => 'trim'
            ],
        ],
        'organisation_email' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'email'
            ],
        ],
        'organisation_street' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//required
            ],
        ],
        'organisation_zipcode' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_zipcode',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//required
            ],
        ],
        'organisation_town' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_town',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',//required
            ],
        ],
        'organisation_phone' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.organisation_phone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'customer_message' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.customer_message',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ],
        ],
        'agree_to_terms' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.agree_to_terms',
            'config' => [
                'type' => 'check',
                'default' => 0,
                'eval' => 'required'
            ],
        ],
        'newsletter' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.newsletter',
            'config' => [
                'type' => 'check',
                'default' => 0,
                'eval' => 'required'
            ],
        ],
        'hash' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'status' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.status',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:please_choose', 0],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.status.1', \KDN\KdnEvents\Domain\Model\AbstractEventPersonModel::STATUS_CREATED],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.status.2', \KDN\KdnEvents\Domain\Model\Registration::STATUS_PARTICIPATED],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.status.3', \KDN\KdnEvents\Domain\Model\Registration::STATUS_NOT_PRESENT],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.status.4', \KDN\KdnEvents\Domain\Model\Registration::STATUS_WAITLIST],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.status.9', \KDN\KdnEvents\Domain\Model\AbstractEventPersonModel::STATUS_CANCELLED],
                ],
                'size' => 1,
                'maxitems' => 1,
                'default' => 0,
                'eval' => 'required'
            ],
        ],
        /*'event' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.event',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:please_choose', 0],
                ],
                'foreign_table' => 'tx_kdnevents_domain_model_event',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_event.pid=###CURRENT_PID### AND tx_kdnevents_domain_model_event.sys_language_uid=###REC_FIELD_sys_language_uid###',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],*/
        'event' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.event',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_kdnevents_domain_model_event',
                'foreign_table' => 'tx_kdnevents_domain_model_event',
                'hideSuggest' => true,
                'hideMoveIcons' => true,
                'hideDeleteIcon' => true,
                'size' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                    'elementBrowser' => [
                        'disabled' => false,
                    ],
                ],
                'fieldWizard' => [
                    'tableList' => [
                        'disabled' => true,
                    ],
                    'recordsOverview' => [
                        'disabled' => true,
                    ],
                ],
                'minitems' => 1,
                'maxitems' => 1,
                'default' => 0
            ],
        ],
        'event_time' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.event_time',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kdnevents_domain_model_time',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_time.event=(SELECT event FROM tx_kdnevents_domain_model_registration WHERE uid=###THIS_UID###)',
                'minitems' => 1,
                'maxitems' => 1,
                'default' => 0,
            ],
        ],
        'facility' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.facility',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:please_choose', 0],
                ],
                'foreign_table' => 'tx_kdnevents_domain_model_facility',
                //'foreign_table_where' => 'AND tx_kdnevents_domain_model_facility.pid=###CURRENT_PID### AND tx_kdnevents_domain_model_facility.sys_language_uid=###REC_FIELD_sys_language_uid###',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_facility.sys_language_uid=###REC_FIELD_sys_language_uid###',
                'size' => 1,
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 0,
                //'eval' => 'required'
            ],
        ],
        'work_areas' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.work_areas',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_kdnevents_domain_model_workarea',
                'foreign_table_where' => 'AND tx_kdnevents_domain_model_workarea.sys_language_uid=###REC_FIELD_sys_language_uid### ORDER BY tx_kdnevents_domain_model_workarea.name',
                'MM' => 'tx_kdnevents_domain_model_registration_workarea_mm',
                'size' => 2,
                'autoSizeMax' => 5,
                'maxitems' => 50,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
        ],
        'crdate' => [
            'label' => 'crdate',
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'internal_remarks' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.internal_remarks',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
            ],
        ],
        'reminder_status' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.reminder_status',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.reminder_status.0', 0],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.reminder_status.1', 1],
                    ['LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.reminder_status.2', 2],
                ],
                'default' => 0,
                //'size' => 1,
                //'maxitems' => 1,
                //'eval' => 'required'
            ],
        ],
        'user' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.user',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'fe_users',
                'foreign_table' => 'fe_users',
                'foreign_table_where' => 'ORDER BY last_name DESC',
                'hideSuggest' => true,
                'hideMoveIcons' => true,
                'hideDeleteIcon' => true,
                'size' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                    'elementBrowser' => [
                        'disabled' => false,
                    ],
                ],
                'fieldWizard' => [
                    'tableList' => [
                        'disabled' => true,
                    ],
                    'recordsOverview' => [
                        'disabled' => true,
                    ],
                ],
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 0
            ],
        ],
        'invitation' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_db.xlf:tx_kdnevents_domain_model_registration.invitation',
            'config' => $enableInvitations ? [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'tx_kdnevents_domain_model_invitation',
                'foreign_table' => 'tx_kdnevents_domain_model_invitation',
                'hideSuggest' => true,
                'hideMoveIcons' => true,
                'hideDeleteIcon' => true,
                'size' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => true,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                    'elementBrowser' => [
                        'disabled' => false,
                    ],
                ],
                'fieldWizard' => [
                    'tableList' => [
                        'disabled' => true,
                    ],
                    'recordsOverview' => [
                        'disabled' => true,
                    ],
                ],
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 0
            ] : [
                'type' => 'passthrough',
            ],
        ],
    ],
];
