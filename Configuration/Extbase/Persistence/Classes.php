<?php
declare(strict_types=1);

return [
    \KDN\KdnEvents\Domain\Model\Category::class => [
        'tableName' => 'sys_category',
        'properties' => [
            'parent' => [
                'fieldName' => 'parentcategory'
            ],
        ],
    ],
];
