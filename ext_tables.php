<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    static function($extKey)
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kdnevents_domain_model_event');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kdnevents_domain_model_status');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kdnevents_domain_model_organizer');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kdnevents_domain_model_location');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kdnevents_domain_model_registration');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kdnevents_domain_model_emailtemplate');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kdnevents_domain_model_facility');
        if (\KDN\KdnEvents\Utility\TcaUtility::getExtConf('enableInvitations')) {
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kdnevents_domain_model_invitation');
        }

        // Page TSConfig
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            chr (10) . '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $extKey . '/Configuration/TSConfig/Page/Config.typoscript">' . chr(10)
        );

        /**
         * Registers a Backend Module
         */
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            'KDN.' . $extKey,
            'web',	 // Make module a submodule of 'web'
            'kdnevents',	// Submodule key
            '',						// Position
            [
                'Backend' => 'list, export, notify, events, exportPdf',
                'Evaluation' => 'evaluation, evaluateMonthly, evaluateLocations, evaluateCancellations',

            ],
            [
                'access' => 'user,group',
                'icon'   => 'EXT:kdn_events/Resources/Public/Icons/module-kdnevents.svg',
                'labels' => 'LLL:EXT:kdn_events/Resources/Private/Language/locallang_kdnevents.xlf',
            ]
        );
        //hooks for TCEMAIN: handles sophisticated backend logic
        $GLOBALS ['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][$extKey] = \KDN\KdnEvents\Hook\CoreDataHandler::class;
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processCmdmapClass'][$extKey] = \KDN\KdnEvents\Hook\CoreDataHandler::class;

        /* @var $iconRegistry \TYPO3\CMS\Core\Imaging\IconRegistry */
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        $iconRegistry->registerIcon(
            'kdn-events',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'calendar']
        );
        $iconRegistry->registerIcon(
            'kdn-events-email-template',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'envelope']
        );
        $iconRegistry->registerIcon(
            'kdn-events-facility',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'building-o']
        );
        $iconRegistry->registerIcon(
            'kdn-events-work-area',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'briefcase']
        );
        $iconRegistry->registerIcon(
            'kdn-events-location',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'map-marker']
        );
        $iconRegistry->registerIcon(
            'kdn-events-organizer',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'address-book']
        );
        $iconRegistry->registerIcon(
            'kdn-events-registration',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'address-card-o']
        );
        $iconRegistry->registerIcon(
            'kdn-events-status',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'cogs']
        );
        $iconRegistry->registerIcon(
            'kdn-events-time',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'calendar-o']
        );
        $iconRegistry->registerIcon(
            'kdn-events-evaluation',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'bar-chart']
        );
        $iconRegistry->registerIcon(
            'kdn-events-invitation',
            \TYPO3\CMS\Core\Imaging\IconProvider\FontawesomeIconProvider::class,
            ['name' => 'ticket']
        );
    },
    'kdn_events'
);
