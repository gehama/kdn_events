<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$boot = static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'KDN.kdn_events',
        'Items',
        [
            'Event' => 'list, cancel, show, submit, register, confirm, icalDownload',

        ],
        // non-cacheable actions
        [
            'Event' => 'list, cancel, show, submit, register, confirm, icalDownload',

        ]
    );
    if (\KDN\KdnEvents\Utility\TcaUtility::getExtConf('enableInvitations')) {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'KDN.kdn_events',
            'Invitation',
            [
                'Invitation' => 'show, submit, invite, confirm, accept',

            ],
            // non-cacheable actions
            [
                'Invitation' => 'show, submit, invite, confirm, accept',

            ]
        );
    }

    // https://docs.typo3.org/typo3cms/TCAReference/ColumnsConfig/Properties/InputEval.html
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tce']['formevals'][\KDN\KdnEvents\Evaluation\EmailDomainEvaluator::class] = '';

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update']['eventSlugs']
        = \KDN\KdnEvents\Updates\PopulateEventSlugs::class;


    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('ke_search')) {
        $eventIndexerClassName = \KDN\KdnEvents\KeSearchHooks\CustomIndexer::class;
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['ke_search']['registerIndexerConfiguration'][] = $eventIndexerClassName;
        $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['ke_search']['customIndexer'][] = $eventIndexerClassName;
        // Register hooks for indexing additional fields.
        //$additionContentClassName = 'TeaminmediasPluswerk\KeSearchHooks\AdditionalContentFields';
        //$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['ke_search']['modifyPageContentFields'][] = $additionContentClassName;
        //$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['ke_search']['modifyContentFromContentElement'][] = $additionContentClassName;
    }

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(trim('
    config.pageTitleProviders {
        events {
            provider = KDN\KdnEvents\Seo\EventTitleProvider
            before = altPageTitle,record,seo
        }
    }
    '));
    // Custom form element for email template markers
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1607519269] = [
        'nodeName' => 'eventMarkers',
        'priority' => 30,
        'class' => \KDN\KdnEvents\FormEngine\FieldControl\EventMarkerControl::class
    ];
};

$boot();
unset($boot);